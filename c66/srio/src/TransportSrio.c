/* 
 * Copyright (c) 2011-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/*
 *  ======== TransportSrio.c ========
 */

/* XDC include */
#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>

/* BIOS include */
#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/sysbios/family/c66/Cache.h>

/* IPC internal includes */
#include <ti/sdo/ipc/_MessageQ.h>

/* IPC external include */
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MultiProc.h>

/* CSL include */
#include <ti/csl/csl_chip.h>

/* SRIO LLD */
#include <ti/drv/srio/srio_drv.h>

/* QMSS LLD */
#include <ti/drv/qmss/qmss_drv.h>

/* CPPI LLD */
#include <ti/drv/cppi/cppi_drv.h>

/* SRIO Transport include */
#include "package/internal/TransportSrio.xdc.h"

/* SRIO HW maximum transmittable packet size */
#define TRANS_SRIO_HW_MTU_BYTES        4096
#define TRANS_SRIO_MAX_PAGE_ENTRIES    80

#define TRANS_SRIO_RECYCLE_ALL_TX_BUFS -1

/* Fragmentation/reassembly header for packets greater than or equal to
 * 4096 bytes */
typedef struct {
    uint8_t seqn;
    uint8_t maxSeqn;
    uint8_t tag;
    uint8_t reserved;
} transFragHdr_t;

/* Source endpoint fragmentation stream tracker */
typedef struct {
    uint8_t   tag;
    int8_t    prevSeqn;
    uint16_t  bufLen;
    uint8_t  *rcvBuf;
} transFragEp_t;

static UInt32 l2_to_global_addr(UInt32 addr)
{
    if ((addr & 0xFF800000) == 0x00800000) {
        /* In L2, compute the global address. */
        return(addr + (0x10000000 + (DNUM << 24)));
    } else {
        return(addr);
    }
}

static UInt32 global_to_l2_addr(UInt32 addr)
{
    UInt32 mask = 0x10000000 + (DNUM << 24);

    if ((addr & 0xFF000000) == mask) {
        /* Return L2 address */
        return(addr - mask);
    } else {
        /* DDR3 or MSMC - do nothing */
        return(addr);
    }
}

static Void wb(Void *addr, UInt32 numBytes)
{
    UInt32 coreMask = 0x10000000 + (DNUM << 24);

    if ((((UInt32)addr) & 0x1F800000) != coreMask) {
        /* A different core's global L2 addres - only writeback L1 */
        Cache_wb((Ptr)addr, numBytes, Cache_Type_L1D, TRUE);
    } else if ((((UInt32)addr) & 0xFC000000) == 0x0C000000) {
        /* MSMC addres - only writeback L1 */
        Cache_wb((Ptr)addr, numBytes, Cache_Type_L1D, TRUE);
    } else if ((((UInt32)addr) & 0x80000000) == 0x80000000) {
        /* DDR3 address - writeback L1 & L2 */
        Cache_wb((Ptr)addr, numBytes, Cache_Type_ALL, TRUE);
    }
}

static Void inv(Void *addr, UInt32 numBytes) 
{
    UInt32 coreMask = 0x10000000 + (DNUM << 24);

    if ((((UInt32)addr) & 0x1F800000) != coreMask) {
        /* A different core's global L2 addres - only invalidate L1 */
        Cache_inv((Ptr)addr, numBytes, Cache_Type_L1D, TRUE);
    } else if ((((UInt32)addr) & 0xFC000000) == 0x0C000000) {
        /* MSMC addres - only invalidate L1 */
        Cache_inv((Ptr)addr, numBytes, Cache_Type_L1D, TRUE);
    } else if ((((UInt32)addr) & 0x80000000) == 0x80000000) {
        /* DDR3 address - invalidate L1 & L2 */
        Cache_inv((Ptr)addr, numBytes, Cache_Type_ALL, TRUE);
    }
}

/*
 *************************************************************************
 *                       Module functions
 *************************************************************************
 */

/**
 *  @b Description
 *  @n
 *      Resets all SRIO transport module parameters to initial state.
 *      This should be done after all instances of a transport have
 *      been closed on a DSP core.
 *
 *  @retval
 *      Not Applicable.
 */
static Void transportSrioResetModuleParams(TransportSrio_srioSockType sockType,
                                           Int status)
{
    UInt32 hwiKey;

    switch (status) {
        /* Errors that don't require module param reset */
        case TransportSrio_ERROR_NULL_SOCKET_PARAMS:
        case TransportSrio_ERROR_INVALID_SOCKET_TYPE:
        case TransportSrio_ERROR_SRIO_TYPE11_INST_ALREADY_EXISTS:
        case TransportSrio_ERROR_SRIO_TYPE9_INST_ALREADY_EXISTS:
            return;
        default:
            break;
    }

    hwiKey = Hwi_disable();
    if (sockType == TransportSrio_srioSockType_TYPE_11) {
        TransportSrio_module->t11InstCreated = 0;
    } else if (sockType == TransportSrio_srioSockType_TYPE_9) {
        TransportSrio_module->t9InstCreated = 0;
    }
    Hwi_restore(hwiKey);
}

/**
 *  @b Description
 *  @n
 *      Cleans up an instance of TransportSrio.
 *
 *  @retval
 *      Not Applicable.
 */
static Void transportSrioInstCleanup(TransportSrio_Object *obj)
{
    if (obj->hwiHandle) {
        /* Disable the interrupt tied to the receive side */
        Hwi_disableInterrupt(obj->rxIntVectorId);
        Hwi_delete((Hwi_Handle *) &(obj->hwiHandle));
    }

    if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_11) {
        if (obj->sockParams.u.pT11Eps) {
            Memory_free(0, obj->sockParams.u.pT11Eps,
                        obj->sockParams.epListSize *
                        sizeof(TransportSrio_srioSockType11EpParams));
        }
    } else if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_9) {
        if (obj->sockParams.u.pT9Eps) {
            Memory_free(0, obj->sockParams.u.pT9Eps,
                        obj->sockParams.epListSize *
                        sizeof(TransportSrio_srioSockType9EpParams));
        }
    }

    if (obj->socketHandle) {
        /* Close the SRIO socket */
        Srio_sockClose(obj->socketHandle);
    }

    if (obj->srioHandle) {
        /* Stop SRIO */
        Srio_stop(obj->srioHandle);
    }

    /* Empty and close all QMSS queues */
    if (obj->txCompletionQ) {
        /* Clean up any MessageQ buffers attached to completed send
         * descriptors */
        while (Qmss_getQueueEntryCount(obj->txCompletionQ)) {
            Cppi_HostDesc *hostDesc;
            MessageQ_Msg   msgQBuf;
            UInt32         numBytes;

            hostDesc = (Cppi_HostDesc *)QMSS_DESC_PTR(Qmss_queuePop(obj->txCompletionQ));
            Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                         (UInt8 **)&msgQBuf, &numBytes);
            if (msgQBuf) {
                msgQBuf = (MessageQ_Msg)global_to_l2_addr((UInt32) msgQBuf);
                MessageQ_free(msgQBuf);
            }
        }
        Qmss_queueEmpty(obj->txCompletionQ);
        Qmss_queueClose(obj->txCompletionQ);
    }
    if (obj->txFragQ) {
        /* Clean up any MessageQ buffers attached to fragment descriptors */
        while (Qmss_getQueueEntryCount(obj->txFragQ)) {
            Cppi_HostDesc *hostDesc;
            MessageQ_Msg   msgQBuf;
            UInt32         numBytes;

            hostDesc = (Cppi_HostDesc *)QMSS_DESC_PTR(Qmss_queuePop(obj->txFragQ));
            Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                         (UInt8 **)&msgQBuf, &numBytes);
            if (msgQBuf) {
                msgQBuf = (MessageQ_Msg)global_to_l2_addr((UInt32) msgQBuf);
                MessageQ_free(msgQBuf);
            }
        }
        Qmss_queueEmpty(obj->txFragQ);
        Qmss_queueClose(obj->txFragQ);
    }
    if (obj->txFreeQ) {
        Qmss_queueEmpty(obj->txFreeQ);
        Qmss_queueClose(obj->txFreeQ);
    }
    if (obj->rxFreeQ) {
        /* Clean up any MessageQ buffers attached to rx free descriptors */
        while (Qmss_getQueueEntryCount(obj->rxFreeQ)) {
            Cppi_HostDesc *hostDesc;
            MessageQ_Msg   msgQBuf;
            UInt32         numBytes;

            hostDesc = (Cppi_HostDesc *) QMSS_DESC_PTR(Qmss_queuePop(obj->rxFreeQ));
            Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                         (UInt8 **)&msgQBuf, &numBytes);
            if (msgQBuf) {
                msgQBuf = (MessageQ_Msg) global_to_l2_addr((UInt32) msgQBuf);
                MessageQ_free(msgQBuf);
            }
        }
        Qmss_queueEmpty(obj->rxFreeQ);
        Qmss_queueClose(obj->rxFreeQ);
    }
    if (obj->rxAccumQ) {
        Qmss_queueClose(obj->rxAccumQ);
    }

    if (obj->rxAccList) {
        /* Free accumulator list memory */
        Memory_free(0, (UInt32 *)(global_to_l2_addr((UInt32)obj->rxAccList)),
                    obj->rxAccListSize);
    }

    if (obj->fragSrcs) {
        Memory_free(0, obj->fragSrcs, obj->sockParams.epListSize *
                    sizeof(transFragEp_t));
    }
}

/**
 *  @b Description
 *  @n
 *      Allocates a new MessageQ buffer for a SRIO receive descriptor
 *
 *  @retval
 *      Not Applicable.
 */
static Void transportSrioNewRxBuf(TransportSrio_Object *obj,
                                  Cppi_HostDesc *hostDesc)
{
    MessageQ_Msg newMsg = MessageQ_alloc(obj->rxMsgQHeapId, obj->maxMTU);

    Assert_isTrue((newMsg != NULL), TransportSrio_A_newRxBufNull);

    /* Convert the buffer to the global address - Only applicable for buffers
     * residing in L2 */
    newMsg = (MessageQ_Msg) l2_to_global_addr((UInt32) newMsg);

    Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc, (UInt8 *)newMsg,
                 obj->maxMTU);
    Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                            (UInt8 *)newMsg, obj->maxMTU);

    wb((Void *)newMsg, newMsg->msgSize);
    wb((Void *)hostDesc, obj->rxDescSize);
    Qmss_queuePushDescSize(obj->rxFreeQ, (Void *)hostDesc, obj->rxDescSize);
}

/**
 *  @b Description
 *  @n
 *      Recycle SRIO transmit side buffers.  If TRANS_SRIO_RECYCLE_ALL_TX_BUFS
 *      (-1) is passed all buffers in the TX completion queue will be recycled.
 *
 *  @retval
 *      Int - Number of buffers recycled
 */
static Int transportSrioTxBufRecycle(TransportSrio_Object *obj,
                                     Int numBufsToRecycle)
{
    Int            i = 0;
    Cppi_HostDesc *hostDesc;
    Ptr            msg;
    UInt32         msgQDataLen;
    Int32          status;
    Qmss_QueueHnd  txCompQ = (Qmss_QueueHnd) obj->txCompletionQ;

    while (1) {
        if (numBufsToRecycle != TRANS_SRIO_RECYCLE_ALL_TX_BUFS) {
            if (i >= numBufsToRecycle) {
                break;
            }
        }

        if (hostDesc = (Cppi_HostDesc *)QMSS_DESC_PTR(Qmss_queuePop(txCompQ))) {
            inv((Void *) hostDesc, obj->txDescSize);
            Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                         (UInt8 **)&msg, (UInt32 *)&msgQDataLen);

            /* Free msg back to MessageQ */
            msg = (Ptr) global_to_l2_addr((UInt32)msg);
            status = MessageQ_free(msg);
            Assert_isTrue((status >= 0), TransportSrio_A_txMsgQFreeError);

            Qmss_queuePushDesc((Qmss_QueueHnd) obj->txFreeQ, (Void *)hostDesc);
        } else {
            break;
        }
        i++;
    }

    return(i);
}

/*
 *  ======== transportSrioStandardSend ========
 *  Routine used to send a single packet containing a MessageQ message that is
 *  smaller than the configured maximum MTU.  Fragmentation is not needed.
 */
static Bool transportSrioStandardSend(TransportSrio_Object *obj, Ptr msg,
                                      Srio_SockAddrInfo *srioAddr)
{
    Cppi_HostDesc *hostDesc;
    UInt32         msgQDataLen = MessageQ_getMsgSize(msg);
    Int32          status;

    hostDesc = (Cppi_HostDesc *)Qmss_queuePop((Qmss_QueueHnd)obj->txFreeQ);
    if (!hostDesc) {
        Qmss_QueueHnd compQ = (Qmss_QueueHnd)obj->txCompletionQ;
        Ptr           oldMsg;
        uint32_t      oldBufLen;

        /* Free Q out of descriptors: poll tx completion Q until descriptor
         * becomes available from SRIO hardware */
        do {
            hostDesc = (Cppi_HostDesc *)QMSS_DESC_PTR(Qmss_queuePop(compQ));
        } while (!hostDesc);
        /* Free old MessageQ msg attached to descriptor before reusing */
        Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                     (UInt8 **)&oldMsg, (UInt32 *)&oldBufLen);
        oldMsg = (Ptr) global_to_l2_addr((UInt32)oldMsg);
        status = MessageQ_free(oldMsg);
        Assert_isTrue((status >= 0), TransportSrio_A_txMsgQFreeError);
    }

    /* Convert MsgQ message addr to global addr (applicable to L2),
     * writeback, then add to descriptor */
    msg = (Ptr) l2_to_global_addr((UInt32)msg);
    wb((Void *) msg, msgQDataLen);
    Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc, (UInt8 *)msg,
                 msgQDataLen);
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                      msgQDataLen);
    Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc*)hostDesc,
                            (UInt8 *)msg, msgQDataLen);
    wb((Void *) hostDesc, obj->txDescSize);

    /* Send the packet */
    status = Srio_sockSend(obj->socketHandle, (Srio_DrvBuffer)hostDesc,
                           obj->txDescSize, srioAddr);
    Assert_isTrue ((status >= 0), TransportSrio_A_socketSendError);
    if (status < 0) {
        /* Error when sending - Push packet on completion queue for
         * cleanup at a later time */
        Qmss_queuePushDescSize((Qmss_QueueHnd) obj->txCompletionQ,
                               (void *)hostDesc, obj->txDescSize);
        return(FALSE);
    }

    /* Check completion queue to clean up the previous transaction
     * descriptor */
    transportSrioTxBufRecycle(obj, 1);
    return(TRUE);
}

/*
 *  ======== transportSrioFragmentSend ========
 *  Routine used to fragment and send a MessageQ message that is greater than
 *  the configured maximum MTU
 */
static Bool transportSrioFragmentSend(TransportSrio_Object *obj, Ptr msg,
                                      Srio_SockAddrInfo *srioAddr)
{
    UInt32   const  msgQDataLen = MessageQ_getMsgSize(msg);
    uint8_t         maxSeqn = 0;
    uint32_t const  fragDataSize = obj->maxMTU - sizeof(MessageQ_MsgHeader) -
                                   sizeof(transFragHdr_t);
    uint8_t        *msgQMsg = (uint8_t *)msg;
    uint8_t         seqn = 0;
    uint32_t        msgOffset = 0;
    Bool            status = TRUE;

    /* Calculate number of fragments needed (base zero) */
    maxSeqn = ((msgQDataLen + fragDataSize - 1) / fragDataSize) - 1;
    /* Update fragmented message tag.  Overflow back to zero is okay */
    obj->fragMsgTag++;

    while(msgOffset < msgQDataLen) {
        Cppi_HostDesc  *hostDesc;
        uint8_t        *sendBuf = NULL;
        uint32_t        sendBufLen;
        uint32_t        fragBytes;
        transFragHdr_t *fragHdr;
        Int32           status;

        /* Poll fragment Q until descriptor becomes available from SRIO
         * hardware */
        do {
            Qmss_QueueHnd fragQ = (Qmss_QueueHnd)obj->txFragQ;

            hostDesc = (Cppi_HostDesc *)QMSS_DESC_PTR(Qmss_queuePop(fragQ));
        } while (!hostDesc);
        Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                     &sendBuf, &sendBufLen);

        /* Calculate number of bytes that need to be copied from msg to
         * this fragment */
        fragBytes = (((seqn + 1) * fragDataSize) > msgQDataLen) ?
                    msgQDataLen - (seqn * fragDataSize) : fragDataSize;

        /* Set fragment header which is placed after the MessageQ header */
        fragHdr = (transFragHdr_t *)&sendBuf[sizeof(MessageQ_MsgHeader)];
        fragHdr->seqn    = seqn++;
        fragHdr->maxSeqn = maxSeqn;
        fragHdr->tag     = obj->fragMsgTag;
        memcpy((void *)&sendBuf[sizeof(MessageQ_MsgHeader) +
                                sizeof(transFragHdr_t)],
               (void *)&msgQMsg[msgOffset], fragBytes);
        msgOffset += fragBytes;

        wb((Void *)sendBuf, sendBufLen);
        Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc, sendBuf,
                     sendBufLen);
        Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                          sendBufLen);
        Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc*)hostDesc,
                                sendBuf, sendBufLen);
        wb((Void *) hostDesc, obj->txDescSize);

        /* Send the packet */
        status = Srio_sockSend(obj->socketHandle, (Srio_DrvBuffer)hostDesc,
                               obj->txDescSize, srioAddr);
        Assert_isTrue ((status >= 0), TransportSrio_A_socketSendError);
        if (status < 0) {
            /* Error when sending - Push packet back on fragmentation queue */
            Qmss_queuePushDescSize((Qmss_QueueHnd) obj->txFragQ,
                                   (void *)hostDesc, obj->txDescSize);
            status = FALSE;
            goto error_exit;
        }
    }

error_exit:
    MessageQ_free((MessageQ_Msg)msg);
    return(status);
}

/*
 *  ======== transportSrioFragmentSend ========
 *  Routine used to form a full message from fragments received over the SRIO
 *  interface.
 */
static MessageQ_Msg transportSrioFragmentRcv(TransportSrio_Object *obj,
                                             MessageQ_Msg fragMsg,
                                             Srio_SockAddrInfo *srcSocket)
{
    uint32_t const  fragDataSize = obj->maxMTU - sizeof(MessageQ_MsgHeader) -
                                   sizeof(transFragHdr_t);
    uint8_t        *fragBuf = (uint8_t *)fragMsg;
    transFragHdr_t *fragHdr;
    uint8_t        *data;
    uint8_t         maxSeqn = 0;
    uint8_t         seqn = 0;
    uint8_t         msgTag;
    transFragEp_t  *fragSrcs = (transFragEp_t *)obj->fragSrcs;
    int             i;
    transFragEp_t  *srcEp = NULL;
    uint8_t        *rcvBufOffset;
    MessageQ_Msg    rcvMsg = NULL;

    /* Set fragment header which is placed after the MessageQ header */
    fragHdr = (transFragHdr_t *)&fragBuf[sizeof(MessageQ_MsgHeader)];
    data    = (uint8_t *)&fragBuf[sizeof(MessageQ_MsgHeader) +
                                  sizeof(transFragHdr_t)];
    maxSeqn = fragHdr->maxSeqn;
    seqn    = fragHdr->seqn;
    msgTag  = fragHdr->tag;

    for (i = 0; i < obj->sockParams.epListSize; i++) {
        TransportSrio_srioSockType11EpParams *t11Ep = obj->sockParams.u.pT11Eps;
        TransportSrio_srioSockType9EpParams *t9Ep = obj->sockParams.u.pT9Eps;

        switch (obj->sockParams.sockType) {
            case TransportSrio_srioSockType_TYPE_11:
                if ((srcSocket->type11.id == t11Ep[i].deviceId) &&
                    (srcSocket->type11.letter == t11Ep[i].letter) &&
                    (srcSocket->type11.mbox == t11Ep[i].mailbox)) {
                    srcEp = &fragSrcs[i];
                }
                break;
            case TransportSrio_srioSockType_TYPE_9:
                if ((srcSocket->type9.id == t9Ep[i].deviceId) &&
                    (srcSocket->type9.cos == t9Ep[i].cos) &&
                    (srcSocket->type9.streamId == t9Ep[i].streamId)) {
                    srcEp = &fragSrcs[i];
                }
                break;
            default:
                Log_warning1("TransportSrio_isr reassembly: "
                             "(MsgTag: %d) Received fragment with an invalid "
                             "socket type\n", msgTag);
                goto error_exit;
        }

        if (srcEp) {
            break;
        }
    }

    if (srcEp == NULL) {
        if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_11) {
            Log_warning4("TransportSrio_isr T11 reassembly: "
                         "(MsgTag: %d) Fragment source not recognized for "
                         "ID: 0x%x, letter: %d, mailbox: %d\n", msgTag,
                         srcSocket->type11.id, srcSocket->type11.letter,
                         srcSocket->type11.mbox);
        } else {
            Log_warning4("TransportSrio_isr T9 reassembly: "
                         "(MsgTag: %d) Fragment source not recognized for "
                         "ID: 0x%x, cos: %d, streamID: %d\n", msgTag,
                         srcSocket->type9.id, srcSocket->type9.cos,
                         srcSocket->type9.streamId);
        }
    } else {
        if (seqn == 0) {
            /* Start of new fragment stream.  Get length of entire stream from
             * MessageQ header in first fragment. */
            UInt32 totalLen = MessageQ_getMsgSize(data);

            if (srcEp->rcvBuf) {
                /* Previous fragment stream hasn't completed.  Drop reassembly
                 * buffer */
                Log_warning2("TransportSrio_isr reassembly: "
                             "Received start of new stream with tag %d before "
                             "stream with tag %d completed.  Dropping old "
                             "fragments\n", msgTag, srcEp->tag);
                MessageQ_free((MessageQ_Msg)srcEp->rcvBuf);
            }

            srcEp->prevSeqn = -1;
            srcEp->tag      = msgTag;
            srcEp->bufLen   = totalLen;
            srcEp->rcvBuf   = (uint8_t *)MessageQ_alloc(obj->rxMsgQHeapId,
                                                        totalLen);
            if (srcEp->rcvBuf == NULL) {
                Log_warning2("TransportSrio_isr reassembly: "
                             "(MsgTag: %d) Could not allocate receive buffer "
                             "of size %d bytes\n", msgTag, totalLen);
                goto error_exit;
            }
        }

        if (((srcEp->prevSeqn + 1) != seqn) && (srcEp->tag == msgTag)) {
            /* Out of order packet in current tag stream.  Drop current and
             * future fragments until seqn zero is received */
            MessageQ_free((MessageQ_Msg)srcEp->rcvBuf);
            srcEp->rcvBuf = NULL;
            Log_warning1("TransportSrio_isr reassembly: "
                         "(MsgTag: %d) Out of order fragment.  Dropping "
                         "entire stream\n", msgTag);

        /* Don't reset entire stream if current stream's tag is different from
         * tag in received fragment (zero will reset) */
        } else if (srcEp->tag == msgTag) {
            uint32_t copySize = fragDataSize;
            
            rcvBufOffset = srcEp->rcvBuf + (seqn * fragDataSize);
            if (seqn == maxSeqn) {
                /* Don't copy padding from last fragment */
                copySize = srcEp->bufLen - (fragDataSize * seqn);
                /* All fragments assembled after following copy */
                rcvMsg = (MessageQ_Msg)srcEp->rcvBuf;
                srcEp->rcvBuf = NULL;
            }
            memcpy(rcvBufOffset, data, copySize);

            if (seqn == 0) {
                MessageQ_Msg hdr = (MessageQ_Msg)srcEp->rcvBuf;

                hdr->heapId = obj->rxMsgQHeapId;
            }
            srcEp->prevSeqn = seqn;
        } else {
            Log_warning2("TransportSrio_isr reassembly: "
                         "Received inconsistent tag.  Assembling msgTag %d "
                         "but received msgTag %d.  Dropping fragment\n",
                         srcEp->tag, msgTag);
        }
    }

error_exit:
    /* Recycle fragment */
    fragMsg->heapId = obj->rxMsgQHeapId;
    MessageQ_free(fragMsg);

    return(rcvMsg);
}

/**
 *  @b Description
 *  @n
 *      Recycle SRIO receive side buffers.
 *
 *  @retval
 *      Not Applicable.
 */
Void transportSrioRxBufRecycle(void *arg, Srio_DrvBuffer hDrvBuffer)
{
    TransportSrio_Object *obj = (TransportSrio_Object *)arg;
    Cppi_HostDesc        *hostDesc = (Cppi_HostDesc *) hDrvBuffer;
    MessageQ_Msg          rxMsg;
    Int32                 status;
    UInt32                rxNumBytes;

    Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc, (UInt8 **)&rxMsg,
                 (UInt32 *)&rxNumBytes);
    inv((Void *)rxMsg, rxNumBytes);

    /* Reset heapId to rxMsgQHeapId.  This heap is overwritten when message is
     * received over SRIO */
    rxMsg->heapId = obj->rxMsgQHeapId;

    /* Free the MessageQ Msg */
    status = MessageQ_free(rxMsg);
    Assert_isTrue((status >= 0), TransportSrio_A_rxMsgQFreeError);

    transportSrioNewRxBuf(obj, hostDesc);
}

/*
 *************************************************************************
 *                       Instance functions
 *************************************************************************
 */
 
/*
 *  ======== TransportSrio_Instance_init ========
 */
Int TransportSrio_Instance_init(TransportSrio_Object *obj,
                                const TransportSrio_Params *params,
                                Error_Block *eb)
{
    Int32                  ipcCoreNum = MultiProc_self();
    UInt32                 isAllocated;
    Cppi_DescCfg           descCfg;
    Qmss_QueueHnd          tmpQ;
    Int                    i;
    Cppi_HostDesc         *hostDesc;
    Qmss_Queue             queueInfo;
    Srio_DrvConfig         srioCfg;
    Int16                  isrEventId;
    Hwi_Params             hwiAttrs;
    Srio_SocketType        socketType;
    Srio_SockBindAddrInfo  bindInfo;
    UInt32                 hwiKey;
    Int                    retVal = TransportSrio_OK;

    /* Initialize object parameters - don't memset to avoid wiping out
     * parameters set by RTSC wrapper */
    obj->deviceCfgParams       = NULL;
    obj->txCompletionQ         = 0;
    obj->txFreeQ               = 0;
    obj->txFragQ               = 0;
    obj->rxFreeQ               = 0;
    obj->rxAccumQ              = 0;
    obj->rxMsgQHeapId          = 0;
    obj->maxMTU                = 0;
    obj->txDescSize            = 0;
    obj->rxDescSize            = 0;
    obj->rxAccListSize         = 0;
    obj->rxAccList             = NULL;
    obj->srioHandle            = NULL;
    obj->hwiHandle             = NULL;
    obj->rxIntVectorId         = 0;
    obj->sockParams.sockType   = TransportSrio_srioSockType_INVALID;
    obj->sockParams.epListSize = 0;
    obj->sockParams.u.pT11Eps  = NULL;
    obj->sockParams.u.pT9Eps   = NULL;
    obj->socketHandle          = NULL;
    obj->fragSrcs              = NULL;
    obj->fragMsgTag            = 0;
    obj->transNetId            = 0;

    if (params->sockParams == NULL) {
        retVal = TransportSrio_ERROR_NULL_SOCKET_PARAMS;
        goto error_cleanup;
    }
    if ((params->sockParams->sockType != TransportSrio_srioSockType_TYPE_11) &&
        (params->sockParams->sockType != TransportSrio_srioSockType_TYPE_9)) {
        retVal = TransportSrio_ERROR_INVALID_SOCKET_TYPE;
        goto error_cleanup;
    }
    if (params->maxMTU > TRANS_SRIO_HW_MTU_BYTES) {
        retVal = TransportSrio_ERROR_INVALID_MAX_MTU;
        goto error_cleanup;
    }

    /* Check if SRIO instance has already been created.  Only one instance of
     * each SRIO Type can exist per DSP core.  Protect from interrupt from
     * another task */
    hwiKey = Hwi_disable();
    if ((params->sockParams->sockType == TransportSrio_srioSockType_TYPE_11) &&
        TransportSrio_module->t11InstCreated) {
        retVal = TransportSrio_ERROR_SRIO_TYPE11_INST_ALREADY_EXISTS;
    }
    if ((params->sockParams->sockType == TransportSrio_srioSockType_TYPE_9) &&
        TransportSrio_module->t9InstCreated) {
        retVal = TransportSrio_ERROR_SRIO_TYPE9_INST_ALREADY_EXISTS;
    }

    if (retVal != TransportSrio_OK) {
        Hwi_restore(hwiKey);
        goto error_cleanup;
    }

    /* Designate creation of specific SRIO instance type */
    obj->sockParams.sockType = params->sockParams->sockType;
    if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_11) {
        TransportSrio_module->t11InstCreated = 1;
    }
    if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_9) {
        TransportSrio_module->t9InstCreated = 1;
    }
    Hwi_restore(hwiKey);

    if (params->deviceCfgParams) {
        /* Store the device configuration parameters */
        obj->deviceCfgParams = params->deviceCfgParams;
    } else {
        retVal = TransportSrio_ERROR_INVALID_DEVICE_CFG_PARAMS;
        goto error_cleanup;
    }

    /* Transmit queue and descriptor initialization - MessageQ buffers will be
     * attached to the descriptors at _put() time to facilitate zero copy */
    obj->txCompletionQ = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE,
                                        QMSS_PARAM_NOT_SPECIFIED,
                                        (UInt8 *)&isAllocated);
    if (obj->txCompletionQ < 0) {
        retVal = TransportSrio_ERROR_COULD_NOT_OPEN_TX_COMP_Q;
        goto error_cleanup;
    }

    memset(&descCfg, 0, sizeof(descCfg));
    descCfg.memRegion             = (Qmss_MemRegion) params->txMemRegion;
    descCfg.descNum               = params->txNumDesc;
    descCfg.destQueueNum          = QMSS_PARAM_NOT_SPECIFIED;
    descCfg.queueType             = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
    descCfg.initDesc              = Cppi_InitDesc_INIT_DESCRIPTOR;
    descCfg.descType              = Cppi_DescType_HOST;
    descCfg.returnQueue           = Qmss_getQueueNumber(obj->txCompletionQ);
    descCfg.epibPresent           = Cppi_EPIB_NO_EPIB_PRESENT;
    descCfg.returnPushPolicy      = Qmss_Location_TAIL;
    descCfg.cfg.host.returnPolicy = Cppi_ReturnPolicy_RETURN_ENTIRE_PACKET;
    descCfg.cfg.host.psLocation   = Cppi_PSLoc_PS_IN_DESC;
    obj->txFreeQ = Cppi_initDescriptor(&descCfg, &isAllocated);
    if (obj->txFreeQ < 0) {
        retVal = TransportSrio_ERROR_TX_DESCRIPTOR_INIT_FAILED;
        goto error_cleanup;
    }

    /* Transmit fragment queue and descriptor initialization - buffers are
     * pre-allocated and attached to the host fragment descriptors. */
    obj->txFragQ = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE,
                                  QMSS_PARAM_NOT_SPECIFIED,
                                  (UInt8 *)&isAllocated);
    if (obj->txFragQ < 0) {
        retVal = TransportSrio_ERROR_COULD_NOT_OPEN_TX_FRAG_Q;
        goto error_cleanup;
    }

    memset(&descCfg, 0, sizeof(descCfg));
    descCfg.memRegion             = (Qmss_MemRegion) params->txMemRegion;
    descCfg.descNum               = params->txNumFragDesc;
    descCfg.destQueueNum          = QMSS_PARAM_NOT_SPECIFIED;
    descCfg.queueType             = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
    descCfg.initDesc              = Cppi_InitDesc_INIT_DESCRIPTOR;
    descCfg.descType              = Cppi_DescType_HOST;
    descCfg.returnQueue           = Qmss_getQueueNumber(obj->txFragQ);
    descCfg.epibPresent           = Cppi_EPIB_NO_EPIB_PRESENT;
    descCfg.returnPushPolicy      = Qmss_Location_HEAD;
    descCfg.cfg.host.returnPolicy = Cppi_ReturnPolicy_RETURN_ENTIRE_PACKET;
    descCfg.cfg.host.psLocation   = Cppi_PSLoc_PS_IN_DESC;
    tmpQ = Cppi_initDescriptor (&descCfg, &isAllocated);
    if (tmpQ < 0) {
        retVal = TransportSrio_ERROR_RX_DESCRIPTOR_INIT_FAILED;
        goto error_cleanup;
    }

    for (i = 0; i < params->txNumFragDesc; i++) {
        /* Attach pre-allocated fragment buffers to the fragment descriptors */
        if (hostDesc = (Cppi_HostDesc *)Qmss_queuePop(tmpQ)) {
            MessageQ_Msg msgQBuf;

            msgQBuf = MessageQ_alloc(params->txMsgQFragHeapId, params->maxMTU);
            if (msgQBuf == NULL) {
                retVal = TransportSrio_ERROR_ALLOC_FROM_TX_FRAG_HEAP_FAILED;
                Qmss_queueEmpty(tmpQ);
                Qmss_queueClose(tmpQ);
                goto error_cleanup;
            }
            /* Convert the buffer to the global address - Only applicable for
             * buffers residing in L2 */
            msgQBuf = (MessageQ_Msg)l2_to_global_addr((UInt32)msgQBuf);

            Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                         (UInt8 *)msgQBuf, params->maxMTU);
            Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                                    (UInt8 *)msgQBuf, params->maxMTU);

            wb((Void *)msgQBuf, msgQBuf->msgSize);
            wb((Void *)hostDesc, params->rxDescSize);

            Qmss_queuePushDescSize(obj->txFragQ, (void *)hostDesc,
                                   params->txDescSize);
        }
    }
    Qmss_queueClose(tmpQ);

    /* Receive queue and descriptor initialization - MessageQ buffers are
     * pre-allocated and attached to the host descriptors.  On packet reception,
     * SRIO's configured flow will copy the data directly into the MessageQ
     * buffer attached to the popped rx descriptor */
    obj->rxFreeQ = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE,
                                  QMSS_PARAM_NOT_SPECIFIED,
                                  (UInt8 *)&isAllocated);
    if (obj->rxFreeQ < 0) {
        retVal = TransportSrio_ERROR_COULD_NOT_OPEN_RX_FREE_Q;
        goto error_cleanup;
    }

    /* Open the receive queue used by the accumulator - queue type does not
     * matter so pass in from application. */
    obj->rxAccumQ = Qmss_queueOpen((Qmss_QueueType) params->rxQType,
                                   QMSS_PARAM_NOT_SPECIFIED,
                                   (UInt8 *) &isAllocated);
    if (obj->rxAccumQ < 0) {
        retVal = TransportSrio_ERROR_COULD_NOT_OPEN_RX_ACCUM_Q;
        goto error_cleanup;
    }

    memset(&descCfg, 0, sizeof(descCfg));
    descCfg.memRegion             = (Qmss_MemRegion) params->rxMemRegion;
    descCfg.descNum               = params->rxNumDesc;
    descCfg.destQueueNum          = QMSS_PARAM_NOT_SPECIFIED;
    descCfg.queueType             = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
    descCfg.initDesc              = Cppi_InitDesc_INIT_DESCRIPTOR;
    descCfg.descType              = Cppi_DescType_HOST;
    descCfg.returnQueue           = Qmss_getQueueNumber(obj->rxFreeQ);
    descCfg.epibPresent           = Cppi_EPIB_NO_EPIB_PRESENT;
    descCfg.returnPushPolicy      = Qmss_Location_HEAD;
    descCfg.cfg.host.returnPolicy = Cppi_ReturnPolicy_RETURN_ENTIRE_PACKET;
    descCfg.cfg.host.psLocation   = Cppi_PSLoc_PS_IN_DESC;
    tmpQ = Cppi_initDescriptor (&descCfg, &isAllocated);
    if (tmpQ < 0) {
        retVal = TransportSrio_ERROR_RX_DESCRIPTOR_INIT_FAILED;
        goto error_cleanup;
    }

    for (i = 0; i < params->rxNumDesc; i++) {
        /* Attach pre-allocated MessageQ buffers to the receive descriptors */
        if (hostDesc = (Cppi_HostDesc *) Qmss_queuePop(tmpQ)) {
            MessageQ_Msg msgQBuf;

            msgQBuf = MessageQ_alloc(params->rxMsgQHeapId, params->maxMTU);
            if (msgQBuf == NULL) {
                retVal = TransportSrio_ERROR_ALLOC_FROM_RX_MESSAGEQ_HEAP_FAILED;
                Qmss_queueEmpty(tmpQ);
                Qmss_queueClose(tmpQ);
                goto error_cleanup;
            }

            /* Convert the buffer to the global address - Only applicable for
             * buffers residing in L2 */
            msgQBuf = (MessageQ_Msg) l2_to_global_addr((UInt32)msgQBuf);

            Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                         (UInt8 *)msgQBuf, params->maxMTU);
            Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                                    (UInt8 *)msgQBuf, params->maxMTU);

            wb((Void *)msgQBuf, msgQBuf->msgSize);
            wb((Void *)hostDesc, params->rxDescSize);

            Qmss_queuePushDescSize(obj->rxFreeQ, (Void *)hostDesc,
                                   params->rxDescSize);
        }
    }

    Qmss_queueClose(tmpQ);

    /* Save receive MessageQ heap ID and the maxMTU size for use in the ISR */
    obj->rxMsgQHeapId = params->rxMsgQHeapId;
    obj->maxMTU = params->maxMTU;
    /* Save the send and receive descriptor lengths for cache coherence
     * operations */
    obj->txDescSize = params->txDescSize;
    obj->rxDescSize = params->rxDescSize;

    /* SRIO driver configuration */
    memset((Void *)&srioCfg, 0, sizeof(srioCfg));
    /* App-managed: handle all aspects of SRIO config and buffer management */
    srioCfg.bAppManagedConfig = TRUE;

    srioCfg.u.appManagedCfg.bIsRxFlowCfgValid = 1;
    /* Configure the receive flow */
    srioCfg.u.appManagedCfg.rxFlowCfg.flowIdNum          = -1;
    queueInfo = Qmss_getQueueNumber(obj->rxAccumQ);
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_qnum       = queueInfo.qNum;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_qmgr       = queueInfo.qMgr;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_sop_offset      = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_ps_location     = 0x0;
    /* Set Host descriptor */
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_desc_type       = 0x1;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_error_handling  = 0x0;
    /* Set PS information */
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_psinfo_present  = 0x1;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_einfo_present   = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_tag_lo     = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_tag_hi     = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_src_tag_lo      = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_src_tag_hi      = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_tag_lo_sel = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_tag_hi_sel = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_src_tag_lo_sel  = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_src_tag_hi_sel  = 0x0;
    /* Disable Receive size thresholds */
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_size_thresh0_en = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_size_thresh1_en = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_size_thresh2_en = 0x0;
    /* Use the rxFreeQ for picking up descriptors */
    queueInfo = Qmss_getQueueNumber(obj->rxFreeQ);
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq1_qnum       = queueInfo.qNum;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq1_qmgr       = queueInfo.qMgr;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq2_qnum       = 0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq2_qmgr       = 0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq3_qnum       = 0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq3_qmgr       = 0;
    /* Use the rxFreeQ for picking the SOP packet also */
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz0_qnum   = queueInfo.qNum;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz0_qmgr   = queueInfo.qMgr;
    /* There are no size thresholds configured */
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_size_thresh0    = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_size_thresh1    = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_size_thresh2    = 0x0;
    /* The other threshold queues do not need to be configured */
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz1_qnum   = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz1_qmgr   = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz2_qnum   = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz2_qmgr   = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz3_qnum   = 0x0;
    srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz3_qmgr   = 0x0;

    /* Populate the rest of the receive configuration. */
    srioCfg.u.appManagedCfg.rawRxFreeDrvBufferArg = transportSrioRxBufRecycle;
    srioCfg.u.appManagedCfg.rawRxFreeDrvBufferFuncArg = (Void *)obj;
    srioCfg.u.appManagedCfg.rxDescSize = obj->rxDescSize;

    /* Program the accumulator. */
    srioCfg.u.appManagedCfg.bIsAccumlatorCfgValid      = 1;
    srioCfg.u.appManagedCfg.accCfg.channel             = params->accumCh;
    srioCfg.u.appManagedCfg.accCfg.command             = Qmss_AccCmd_ENABLE_CHANNEL;
    srioCfg.u.appManagedCfg.accCfg.queueEnMask         = 0;
    srioCfg.u.appManagedCfg.accCfg.queMgrIndex         = QMSS_QUEUE_QID(obj->rxAccumQ);
    srioCfg.u.appManagedCfg.accCfg.maxPageEntries      = TRANS_SRIO_MAX_PAGE_ENTRIES;
    srioCfg.u.appManagedCfg.accCfg.timerLoadCount      = params->accumTimerCount;
    srioCfg.u.appManagedCfg.accCfg.interruptPacingMode = Qmss_AccPacingMode_LAST_INTERRUPT;
    srioCfg.u.appManagedCfg.accCfg.listEntrySize       = Qmss_AccEntrySize_REG_D;
    srioCfg.u.appManagedCfg.accCfg.listCountMode       = Qmss_AccCountMode_ENTRY_COUNT;
    srioCfg.u.appManagedCfg.accCfg.multiQueueMode      = Qmss_AccQueueMode_SINGLE_QUEUE;

    /* Init accumulator list */
    obj->rxAccListSize = TRANS_SRIO_MAX_PAGE_ENTRIES *
                         sizeof(Cppi_HostDesc *) * 2; /* x2 for ping/pong */
    obj->rxAccList = (UInt32 *)l2_to_global_addr((UInt32)Memory_alloc(0,
                                                  obj->rxAccListSize, 16, eb));
    if (obj->rxAccList == NULL) {
        retVal = TransportSrio_ERROR_ALLOC_OF_ACCUM_LIST_FAILED;
        goto error_cleanup;
    }
    memset(obj->rxAccList, 0, obj->rxAccListSize);
    srioCfg.u.appManagedCfg.accCfg.listAddress = (UInt32) obj->rxAccList;

    /* Specify that Srio queue allocation is up to the driver */
    srioCfg.u.appManagedCfg.txQueueNum = QMSS_PARAM_NOT_SPECIFIED;

    /* Provide service RM handle to SRIO.  SRIO will manage own resources
     * if service handle is NULL */
    srioCfg.rmServiceHandle = params->rmServiceHandle;

    obj->srioHandle = Srio_start(&srioCfg);
    if (obj->srioHandle == NULL) {
        retVal = TransportSrio_ERROR_STARTING_SRIO_DRIVER;
        goto error_cleanup;
    }

    /* Configure and register interrupt */
    if ((params->accumCh > obj->deviceCfgParams->numAccumCh) ||
        ((isrEventId = obj->deviceCfgParams->accChToEventMap[(DNUM *
                                            obj->deviceCfgParams->numAccumCh) +
                                            params->accumCh]) == 0)) {
        retVal = TransportSrio_ERROR_INVALID_ACCUMULATOR_CH;
        goto error_cleanup;
    }
    Hwi_Params_init(&hwiAttrs);
    hwiAttrs.maskSetting = Hwi_MaskingOption_SELF;
    hwiAttrs.arg         = (UArg) obj;
    hwiAttrs.eventId     = isrEventId;
    obj->hwiHandle = (Void *) Hwi_create(params->rxIntVectorId,
                                         (Hwi_FuncPtr) TransportSrio_isr,
                                         &hwiAttrs, eb);
    Hwi_enableInterrupt(params->rxIntVectorId);
    obj->rxIntVectorId = params->rxIntVectorId;

    /* Open a raw, non-blocking socket of the specified type and bind it */
    if (params->sockParams->sockType == TransportSrio_srioSockType_TYPE_11) {
        socketType = Srio_SocketType_RAW_TYPE11;
    } else if (params->sockParams->sockType == TransportSrio_srioSockType_TYPE_9) {
        socketType = Srio_SocketType_RAW_TYPE9;
    }

    obj->socketHandle = Srio_sockOpen(obj->srioHandle, socketType, FALSE);
    if (obj->socketHandle == NULL) {
        retVal = TransportSrio_ERROR_SOCK_OPEN_FAILED;
        goto error_cleanup;
    }

    if (ipcCoreNum >= params->sockParams->epListSize) {
        retVal = TransportSrio_ERROR_INVALID_SRIO_SOCKET_LIST_SIZE;
        goto error_cleanup;
    }

    if (params->sockParams->sockType == TransportSrio_srioSockType_TYPE_11) {
        bindInfo.type11.tt     = params->sockParams->u.pT11Eps[ipcCoreNum].tt;
        bindInfo.type11.id     = params->sockParams->u.pT11Eps[ipcCoreNum].deviceId;
        bindInfo.type11.letter = params->sockParams->u.pT11Eps[ipcCoreNum].letter;
        bindInfo.type11.mbox   = params->sockParams->u.pT11Eps[ipcCoreNum].mailbox;
        bindInfo.type11.segMap = params->sockParams->u.pT11Eps[ipcCoreNum].segMap;
    } else if (params->sockParams->sockType == TransportSrio_srioSockType_TYPE_9) {
        bindInfo.type9.tt       = params->sockParams->u.pT9Eps[ipcCoreNum].tt;
        bindInfo.type9.id       = params->sockParams->u.pT9Eps[ipcCoreNum].deviceId;
        bindInfo.type9.cos      = params->sockParams->u.pT9Eps[ipcCoreNum].cos;
        bindInfo.type9.streamId = params->sockParams->u.pT9Eps[ipcCoreNum].streamId;
    }

    if (Srio_sockBind(obj->socketHandle, &bindInfo) < 0) {
        retVal = TransportSrio_ERROR_SOCKET_BIND_FAILED;
        goto error_cleanup;
    }

    /* Set socket receive queue to be number of descriptors RX descriptors
     * to avoid socket receive overflow */
    if (Srio_setSockOpt(obj->socketHandle, Srio_Opt_PENDING_PKT_COUNT,
                        (Void *) &params->rxNumDesc, sizeof(UInt16))) {
        retVal = TransportSrio_ERROR_COULD_SET_SOCKET_OPTION;
        goto error_cleanup;
    }

    /* Save socket parameters */
    obj->sockParams.sockType = params->sockParams->sockType;
    obj->sockParams.epListSize = params->sockParams->epListSize;
    if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_11) {
        obj->sockParams.u.pT11Eps = Memory_alloc(0, obj->sockParams.epListSize *
            sizeof(TransportSrio_srioSockType11EpParams), 0, eb);
        memcpy(obj->sockParams.u.pT11Eps, params->sockParams->u.pT11Eps,
               obj->sockParams.epListSize *
               sizeof(TransportSrio_srioSockType11EpParams));
    } else if (obj->sockParams.sockType == TransportSrio_srioSockType_TYPE_9) {
        obj->sockParams.u.pT9Eps = Memory_alloc(0, obj->sockParams.epListSize *
            sizeof(TransportSrio_srioSockType9EpParams), 0, eb);
        memcpy(obj->sockParams.u.pT9Eps, params->sockParams->u.pT9Eps,
               obj->sockParams.epListSize *
               sizeof(TransportSrio_srioSockType9EpParams));
    }

    /* Alloc and init a fragment tracking structure for each T11 & T9
     * endpoint */
    obj->fragSrcs = Memory_alloc(0, obj->sockParams.epListSize *
                                 sizeof(transFragEp_t), 16, eb);
    if (obj->fragSrcs == NULL) {
        retVal = TransportSrio_ERROR_ALLOC_OF_FRAG_SOURCE_LIST;
        goto error_cleanup;
    }
    memset(obj->fragSrcs, 0, obj->sockParams.epListSize *
           sizeof(transFragEp_t));

    if ((params->transNetworkId < 1) && (params->transNetworkId > 7)) {
        retVal = TransportSrio_ERROR_INVALID_TRANSPORT_ID;
        goto error_cleanup;
    }
    obj->transNetId = params->transNetworkId;

    /* Register the transport with MessageQ */
    if (MessageQ_registerTransportId(obj->transNetId,
        ti_transport_ipc_c66_srio_TransportSrio_Handle_upCast2(obj)) == FALSE) {
        retVal = TransportSrio_ERROR_COULD_NOT_REGISTER_TRANSPORT;
        goto error_cleanup;
    }

    return(retVal);

error_cleanup:
    /* No need to cleanup on error here.  Specifying an error return value
     * will cause RTSC wrapper to invoke TransportSrio_Instance_finalize */
    Error_raise(eb, retVal, 0, 0);
    return(retVal);
}

/*
 *  ======== TransportSrio_Instance_finalize ========
 */
Void TransportSrio_Instance_finalize(TransportSrio_Object* obj, Int status)
{
    TransportSrio_srioSockType sockType = obj->sockParams.sockType;

    if ((obj->transNetId >= 1) && (obj->transNetId <= 7)) {
        MessageQ_unregisterTransportId(obj->transNetId);
    }

    transportSrioInstCleanup(obj);
    transportSrioResetModuleParams(sockType, status);
}

/*
 *  ======== TransportSrio_put ========
 *  Routine used to send packets via SRIO driver
 */
Bool TransportSrio_put(TransportSrio_Object *obj, Ptr msg)
{
    Srio_SockAddrInfo dstSrioAddr;
    UInt16            dstProcId = (UInt16)(((MessageQ_Msg)(msg))->dstProc);
    UInt32            msgQDataLen = MessageQ_getMsgSize(msg);

    /* Find destination SRIO endpoint address based on remote
     * MultiProc ID */
    switch (obj->sockParams.sockType) {
        case TransportSrio_srioSockType_TYPE_11: {
            TransportSrio_srioSockType11EpParams *t11;

            t11 = obj->sockParams.u.pT11Eps;
            dstSrioAddr.type11.tt     = t11[dstProcId].tt;
            dstSrioAddr.type11.id     = t11[dstProcId].deviceId;
            dstSrioAddr.type11.letter = t11[dstProcId].letter;
            dstSrioAddr.type11.mbox   = t11[dstProcId].mailbox;
            break;
        }
        case TransportSrio_srioSockType_TYPE_9: {
            TransportSrio_srioSockType9EpParams *t9;

            t9 = obj->sockParams.u.pT9Eps;
            dstSrioAddr.type9.tt       = t9[dstProcId].tt;
            dstSrioAddr.type9.id       = t9[dstProcId].deviceId;
            dstSrioAddr.type9.cos      = t9[dstProcId].cos;
            dstSrioAddr.type9.streamId = t9[dstProcId].streamId;
            break;
        }
        default:
            Assert_isTrue(0, TransportSrio_A_putInvalidSockType);
            return(FALSE);
    }

    if (msgQDataLen >= (obj->maxMTU)) {
        /* Fragment packet.  Fragments will always be SRIO max MTU in size.
         * Last fragment will be padded.  This is to distinguish fragmented
         * packets from non-fragmented. */
        return(transportSrioFragmentSend(obj, msg, &dstSrioAddr));
    } else {
        return(transportSrioStandardSend(obj, msg, &dstSrioAddr));
    }
}

/* bind - not used */
Int TransportSrio_bind(TransportSrio_Object *obj, UInt32 queueId)
{
    /* 0 is failure according to MessageQ_Instance_init */
    return(1);
}

/* unbind - not used */
Int TransportSrio_unbind(TransportSrio_Object *obj, UInt32 queueId)
{
    /* 0 is failure according to MessageQ_Instance_init */
    return(1);
}

/*
 *************************************************************************
 *                      Module functions
 *************************************************************************
 */

Int TransportSrio_recycleUsedTxBufs(TransportSrio_Handle handle)
{
    return(transportSrioTxBufRecycle((TransportSrio_Object *)handle,
                                     TRANS_SRIO_RECYCLE_ALL_TX_BUFS));
}

/*
 *************************************************************************
 *                       Internal functions
 *************************************************************************
 */

/*
 *  ======== TransportSrio_isr ========
 */
Void TransportSrio_isr(UArg arg)
{
    TransportSrio_Object *obj = (TransportSrio_Object *)arg;
    Cppi_HostDesc        *hostDesc;
    Srio_SockAddrInfo     srcSocket;

    Assert_isTrue(obj->srioHandle != NULL, TransportSrio_A_invalidSrioHandle);

    /* Invoke SRIO completion ISR to move received packets to destination
     * sockets.  ISR ACK handled here */ 
    Srio_rxCompletionIsr((Srio_DrvHandle) obj->srioHandle);

    while(Srio_sockRecv(obj->socketHandle, (Srio_DrvBuffer*)&hostDesc,
                        &srcSocket)) {
        MessageQ_Msg rxMsg;
        UInt32       rxNumBytes;
        UInt32       queueId;

        /* Extract and invalidate MessageQ msg.  The descriptor doesn't need to
         * be invalidated prior to this operation since Srio_rxCompletionIsr()
         * does this */
        Cppi_getData(Cppi_DescType_HOST, (Cppi_Desc *)hostDesc,
                     (UInt8 **)&rxMsg, (UInt32 *)&rxNumBytes);
        inv((Void *) rxMsg, rxNumBytes);

        if (rxNumBytes == obj->maxMTU) {
            /* Fragmented packet */
            rxMsg = transportSrioFragmentRcv(obj, rxMsg, &srcSocket);
        } else {
            /* Reset MessageQ msgSize to what was allocated above for
             * standard packet */
            rxMsg->msgSize = obj->maxMTU;
        }

        if (rxMsg) {
            /* Reset heapId to rxMsgQHeapId.  This heap is overwritten when
             * message is received over SRIO */
            rxMsg->heapId = obj->rxMsgQHeapId;

            /* Route to destination MessageQ */
            queueId = MessageQ_getDstQueue(rxMsg);
            MessageQ_put(queueId, rxMsg);
        }

        /* Get new MessageQ buffer for descriptor */
        transportSrioNewRxBuf(obj, hostDesc);
    }
}

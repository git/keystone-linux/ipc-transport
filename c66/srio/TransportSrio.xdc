/* File: TransportSrio.xdc
 *
 * Purpose: TransportSrio RTSC module specification.  The module error codes
 *          and initialization parameters are specified here
 *
 * Copyright (c) 2011-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 *  ======== TransportSrio.xdc ========
 */
import xdc.runtime.Assert;
import xdc.rov.ViewInfo;

import ti.sdo.utils.MultiProc;
import ti.sdo.ipc.MessageQ;

/*!
 *  ======== TransportSrio ========
 *  Transport for MessageQ that acts with SRIO.
 */
@InstanceFinalize
@InstanceInitError

module TransportSrio inherits ti.sdo.ipc.interfaces.INetworkTransport
{
    /*!
     *  ======== A_socketSendError ========
     *  An error occurred when the SRIO driver attamped to transmit the
     *  packet.
     */
    config Assert.Id A_socketSendError  = {
        msg: "A_socketSendError: Error occurred during Srio_sockSend operation."
    };

    /*!
     *  ======== A_putInvalidSockType ========
     *  The transport instance passed to TransportSrio_put() has an invalid
     *  socket type.
     */
    config Assert.Id A_putInvalidSockType  = {
        msg: "A_putInvalidSockType: Invalid socket type in TransportSrio_put."
    };

    /*!
     *  ======== A_txMsgQFreeError ========
     *  The MessageQ message attached to a transmit descriptor could not be 
     *  freed back to MessageQ.
     */
    config Assert.Id A_txMsgQFreeError  = {
        msg: "A_txMsgQFreeError: Could not free transmit MessageQ buffer."
    };

    /*!
     *  ======== A_rxMsgQFreeError ========
     *  The MessageQ message attached to a receive descriptor could not be
     *  freed back to MessageQ.
     */
    config Assert.Id A_rxMsgQFreeError  = {
        msg: "A_rxMsgQFreeError: Could not free receive MessageQ buffer."
    };

    /*!
     *  ======== A_invalidSrioHandle ========
     *  The SRIO handle passed to the ISR is NULL
     */
    config Assert.Id A_invalidSrioHandle  = {
        msg: "A_invalidSrioHandle: SRIO handle in ISR is NULL."
    };

    /*!
     *  ======== A_newRxBufNull ========
     *  The newly allocated receive MessageQ buffer is NULL
     */
    config Assert.Id A_newRxBufNull  = {
        msg: "A_newRxBufNull: New allocated receive buffer is NULL."
    };

    /*! Transport operation completed successfully */
    const Int OK                                       = 0;
    /*! QMSS returned an error when attempting to allocate the transmit
     *  completion queue */
    const Int ERROR_COULD_NOT_OPEN_TX_COMP_Q           = 64;
    /*! CPPI returned an error when attempting to initalize the tx descriptors
     *  and allocate the transmit free queue */
    const Int ERROR_TX_DESCRIPTOR_INIT_FAILED          = 65;
    /*! QMSS returned an error when attempting to allocate the receive free
     *  queue */
    const Int ERROR_COULD_NOT_OPEN_RX_FREE_Q           = 66;
    /*! QMSS returned an error when attempting to allocate the receive queue
     *  used by the accumulator */
    const Int ERROR_COULD_NOT_OPEN_RX_ACCUM_Q          = 67;
    /*! CPPI returned an error when attempting to initalize the rx
     *  descriptors */
    const Int ERROR_RX_DESCRIPTOR_INIT_FAILED          = 68;
    /*! MessageQ returned a NULL buffer when pre-allocating buffers for the
     *  SRIO receive descriptors */
    const Int ERROR_ALLOC_FROM_RX_MESSAGEQ_HEAP_FAILED = 69;
    /*! A NULL buffer was returned when trying to allocate the accumulator list
     *  ping/pong buffer */
    const Int ERROR_ALLOC_OF_ACCUM_LIST_FAILED         = 70;
    /*! An error occurred within the SRIO driver when opening a SRIO handle via
     *  Srio_start() */
    const Int ERROR_STARTING_SRIO_DRIVER               = 71;
    /*! An error occurred when trying to open the SRIO socket */
    const Int ERROR_SOCK_OPEN_FAILED                   = 72;
    /*! The DSP core's MultiProc ID is greater than the endpoint parameter list
     *  size provided in the sockParams structure */
    const Int ERROR_INVALID_SRIO_SOCKET_LIST_SIZE      = 73;
    /*! An error occurred when trying to bind the SRIO socket */
    const Int ERROR_SOCKET_BIND_FAILED                 = 74;
    /*! An error occurred when trying to set the SRIO socket options so that
     *  the socket receive queue was equivalent to the number of receive
     *  descriptors */
    const Int ERROR_COULD_SET_SOCKET_OPTION            = 75;
    /*! Invalid network transport ID.  The transport ID can have a value
     *  between 1 and 7 */
    const Int ERROR_INVALID_TRANSPORT_ID               = 76;
    /*! The SRIO transport instance could not be registered with MessageQ */
    const Int ERROR_COULD_NOT_REGISTER_TRANSPORT       = 77;
    /*! The given accumulator channel does not map to a DSP GEM event on this
     *  core.  Please reference the Multicore Navigator spec for the proper DSP
     *  core to channel mappings on the device */
    const Int ERROR_INVALID_ACCUMULATOR_CH             = 78;
    /*! The transport configuration parameter, sockType, contains an invalid
     *  socket type.  Valid socket types are specified within the srioSockType
     *  enum */
    const Int ERROR_INVALID_SOCKET_TYPE                = 79;
    /*! A deviceCfgParams structure pointer provided as part of the
     *  TransportSrio_create initialization parameters is invalid */
    const Int ERROR_INVALID_DEVICE_CFG_PARAMS          = 80;
    /*! NULL was provided for the sockParams pointer.  The transport instance
     *  cannot be created without socket parameters */
    const Int ERROR_NULL_SOCKET_PARAMS                 = 81;
    /*! An attempt to open a second TransportSrio Type 11 instance.  Only one
     *  TransportSrio Type 11 instance can be created per DSP core */
    const Int ERROR_SRIO_TYPE11_INST_ALREADY_EXISTS    = 82;
    /*! An attempt to open a second TransportSrio Type 9 instance.  Only one
     *  TransportSrio Type 9 instance can be created per DSP core */
    const Int ERROR_SRIO_TYPE9_INST_ALREADY_EXISTS     = 83;
    /*! QMSS returned an error when attempting to allocate the transmit
     *  fragment queue */
    const Int ERROR_COULD_NOT_OPEN_TX_FRAG_Q           = 84;
    /*! BIOS returned a NULL buffer when pre-allocating buffers for the
     *  SRIO transmit fragment descriptors */
    const Int ERROR_ALLOC_FROM_TX_FRAG_HEAP_FAILED     = 85;
    /*! The configured maxMTU is greater than what SRIO hardware can handle,
     *  4096 bytes */
    const Int ERROR_INVALID_MAX_MTU                    = 86;
    /*! Must be at least one source specified for interleaving in order to
     *  fragmentation/reassembly from one SRIO source */
    const Int ERROR_INVALID_NUM_INTERLEAVED_SRCS       = 87;
    /*! There was an error allocating the memory for the fragmentation source
     *  list */
    const Int ERROR_ALLOC_OF_FRAG_SOURCE_LIST          = 88;

    /*!
     *  Device specific configuration parameters for the SRIO transport.
     *
     *  @p(blist)
     * -numDspCores     -> Number of DSP cores on the device
     * -numAccumCh      -> Number of accumulator channels supported
     * -accChToEventMap -> Pointer to Accumulator channel to GEM event mapping
     *                     table
     *  @p
    */
    struct DeviceConfigParams {
        UInt16  numDspCores;
        UInt16  numAccumCh;
        UInt8  *accChToEventMap;
    };

    /*!
     *  SRIO transport instance socket types. These values are passed as a
     *  configuration parameter when creating a new SRIO transport.  They
     *  define the type of SRIO socket opened */
    enum srioSockType {
        srioSockType_INVALID,
        srioSockType_TYPE_9,
        srioSockType_TYPE_11
    };

    /*!
     *  A set of SRIO Type 11 socket endpoint parameters used to define a list
     *  of unique SRIO Type 11 endpoints in the system.
     *
     *  @p(blist)
     * -tt       -> Defines the number of bits in the device ID
     * -deviceId -> Endpoint device ID
     * -letter   -> Endpoint letter
     * -mailbox  -> Endpoint mailbox
     * -segMap   -> Endpoint segmentation map
     *  @p
    */
    struct srioSockType11EpParams {
        UInt16 tt;
        UInt16 deviceId;
        UInt16 letter;
        UInt16 mailbox;
        UInt16 segMap;
    };

    /*!
     *  A set of SRIO Type 9 socket endpoint parameters used to define a list
     *  of unique SRIO Type 9 endpoints in the system.
     *
     *  @p(blist)
     * -tt       -> Defines the number of bits in the device ID
     * -deviceId -> Endpoint device ID
     * -cos      -> Endpoint class of service
     * -streamId -> Endpoint stream identifier
     *  @p
    */
    struct srioSockType9EpParams {
        UInt16 tt;
        UInt16 deviceId;
        UInt8  cos;
        UInt16 streamId;
    };

    /*!
     *  SRIO socket parameters used to define the type of socket that will be
     *  bound by the transport as well as define a list of unique SRIO
     *  endpoints in the system.
     *
     *  @p(blist)
     * -sockType   -> The type of SRIO socket that will be opened and bound by
     *                the transport
     * -epListsize -> The number of entries in provided Type 9 or 11 endpoint
     *                parameter list.  This number must be equal to the number
     *                of unique SRIO endpoints in the system, which is equal to
     *                the number DSP cores in the system interconnect according
     *                to IPC's MultiProc module.
     * -pT11Eps    -> Pointer to list of Type 11 socket parameters for each
     *                system endpoint if sockType = Type 11.  The list must be
     *                sized equal to the number of endpoints in the system.
     *                The list will be indexed according to the IPC MultiProc
     *                core ID.
     * -pT9Eps     -> Pointer to list of Type 9 socket parameters for each
     *                system endpoint if sockType = Type 9.  The list must be
     *                sized equal to the number of endpoints in the system.
     *                The list will be indexed according to the IPC MultiProc
     *                core ID.
     *  @p
    */
    struct srioSockParams {
        srioSockType sockType;
        UInt16       epListSize;
        union {
            srioSockType11EpParams *pT11Eps;
            srioSockType9EpParams  *pT9Eps;
        } u;
    };

instance:

    /*!
     *  ======== deviceCfgParams ========
     *  Pointer to the device specific SRIO transport configuration parameters
     */
    config DeviceConfigParams *deviceCfgParams = null;

    /*!
     *  ======== txMemRegion ========
     *  QMSS memory region from which the transmit host descriptors will be
     *  allocated from.  Enough descriptors must be in this region to
     *  accommodate the standard tx descriptors and the tx fragment descriptors
     */
    config Int                 txMemRegion = -1;

    /*!
     *  ======== txNumDesc ========
     *  Number of host descriptors to pre-allocate for SRIO transmit operations
     */
    config UInt32              txNumDesc = 2;

    /*!
     *  ======== txNumFragDesc ========
     *  Number of host descriptors to pre-allocate for SRIO transmit
     *  fragmented packet operations.  Maximum needed is 64k/maxMTU (maxMTU
     *  can be no greater than 4096) but more can be configured without error.
     *  A smaller number will cause the fragment logic to wait for sent
     *  fragments to be recycled before sending remaining fragments of a large
     *  packet.
     */
    config UInt32              txNumFragDesc = 1;

    /*!
     *  ======== txDescSize ========
     *  Size of the transmit descriptors.  This size is used for cache coherence
     *  operations and should be a multiple a cache line.
     */
    config UInt32              txDescSize = 0;

    /*!
     *  ======== txFragHeap ========
     *  MessageQ heap ID for the heap from which the SRIO transmit fragmented
     *  packet are to be allocated.  The heap must contain buffers at least
     *  4096 bytes in size and there must be at least txNumFragDesc number of
     *  buffers
     */
    config UInt16              txMsgQFragHeapId = ~1u;
    
    /*!
     *  ======== rxQType ========
     *  The QMSS queue type that will be opened and used with the receive
     *  accumulator.  The queue type does not matter.  Accumulator GEM events
     *  are mapped to the accumulator channels, not the queue type+value.
     */
    config UInt32              rxQType = 0;

    /*!
     *  ======== rxMemRegion ========
     *  Qmss memory region from which the receive host descriptors will be
     *  allocated from.
     */
    config Int                 rxMemRegion = -1;

    /*!
     *  ======== rxNumDesc ========
     *  Number of host descriptors to pre-allocate for SRIO receive operations
     */
    config UInt32              rxNumDesc = 1;

    /*!
     *  ======== rxDescSize ========
     *  Size of the receive descriptors.  This size is used for cache coherence
     *  operations and should be a multiple a cache line.
     */
    config UInt32              rxDescSize = 0;

    /*!
     *  ======== rxMsgQHeapId ========
     *  Rx-side MessageQ Heap ID.  MessageQ buffers are pre-allocated out of
     *  this heap and attached to descriptors for packets received through the
     *  SRIO interface.
     */
    config UInt16              rxMsgQHeapId = ~1u;

    /*!
     *  ======== maxMTU ========
     *  Maximum transmittable unit in bytes that will be handled by the SRIO
     *  transport.  Used to handle cache coherence operations and pre-allocation
     *  of receive side MessageQ buffers.  Can be no greater than 4096 bytes.
     *  Any MessageQ packet equal to, or greater than, maxMTU - 4 bytes
     *  (fragmentation header) will be fragmented when sent over the SRIO
     *  interface.
     */
    config UInt32              maxMTU = 256;

    /*!
     *  ======== accumCh ========
     *  The accumulator channel used for SRIO packet reception.  The GEM event
     *  for the accumulator interrupt will be derived from the provided
     *  accumulator channel and the DSP core number.
     */
    config UInt8               accumCh = 0;

    /*!
     *  ======== accumTimerCount ========
     *  Number of global timer ticks to delay the periodic accumulator
     *  interrupt.  A value of zero will cause an interrupt immediately upon a
     *  descriptor being placed in the accumulator ping/pong buffer.
     */
    config UInt32              accumTimerCount = 0;

    /*!
     *  ======== rmServiceHandle ========
     *  RM service handle that will be given to Srio_start.  The RM service
     *  handle only needs to be provided if the intent is for RM to manage
     *  SRIO resources
     */
    config Void               *rmServiceHandle = null;
    
    /*!
     *  ======== rxIntVectorId ========
     *  Interrupt vector ID to tie to the receive side accumulator operation
     */
    config UInt                rxIntVectorId = ~1u;

    /*!
     *  ======== *sockParams ========
     *  Pointer to socket parameters used by the SRIO transport bind the socket
     *  and to route messages to the proper endpoints.
     */
    config srioSockParams     *sockParams = null;

    /*!
     *  ======== transNetworkId ========
     *  The transport instance will be registered with MessageQ as the supplied
     *  network transport ID.  MessageQ messages with a matching transport ID in
     *  their MessageQ header will be sent over the transport instance.
     *
     *  The MessageQ transport network ID can have a value between 1 and 7.
     */
    config UInt                transNetworkId = 0;

    /*!
     *  ======== recycleUsedTxBufs ========
     *  Calling this function will recycle all descriptors and buffers in the
     *  transmit completion Q.  The _put() process attempts to recycle one
     *  descriptor and buffer at a time.  However, this functionality may
     *  fall behind the number of descriptors in the completion queue if many
     *  packets are sent in a short period of time.  Use this function to
     *  clean backed up descriptors in the transmit completion queue.
     *
     *  Returns the number of buffers recycled
     */
    Int recycleUsedTxBufs();

internal:

    /*!
     *  Handles SRIO packet reception and routing to destination MessageQs
     */
    Void isr(UArg arg);

    /* Per core state information */
    struct Module_State {
        UInt t11InstCreated;
        UInt t9InstCreated;
    }

    /* Instance State object - Per transport instance (multiple per core if
     *                         multiple connections) */
    struct Instance_State {
        DeviceConfigParams *deviceCfgParams;
        Int32               txCompletionQ;
        Int32               txFreeQ;
        Int32               txFragQ;
        Int32               rxFreeQ;
        Int32               rxAccumQ;
        UInt16              rxMsgQHeapId;
        UInt32              maxMTU;
        UInt32              txDescSize;
        UInt32              rxDescSize;
        UInt32              rxAccListSize;
        UInt32             *rxAccList;
        Void               *srioHandle;
        Void               *hwiHandle;
        UInt32              rxIntVectorId;
        srioSockParams      sockParams;
        Void               *socketHandle;
        Void               *fragSrcs;
        UInt8               fragMsgTag;
        UInt16              transNetId;
    }
}


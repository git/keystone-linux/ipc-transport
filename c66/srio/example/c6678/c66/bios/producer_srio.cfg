/* 
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

var Task      = xdc.useModule('ti.sysbios.knl.Task'); 
var Semaphore = xdc.useModule('ti.sysbios.knl.Semaphore');
var System    = xdc.useModule('xdc.runtime.System');
var SysStd = xdc.useModule('xdc.runtime.SysStd');
System.SupportProxy = SysStd;

var BIOS = xdc.useModule('ti.sysbios.BIOS');
BIOS.heapSize = 0x10000;
/* BIOS.libType = BIOS.LibType_Debug; */ /* Uncomment to debug step BIOS and
                                          * IPC code from CCS */

/* Load and use the CSL, CPPI, QMSS, RM and SRIO packages */
var devType = "c6678"
var Csl = xdc.useModule('ti.csl.Settings');
Csl.deviceType = devType;
var Cppi = xdc.loadPackage('ti.drv.cppi'); 
var Qmss = xdc.loadPackage('ti.drv.qmss');
var Srio = xdc.loadPackage('ti.drv.srio');
var Rm   = xdc.loadPackage('ti.drv.rm');

Program.sectMap[".qmss"] = new Program.SectionSpec();
Program.sectMap[".qmss"] = "MSMCSRAM";

Program.sectMap[".cppi"] = new Program.SectionSpec();
Program.sectMap[".cppi"] = "MSMCSRAM";

Program.sectMap[".desc"] = new Program.SectionSpec();
Program.sectMap[".desc"] = "MSMCSRAM";

Program.sectMap[".sharedGRL"] = new Program.SectionSpec();
Program.sectMap[".sharedGRL"] = "L2SRAM";

Program.sectMap[".sharedPolicy"] = new Program.SectionSpec();
Program.sectMap[".sharedPolicy"] = "L2SRAM";

Program.sectMap[".srioSharedMem"] = new Program.SectionSpec();
Program.sectMap[".srioSharedMem"] = "MSMCSRAM";

Program.sectMap[".sync"] = new Program.SectionSpec();
Program.sectMap[".sync"] = "MSMCSRAM";

var Ipc = xdc.useModule('ti.sdo.ipc.Ipc');
/* Synchronize all processors (this will be done in Ipc_start using
 * TransportShmNotify transport) */
Ipc.procSync = Ipc.ProcSync_ALL;
var MessageQ = xdc.useModule('ti.sdo.ipc.MessageQ');
MessageQ.numReservedEntries = 1;
var TransportSrio = xdc.useModule('ti.transport.ipc.c66.srio.TransportSrio');

var NotifyDriverCirc = xdc.useModule('ti.sdo.ipc.notifyDrivers.NotifyDriverCirc');
var Interrupt = xdc.useModule('ti.sdo.ipc.family.c647x.Interrupt');
NotifyDriverCirc.InterruptProxy = Interrupt;

/*  Notify brings in the ti.sdo.ipc.family.Settings module, which does
 *  lots of config magic which will need to be UNDONE later, or setup
 *  earlier, to get the necessary overrides to various IPC module proxies!
 */
var Notify = xdc.module('ti.sdo.ipc.Notify');
var Ipc = xdc.useModule('ti.sdo.ipc.Ipc');

/* Note: Must call this to override what's done in Settings.xs ! */
Notify.SetupProxy = xdc.module('ti.sdo.ipc.family.c647x.NotifyCircSetup');

/* Set to disable error printouts */
/* var Error = xdc.useModule('xdc.runtime.Error'); */
/* Error.raiseHook = null; */

var MultiProc = xdc.useModule('ti.sdo.utils.MultiProc');
/* Cluster definitions - Example has two clusters, one for each device.  Each cluster
 * has two DSPs within it.
 * Producer device [Cluster Base ID: 0] - 2 DSPs (Procs)
 * Consumer device [Cluster Base ID: 2] - 2 DSPs (Procs)
 * Total of 4 DSPs (Procs) */
MultiProc.numProcessors = 4;
/* baseIdOfCluster and numProcessors must be set BEFORE setConfig is run */
MultiProc.numProcsInCluster = 2;
MultiProc.baseIdOfCluster = 0;
var procNameList = ["CORE0", "CORE1"];
MultiProc.setConfig(null, procNameList);

var MultiProcSetup = xdc.useModule('ti.sdo.ipc.family.c647x.MultiProcSetup');
MultiProcSetup.configureProcId = true;

Program.global.DEVICENAME = Program.cpu.deviceName;
Program.global.PROCNAMES = procNameList.join(",");
Program.global.BUILDPROFILE = Program.build.profile;

var HeapBufMP   = xdc.useModule('ti.sdo.ipc.heaps.HeapBufMP');
var SharedRegion = xdc.useModule('ti.sdo.ipc.SharedRegion');
SharedRegion.translate = false;
SharedRegion.setEntryMeta(0,
    { base: 0x0C000000, 
      len: 0x00300000,
      ownerProcId: MultiProc.baseIdOfCluster + 1,  /* Needs to be global core ID of DSP Core 0 */
      isValid: true,
      cacheEnable: true,
      cacheLineSize: 128,  /* Allocated messages aligned to cache line */
      name: "internal_shared_mem",
    });


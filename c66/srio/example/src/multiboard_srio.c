/**
 * @file  multiBoard_srio.c
 *
 * @brief   
 *    Example application that sends data between two boards over the SRIO
 *    IPC transport.  One board acts as the data producer and the other as
 *    the data consumer.  Source code for both the producer and
 *    consumer is contained within this source file.  Supply the following
 *    flags at compile time to build each endpoint:
 *       - MULTIBOARD_PRODUCER
 *       - MULTIBOARD_CONSUMER
 *    An error will be thrown immediately within main() if the target is 
 *    built with neither compile flag.
 *
 * Copyright (c) 2011-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>

/* XDC include */
#include <xdc/runtime/System.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Error.h>

/* BIOS6 include */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* IPC include */
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/GateMP.h>
#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/SharedRegion.h>

/* CSL include */
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_pscAux.h>
#include <ti/csl/csl_chip.h>

/* QMSS LLD */
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/qmss/qmss_firmware.h>

/* CPPI LLD */
#include <ti/drv/cppi/cppi_drv.h>

/* SRIO LLD */
#include <ti/drv/srio/srio_drv.h>

/* RM LLD */
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_services.h>

/* Transport SRIO includes */
#include <ti/transport/ipc/c66/srio/TransportSrio.h>

#include <ti/transport/ipc/c66/example/common_src/bench_common.h>

/************************ USER DEFINES ********************/

#define RM_PRINT_STATS        0
#define SYSINIT               0
#define NUM_LOCAL_DSP_CORES   2
#if (defined(DEVICE_C6657) || defined(DEVICE_C6678))
#define NUM_TOTAL_CORES       4
#else
#define NUM_TOTAL_CORES       6
#endif
#define TEST_ITERATIONS_STD   3
#define TEST_ITERATIONS_FRAG  3
#define TEST_ITERATIONS_TOT   (TEST_ITERATIONS_STD + TEST_ITERATIONS_FRAG)
#if defined(DEVICE_C6657)
#define NUM_TEST_MSGS         8
#else
#define NUM_TEST_MSGS         50
#endif

#if (defined(DEVICE_C6657) || defined(DEVICE_C6678))
#define IPC_CLUSTER_OFFSET    2
#else
#define IPC_CLUSTER_OFFSET    3
#endif

#define HOST_DESC_SIZE_BYTES  128
#if defined(DEVICE_C6657)
#define HOST_DESC_NUM         128
#define TRANS_RCV_DESC        24
#else
#define HOST_DESC_NUM         512
#define TRANS_RCV_DESC        120
#endif
#define TRANS_SEND_DESC       4
#define TRANS_FRAG_DESC       4
#define HOST_DESC_MEM_REGION  0

/* Number of HeapBufMP buffers to be allocated for transport instance and test
 * usage
 * T11 instance receive  --> (  TRANS_RCV_DESC
 * T9 instance receive   -->  + TRANS_RCV_DESC
 * T11 fragment buffers  -->  + TRANS_FRAG_DESC
 * T9 fragment buffers   -->  + TRANS_FRAG_DESC
 * Number of DSP cores   -->  * NUM_LOCAL_DSP_CORES)
 * Test buffer overheard --> + NUM_TEST_MSGS * 2 (snd/rcv)
 * Extra buffer space    --> + (16 should be enough) */
#define TEST_BUF_NUM          (((TRANS_RCV_DESC + TRANS_RCV_DESC +    \
                                 TRANS_FRAG_DESC + TRANS_FRAG_DESC) * \
                                NUM_LOCAL_DSP_CORES) +                \
                               (NUM_TEST_MSGS * 2) + 16)
#define TEST_BUF_SIZE         4096

#define SRIO_MTU_SIZE         4096
/* Use smaller MTU size so that fragmentation kicks in when sending a buffer
 * of size sizeof(TstMsg) */
#define SRIO_MTU_SIZE_FRAG    (4096-128)

#define SRIO_MSGQ_HEAP_NAME   "srioIpcHeapBuf"
#define SRIO_MSGQ_HEAP_ID     1

#define SRIO_T11_TRANS_NET_ID 1
#define SRIO_T9_TRANS_NET_ID  2

#define TEST_PASS             0
#define TEST_FAIL             1

/************************ GLOBAL VARIABLES ********************/

uint32_t testIterations;
uint32_t testFailed;

/* Core definitions according to IPC */
uint16_t ipcCoreId           = 0;
uint16_t ipcNumLocalDspCores = 0;
uint16_t remoteProcId        = 0;

/* Core number according to the device (DNUM) */
uint32_t coreNum;

Task_Handle initTskHandle    = NULL;
Task_Handle testTskHandle    = NULL;
Task_Handle cleanupTskHandle = NULL;

#define MESSAGEQ_RESERVED_RCV_Q 0

MessageQ_QueueId     remoteQueueId;
MessageQ_Handle      localMessageQ = NULL;

GateMP_Handle        gateMpHandle;
HeapBufMP_Handle     heapHandle;
TransportSrio_Handle srioT11TransHandle = NULL;
TransportSrio_Handle srioT9TransHandle = NULL;

/* These are the device identifiers used in the test Application */
uint32_t DEVICE_ID1_16BIT = 0xBEEF;
uint32_t DEVICE_ID2_16BIT = 0x4560;
uint32_t DEVICE_ID3_16BIT = 0x1234;
uint32_t DEVICE_ID4_16BIT = 0x5678;
uint32_t DEVICE_ID1_8BIT  = 0xAB;
uint32_t DEVICE_ID2_8BIT  = 0xCD;
uint32_t DEVICE_ID3_8BIT  = 0x12;
uint32_t DEVICE_ID4_8BIT  = 0x34;

TransportSrio_srioSockParams         t11socketParams;
TransportSrio_srioSockParams         t9socketParams;
TransportSrio_srioSockType11EpParams t11EpParams[NUM_TOTAL_CORES];
TransportSrio_srioSockType9EpParams  t9EpParams[NUM_TOTAL_CORES];

/* Descriptors aligned and padded to cache line */
#pragma DATA_SECTION (hostDesc, ".desc");
#pragma DATA_ALIGN (hostDesc, 128)
/* Total size must be multiple of cache line */
uint8_t hostDesc[HOST_DESC_NUM * HOST_DESC_SIZE_BYTES];

/* Sync indices into the syncInit array */
#define SYNC_INDEX_SYS_INIT        0
#define SYNC_INDEX_SRIO_TRANS_INIT 1
#define SYNC_INDEX_RM_INIT         2

/* Sync block for initialization barriers */
#pragma DATA_SECTION (syncInit, ".sync");
#pragma DATA_ALIGN (syncInit, 128)
volatile uint8_t syncInit[128];

/* Sync block for Transport initialization barriers */
#pragma DATA_SECTION (syncTransInit, ".sync");
#pragma DATA_ALIGN (syncTransInit, 128)
volatile uint8_t syncTransInit[NUM_LOCAL_DSP_CORES][128];

/* Sync block for cleanup barriers */
#pragma DATA_SECTION (syncCleanup, ".sync");
#pragma DATA_ALIGN (syncCleanup, 128)
volatile uint8_t syncCleanup[NUM_LOCAL_DSP_CORES][128];

/* RM instance handle */
Rm_Handle         rmHandle = NULL;
Rm_ServiceHandle *rmServiceHandle = NULL;

/*************** EXTERN VARIABLES & FUNCTIONS *****************/
/* QMSS device specific configuration */
extern Qmss_GlobalConfigParams qmssGblCfgParams;
/* CPPI device specific configuration */
extern Cppi_GlobalConfigParams cppiGblCfgParams;

/* TransportSrio device specification configuration */
extern TransportSrio_DeviceConfigParams srioTransCfgParams;

/* Global Resource List (GRL) */
extern const char rmGlobalResourceList[];

#if defined(LINUX_NO_BOOT) || (defined(DEVICE_C6657) || defined(DEVICE_C6678))
/* DSP only Policy provided to RM Server */
extern const char rmDspOnlyPolicy[];
#else
/* ARM+DSP Policy provided to RM Server */
extern const char rmDspPlusArmPolicy[];
#endif

/* RM instance transport code */
extern int setupRmTransConfig(uint32_t numTestCores, uint32_t systemInitCore,
                              Task_FuncPtr testTask);

/* SRIO initialization function */
extern int32_t SrioDevice_init(uint8_t pathMode);
extern int32_t SrioDevice_deinit(void);

/*************************** FUNCTIONS ************************/

void initTsk(UArg arg0, UArg arg1);
void proxyInitTsk(UArg arg0, UArg arg1);

static void wb(void *addr, uint32_t sizeBytes)
{
#ifdef L2_CACHE
    /* Writeback L2 */
    CACHE_wbL2(addr, sizeBytes, CACHE_WAIT);
#else
    /* Writeback L1D */
    CACHE_wbL1d(addr, sizeBytes, CACHE_WAIT);
#endif
}

static void inv(void *addr, uint32_t sizeBytes)
{
#ifdef L2_CACHE
    /* Invalidate L2 */
    CACHE_invL2(addr, sizeBytes, CACHE_WAIT);
#else
    CACHE_invL1d(addr, sizeBytes, CACHE_WAIT);
#endif
}

/**
 *  @b Description
 *  @n
 *      This function enables the power/clock domains for SRIO. 
 *
 *  @retval
 *      Success     - 0
 *  @retval
 *      Error       - <0
 */
static int32_t enableSrio(void)
{
#ifndef SIMULATOR_SUPPORT
    /* SRIO power domain is turned OFF by default. It needs to be turned on
     * before doing any SRIO device register access. This not required for the
     * simulator. */

    /* Set SRIO Power domain to ON */
    CSL_PSC_enablePowerDomain(CSL_PSC_PD_SRIO);

    /* Enable the clocks too for SRIO */
    CSL_PSC_setModuleNextState(CSL_PSC_LPSC_SRIO, PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition(CSL_PSC_PD_SRIO);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone(CSL_PSC_PD_SRIO));

    /* Return SRIO PSC status */
    if ((CSL_PSC_getPowerDomainState(CSL_PSC_PD_SRIO) == PSC_PDSTATE_ON) &&
        (CSL_PSC_getModuleState(CSL_PSC_LPSC_SRIO) == PSC_MODSTATE_ENABLE)) {
        /* SRIO ON. Ready for use */
        return(0);
    } else {
        /* SRIO Power on failed. Return error */
        return(-1);
    }
#else
    /* PSC is not supported on simulator. Return success always */
    return(0);
#endif
}

/**
 *  @b Description
 *  @n
 *      Configures the descriptor region and initializes CPPI, QMSS, and SRIO.
 *      This function should only be called once per board.
 *
 *  @retval
 *      Success     - 0
 *  @retval
 *      Error       - <0
 */
int32_t systemInit(void)
{
    Qmss_InitCfg    qmssInitConfig; /* QMSS configuration */
    Qmss_MemRegInfo memInfo; /* Memory region configuration information */
    Qmss_Result     result;
    Cppi_StartCfg   cppiStartCfg;

    System_printf("IPC Core %d : L1D cache size %d. L2 cache size %d.\n",
                  ipcCoreId, CACHE_getL1DSize(), CACHE_getL2Size());

    /* Start the timestamp counter */
    TSCL = 0;

    memset ((void *) &qmssInitConfig, 0, sizeof (Qmss_InitCfg));

    /* Set up the linking RAM. Use the internal Linking RAM. LLD will configure
     * the internal linking RAM address and maximum internal linking RAM size
     * if a value of zero is specified.  Linking RAM1 is not used */
    qmssInitConfig.linkingRAM0Base = 0;
    qmssInitConfig.linkingRAM0Size = 0;
    qmssInitConfig.linkingRAM1Base = 0;
    qmssInitConfig.maxDescNum      = HOST_DESC_NUM;
#if defined(LINUX_NO_BOOT) || (defined(DEVICE_C6657) || defined(DEVICE_C6678))
#ifdef xdc_target__bigEndian
    qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP1;
    qmssInitConfig.pdspFirmware[0].firmware = (void *) &acc48_be;
    qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_be);
#else
    qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP1;
    qmssInitConfig.pdspFirmware[0].firmware = (void *) &acc48_le;
    qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_le);
#endif
#else
    /* Download PDSP3 firmware.  PDSP1 will not be used to avoid potential
     * firmware lockup caused by usage contention with Linux */
#ifdef xdc_target__bigEndian
    qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP3;
    qmssInitConfig.pdspFirmware[0].firmware = (void *) &acc48_be;
    qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_be);
#else
    qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP3;
    qmssInitConfig.pdspFirmware[0].firmware = (void *) &acc48_le;
    qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_le);
#endif
#endif

    qmssGblCfgParams.qmRmServiceHandle = rmServiceHandle;
#if (!defined(LINUX_NO_BOOT)) && (!defined(DEVICE_C6657) && !defined(DEVICE_C6678))
    /* Bypass hardware initialization as it is done within Kernel */
    qmssInitConfig.qmssHwStatus = QMSS_HW_INIT_COMPLETE;
#endif

    /* Initialize Queue Manager SubSystem */
    result = Qmss_init (&qmssInitConfig, &qmssGblCfgParams);
    if (result != QMSS_SOK) {
        System_printf("Error IPC Core %d : "
                      "Initializing Queue Manager SubSystem error code : %d\n",
                      ipcCoreId, result);
        return(-1);
    }

    /* Start the QMSS. */
    if (Qmss_start() != QMSS_SOK) {
        System_printf("Error IPC Core %d : Unable to start the QMSS\n",
                      ipcCoreId);
        return(-1);
    }

    result = Cppi_init(&cppiGblCfgParams);
    if (result != CPPI_SOK) {
        System_printf("Error IPC Core %d : "
                      "Initializing CPPI LLD error code : %d\n", ipcCoreId,
                      result);
    }

    /* Register RM with CPPI */
    cppiStartCfg.rmServiceHandle = rmServiceHandle;
    Cppi_startCfg(&cppiStartCfg);

    /* Setup memory region for host descriptors */
    memset((void *) &hostDesc, 0, HOST_DESC_NUM * HOST_DESC_SIZE_BYTES);
    memset((void *) &memInfo, 0, sizeof(memInfo));
    memInfo.descBase       = (uint32_t *) hostDesc;
    memInfo.descSize       = HOST_DESC_SIZE_BYTES;
    memInfo.descNum        = HOST_DESC_NUM;
    memInfo.manageDescFlag = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
    memInfo.memRegion      = (Qmss_MemRegion) HOST_DESC_MEM_REGION;
    result = Qmss_insertMemoryRegion(&memInfo);
    if (result < QMSS_SOK) {
        System_printf("Error IPC Core %d : "
                      "Inserting memory region, error code : %d\n", ipcCoreId,
                      result);
        return(-1);
    } else {
        System_printf("IPC Core %d : Memory region %d inserted\n", ipcCoreId,
                      HOST_DESC_MEM_REGION);
    }

    /* Writeback the descriptor pool. */
    wb((void *)hostDesc, HOST_DESC_NUM * HOST_DESC_SIZE_BYTES);

    return(0);
}

/**
 *  @b Description
 *  @n
 *      Deletes descriptor region and shuts down CPPI, QMSS, and SRIO.
 *      This function should only be called once across all DSP cores.
 *
 *  @retval
 *      Success     - 0
 *  @retval
 *      Error       - <0
 */
int32_t systemDeInit(void)
{
    Qmss_Result qmssResult;
    Cppi_Result cppiResult;

    /* Remove the memory region. */
    qmssResult = Qmss_removeMemoryRegion(HOST_DESC_MEM_REGION, 0);
    if (qmssResult < QMSS_SOK) {
        System_printf("Error IPC Core %d : Removing memory region: %d\n",
                      ipcCoreId, qmssResult);
        return(-1);
    }

    /* De Initialize CPPI CPDMA */
    cppiResult = Cppi_exit();
    if (cppiResult != CPPI_SOK) {
        System_printf("Error IPC Core %d : "
                      "Deinitializing CPPI error code : %d\n", ipcCoreId,
                      cppiResult);
        return(-1);
    }

    return(0);
}

#ifdef MULTIBOARD_PRODUCER
/**
 *  @b Description
 *  @n
 *      This functions sends msgs to a DSP on a remote board over the SRIO 
 *      transport.  Integrity of data within the messages is tested as well.
 */
static uint32_t testRemoteBoardSend(uint16_t transId)
{
    int32_t   status;
    TstMsg   *txMsg;
    TstMsg   *rxMsg = NULL;
    uint32_t  i, j, noSync = 1;
    uint32_t  result = TEST_PASS;

    System_printf("IPC Core %d : Syncing with consumer board DSP %d...\n",
                  ipcCoreId, remoteProcId);
    while(noSync) {
        rxMsg = NULL;
        txMsg = (TstMsg *) MessageQ_alloc(SRIO_MSGQ_HEAP_ID, sizeof(TstMsg));
        if (txMsg == NULL) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_alloc failed for sync msg\n", ipcCoreId);
            result = TEST_FAIL;
            goto error_exit;
        }

        MessageQ_setTransportId(txMsg, transId);
        txMsg->flags = CORE_SYNC_TX_SIDE_READY;

        System_printf("IPC Core %d : Sending sync message...\n", ipcCoreId);
        status = MessageQ_put(remoteQueueId, (MessageQ_Msg) txMsg);
        if (status < 0) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_put failed for sync msg with error %d\n",
                          ipcCoreId, status);
            result = TEST_FAIL;
            goto error_exit;
        }

        System_printf("IPC Core %d : Checking for sync response...\n",
                      ipcCoreId);
        status = MessageQ_get(localMessageQ, (MessageQ_Msg*)&rxMsg, 5000000);
        if ((status < 0) && (status != MessageQ_E_TIMEOUT)) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_get failed for sync msg with error %d\n",
                          ipcCoreId, status);
            result = TEST_FAIL;
            goto error_exit;
        }

        if (rxMsg) {
            if (rxMsg->flags == CORE_SYNC_RX_SIDE_READY) {
                System_printf("IPC Core %d : Received sync response\n",
                              ipcCoreId);
                noSync = 0;
            }

            status = MessageQ_free((MessageQ_Msg) rxMsg);
            if (status < 0) {
                System_printf("Error IPC Core %d : "
                              "Message free failed with error %d\n", ipcCoreId,
                              status);
                result = TEST_FAIL;
                goto error_exit;
            }
        }
    }

    System_printf("IPC Core %d : Sending packets to consumer board DSP %d...\n",
                  ipcCoreId, remoteProcId);
    
    for (i = 0; i < NUM_TEST_MSGS; i++) {
        txMsg = (TstMsg *) MessageQ_alloc(SRIO_MSGQ_HEAP_ID, sizeof(TstMsg));
        if (txMsg == NULL) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_alloc failed for message %d\n",
                          ipcCoreId, i);
            result = TEST_FAIL;
            goto error_exit;
        }

        MessageQ_setTransportId(txMsg, transId);

        /* Populate msg's data with pattern for integrity check */
        for (j = 0; j < TEST_MSG_DATA_LEN_WORDS; j++) {
            txMsg->data[j] = ((i << 16) | j);
        }

        txMsg->flags = 0;
        if (i == (NUM_TEST_MSGS - 1)) {
            /* Signal last message in stream */
            txMsg->flags = SEND_MESSAGE_LAST_MESSAGE;
        }

        status = MessageQ_put(remoteQueueId, (MessageQ_Msg) txMsg);
        if (status < 0) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_put failed with error %d for message %d\n",
                          ipcCoreId, status, i);
            result = TEST_FAIL;
            goto error_exit;
        }
    }

    System_printf("IPC Core %d : Sent a total of %d messages.\n", ipcCoreId,
                  NUM_TEST_MSGS);

    System_printf("IPC Core %d : "
                  "Waiting for consumer board to signal all msgs received...\n",
                  ipcCoreId);
    status = MessageQ_get(localMessageQ, (MessageQ_Msg*)&rxMsg,
                          MessageQ_FOREVER);
    if (status < 0) {
        System_printf("Error IPC Core %d : MessageQ_get failed with error %d\n",
                      ipcCoreId, status);
        result = TEST_FAIL;
        goto error_exit;
    }

    if (rxMsg->flags == CORE_SYNC_RX_SIDE_READY) {
        System_printf("IPC Core %d : Received \"all msgs received\" signal\n",
                      ipcCoreId);
    } else {
        System_printf("IPC Core %d : "
                      "Did not receive \"all msgs received\" signal\n",
                      ipcCoreId);
        result = TEST_FAIL;
    }
    
    status = MessageQ_free((MessageQ_Msg) rxMsg);
    if (status < 0) {
        System_printf("Error IPC Core %d : "
                      "Message free failed with error %d\n", ipcCoreId, status);
        result = TEST_FAIL;
    }

error_exit:
    return(result);
}
#elif defined(MULTIBOARD_CONSUMER)
/**
 *  @b Description
 *  @n
 *      This functions receives msgs from a DSP on a remote board over the SRIO transport.
 *      Integrity of data within the messages is tested as well.
 */
static uint32_t testRemoteBoardReceive(uint16_t transId)
{
    int32_t   status;
    TstMsg   *rxMsg;
    TstMsg   *txMsg;
    uint32_t  numReceived = 0;
    uint32_t  i;
    int32_t   rxFlag;
    uint32_t  result = TEST_PASS;

    System_printf("IPC Core %d : "
                  "Waiting for producer board DSP %d to signal ready...\n",
                  ipcCoreId, remoteProcId);
    status = MessageQ_get(localMessageQ, (MessageQ_Msg*)&rxMsg,
                          MessageQ_FOREVER);
    if (status < 0) {
        System_printf("Error IPC Core %d : "
                      "MessageQ_get failed with error %d\n", ipcCoreId, status);
        result = TEST_FAIL;
        goto error_exit;
    }

    if (rxMsg->flags == CORE_SYNC_TX_SIDE_READY) {
        System_printf("IPC Core %d : Received \"producer ready\" signal\n",
                      ipcCoreId);

        txMsg = rxMsg;
        MessageQ_setTransportId(txMsg, transId);
        txMsg->flags = CORE_SYNC_RX_SIDE_READY;
        System_printf("IPC Core %d : Sending \"consumer ready\" response\n",
                      ipcCoreId);
        status = MessageQ_put(remoteQueueId, (MessageQ_Msg) txMsg);
        if (status < 0) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_put failed with error %d\n", ipcCoreId,
                          status);
            result = TEST_FAIL;
            goto error_exit;
        }
    } else {
        System_printf("IPC Core %d : "
                      "Did not receive \"producer ready\" signal\n", ipcCoreId);
        result = TEST_FAIL;
        status = MessageQ_free((MessageQ_Msg) rxMsg);
        if (status < 0) {
            System_printf("Error IPC Core %d : "
                          "Message free failed with error %d\n", ipcCoreId,
                          status);
        }
        goto error_exit;
    }

    System_printf("IPC Core %d : "
                  "Receiving packets from a producer board DSP %d...\n",
                  ipcCoreId, remoteProcId);

    while (1) {
        status = MessageQ_get(localMessageQ, (MessageQ_Msg*)&rxMsg,
                              MessageQ_FOREVER);
        if (status < 0) {
            System_printf("Error IPC Core %d : "
                          "MessageQ_get failed with error %d\n", ipcCoreId,
                          status);
            result = TEST_FAIL;
            goto error_exit;
        }

        if (rxMsg->flags == CORE_SYNC_TX_SIDE_READY) {
            System_printf("IPC Core %d : "
                          "Dropping extra ready signal msg from producer.\n",
                          ipcCoreId);
        } else {
            for (i = 0; i < TEST_MSG_DATA_LEN_WORDS; i++) {
                uint32_t integrityMask = ((numReceived << 16) | i);
                
                if (rxMsg->data[i] != integrityMask) {
                    System_printf("Error IPC Core %d : "
                                  "Data mismatch in receive message %d\n",
                                  ipcCoreId, numReceived);
                    result = TEST_FAIL;
                    MessageQ_free((MessageQ_Msg) rxMsg);
                    goto error_exit;
                }
            }
            numReceived++;
        }

        rxFlag = rxMsg->flags;
        status = MessageQ_free((MessageQ_Msg) rxMsg);
        if (status < 0) {
            System_printf("Error IPC Core %d : "
                          "Message free failed with error %d\n", ipcCoreId,
                          status);
            result = TEST_FAIL;
            goto error_exit;
        }

        if (rxFlag == SEND_MESSAGE_LAST_MESSAGE) {
            break;
        }
    }

    if (numReceived != NUM_TEST_MSGS) {
        System_printf("Error IPC Core %d : "
                      "Expected %d msgs but only received %d\n", ipcCoreId,
                      NUM_TEST_MSGS, numReceived);
        result = TEST_FAIL;
        goto error_exit;
    } else {
        System_printf("IPC Core %d : Received a total of %d messages.\n",
                      ipcCoreId, numReceived);
    }

    System_printf("IPC Core %d : "
                  "Signal to producer that all msgs were received...\n",
                  ipcCoreId);
    txMsg = (TstMsg *) MessageQ_alloc(SRIO_MSGQ_HEAP_ID, sizeof(TstMsg));
    if (txMsg == NULL) {
        System_printf("Error IPC Core %d : MessageQ_alloc failed\n",
                      ipcCoreId);
        result = TEST_FAIL;
        goto error_exit;
    }

    MessageQ_setTransportId(txMsg, transId);
    txMsg->flags = CORE_SYNC_RX_SIDE_READY;

    System_printf("IPC Core %d : Sending \"all msgs received signal\"...\n",
                  ipcCoreId);
    status = MessageQ_put(remoteQueueId, (MessageQ_Msg) txMsg);
    if (status < 0) {
        System_printf("Error IPC Core %d : MessageQ_put failed with error %d\n",
                      ipcCoreId, status);
        result = TEST_FAIL;
    }

error_exit:
    return(result);
}
#endif

/**
 *  @b Description
 *  @n
 *      Cleanup task used to reset application to pre-initTsk state
 */
void cleanupTsk(UArg arg0, UArg arg1)
{
    Task_Params taskParams;
    Int         status;
    Qmss_Result qmssResult;
    int32_t     rmResult;
    int32_t     i;

    System_printf("IPC Core %d : "
                  "================== CLEANUP ==================\n", ipcCoreId);

    if (initTskHandle) {
        Task_delete(&initTskHandle);
        initTskHandle = NULL;
    }

    if (testTskHandle) {
        Task_delete(&testTskHandle);
        testTskHandle = NULL;
    }

    System_printf("IPC Core %d : Closing remote MessageQ\n", ipcCoreId);
    status = MessageQ_close(&remoteQueueId);
    if (status != MessageQ_S_SUCCESS) {
        testFailed = BENCH_TRUE;
        System_printf("Error IPC Core %d : MessageQ_close\n", ipcCoreId);
        goto cleanup_error;
    }

    System_printf("IPC Core %d : Deleting local MessageQ\n", ipcCoreId);
    status = MessageQ_delete(&localMessageQ);
    if (status != MessageQ_S_SUCCESS) {
        testFailed = BENCH_TRUE;
        System_printf("Error IPC Core %d : MessageQ_delete\n", ipcCoreId);
        goto cleanup_error;
    }

    System_printf("IPC Core %d : Deleting SRIO Type 11 IPC transport\n",
                  ipcCoreId);
    TransportSrio_delete(&srioT11TransHandle);
    srioT11TransHandle = NULL;

    System_printf("IPC Core %d : Deleting SRIO Type 9 IPC transport\n",
                  ipcCoreId);
    TransportSrio_delete(&srioT9TransHandle);
    srioT9TransHandle = NULL;

    System_printf("IPC Core %d : Unregistering MessageQ heap\n", ipcCoreId);
    status = MessageQ_unregisterHeap(SRIO_MSGQ_HEAP_ID);
    if (status != MessageQ_S_SUCCESS) {
        testFailed = BENCH_TRUE;
        System_printf("Error IPC Core %d : MessageQ_unregisterHeap\n",
                      ipcCoreId);
        goto cleanup_error;
    }

    if (coreNum == SYSINIT) {
        /* Wait for other cores to complete cleanup */
        for (i = 0; i < NUM_LOCAL_DSP_CORES; i++) {
            if (i != coreNum) {
                do {
                    inv((void *) &syncCleanup[i][0], sizeof(syncCleanup) /
                        NUM_LOCAL_DSP_CORES);
                } while (!syncCleanup[i][0]);
            }
        }

        System_printf("IPC Core %d : Closing HeapBufMP\n", ipcCoreId);
        status = HeapBufMP_close(&heapHandle);
        if (status != HeapBufMP_S_SUCCESS) {
            testFailed = BENCH_TRUE;
            System_printf("Error IPC Core %d : HeapBufMP_close\n", ipcCoreId);
            goto cleanup_error;
        }

        System_printf("IPC Core %d : Deleting GateMP used by HeapBufMP\n",
                      ipcCoreId);
        status = GateMP_delete(&gateMpHandle);
        if (status != GateMP_S_SUCCESS) {
            testFailed = BENCH_TRUE;
            System_printf("Error IPC Core %d : GateMP_close\n", ipcCoreId);
            goto cleanup_error;
        }

        System_printf("IPC Core %d : Closing SRIO\n", ipcCoreId);
        if (Srio_close() < 0) {
            testFailed = BENCH_TRUE;
            System_printf("Error IPC Core %d : Srio_close() Failed\n",
                          ipcCoreId);
            goto cleanup_error;
        }

        System_printf("IPC Core %d : De-initializing QMSS and CPPI\n",
                      ipcCoreId);
        status = systemDeInit();
        if (status < 0) {
            testFailed = BENCH_TRUE;
            goto cleanup_error;
        }

        if (SrioDevice_deinit() < 0) {
            testFailed = BENCH_TRUE;
            System_printf("Error IPC Core %d : SrioDevice_deinit() Failed\n",
                          ipcCoreId);
            goto cleanup_error;
        }

        /* Exit qmss */
        System_printf("IPC Core %d : exit QMSS\n", ipcCoreId);
        if ((qmssResult = Qmss_exit ())) {
            testFailed = BENCH_TRUE;
            System_printf("Error IPC Core %d : exit error code : %d\n",
                          ipcCoreId, qmssResult);
            goto cleanup_error;
        }
    } else {
        System_printf("IPC Core %d : Closing HeapBufMP\n", ipcCoreId);
        status = HeapBufMP_close(&heapHandle);
        if (status != HeapBufMP_S_SUCCESS) {
            testFailed = BENCH_TRUE;
            System_printf("Error IPC Core %d : HeapBufMP_close\n", ipcCoreId);
            goto cleanup_error;
        }
    }

cleanup_error:

    if (coreNum == SYSINIT) {
        if ((rmResult = Rm_resourceStatus(rmHandle, FALSE)) != 0) {
            System_printf("Error IPC Core %d : "
                          "Number of unfreed resources : %d\n", ipcCoreId,
                          rmResult);
            /* print them */
            Rm_resourceStatus(rmHandle, TRUE);
        } else {
            System_printf("IPC Core %d : All resources freed successfully\n",
                          ipcCoreId);
        }
    }

    /* SYSINIT cannot cleanup until all other cores have completed cleanup */
    if (coreNum != SYSINIT) {
        syncCleanup[coreNum][0] = 1;
        wb((void *) &syncCleanup[coreNum][0], sizeof(syncCleanup) /
           NUM_LOCAL_DSP_CORES);
    }

    System_printf("IPC Core %d : "
                  "*********************************************\n", ipcCoreId);
    System_printf("IPC Core %d : Completed Test Iteration: %d - ", ipcCoreId,
                  testIterations);
    if (testFailed) {
        System_printf("FAILED\n");
        System_printf("IPC Core %d : Cleaning up and exiting...\n", ipcCoreId);
    } else {
        System_printf("PASSED\n");
    }
    System_printf("IPC Core %d : "
                  "*********************************************\n", ipcCoreId);

    testIterations++;
    if ((!testFailed) && (testIterations < TEST_ITERATIONS_TOT)) {
        /* Secondary DSPs must wait for SYSINIT DSP to complete cleanup if
         * test is starting another iteration */
        if (coreNum == SYSINIT) {
            syncCleanup[coreNum][0] = 1;
            wb((void *) &syncCleanup[coreNum][0], sizeof(syncCleanup) /
               NUM_LOCAL_DSP_CORES);
            /* Sleep so secondary cores can get barrier update for sync
             * mechanism reset occurs */
            Task_sleep(1);
        } else {
            do {
                inv((void *) &syncCleanup[SYSINIT][0], sizeof(syncCleanup) /
                    NUM_LOCAL_DSP_CORES);
            } while (!syncCleanup[SYSINIT][0]); 
        }

        /* Reset all sync mechanisms */
        memset(&syncInit, 0, sizeof(syncInit));
        memset((void *)&syncTransInit[coreNum][0], 0, sizeof(syncTransInit) /
               NUM_LOCAL_DSP_CORES);
        memset((void *)&syncCleanup[coreNum][0], 0, sizeof(syncCleanup) /
               NUM_LOCAL_DSP_CORES);
        if (coreNum == SYSINIT) {
            wb((void *)&syncInit, sizeof(syncInit));
        }
        wb((void *)&syncTransInit[coreNum][0], sizeof(syncTransInit) /
           NUM_LOCAL_DSP_CORES);
        wb((void *)&syncCleanup[coreNum][0], sizeof(syncCleanup) /
           NUM_LOCAL_DSP_CORES);

        /* Restart test from initTsk */
        System_printf("IPC Core %d : Recreating init task...\n", ipcCoreId);
        Task_Params_init(&taskParams);
        taskParams.priority = 1;
        taskParams.stackSize = 4096;
        initTskHandle = Task_create(initTsk, &taskParams, NULL);
    } else {
        System_printf("IPC Core %d : Testing complete!\n", ipcCoreId);
    }
}

/**
 *  @b Description
 *  @n
 *      Task that runs the packet send/receive to/from remote board test
 */
void testTsk(UArg arg0, UArg arg1)
{
    Task_Params taskParams;

    System_printf("IPC Core %d : "
                  "*********************************************\n", ipcCoreId);
    System_printf("IPC Core %d : "
                  "Starting Test Iteration: %d\n", ipcCoreId, testIterations);
    System_printf("IPC Core %d : "
                  "Test Packet Size: %d bytes\n", ipcCoreId, sizeof(TstMsg));
    System_printf("IPC Core %d : "
                  "*********************************************\n", ipcCoreId);

#ifdef MULTIBOARD_PRODUCER
    System_printf("IPC Core %d : SRIO transport Type 11 producer send...\n",
                  ipcCoreId);
    if (testRemoteBoardSend(SRIO_T11_TRANS_NET_ID) == TEST_FAIL) {
        testFailed = BENCH_TRUE;
        System_printf("IPC Core %d : FAILED\n", ipcCoreId);
    }

    /* Cleanup any buffers that may be in transmit completion queue */
    TransportSrio_recycleUsedTxBufs(srioT11TransHandle);
#elif defined(MULTIBOARD_CONSUMER)
    System_printf("IPC Core %d : "
                  "*********************************************\n", ipcCoreId);
    System_printf("IPC Core %d : "
                  "SRIO transport Type 11 consumer receive...\n", ipcCoreId);
    if (testRemoteBoardReceive(SRIO_T11_TRANS_NET_ID) == TEST_FAIL) {
        testFailed = BENCH_TRUE;
        System_printf("IPC Core %d : FAILED\n", ipcCoreId);
    }
    System_printf("IPC Core %d : PASSED\n", ipcCoreId);
    System_printf("IPC Core %d : "
                  "*********************************************\n\n",
                  ipcCoreId);
#endif

#ifdef MULTIBOARD_PRODUCER
    System_printf("IPC Core %d : SRIO transport Type 9 producer send...\n",
                  ipcCoreId);
    if (testRemoteBoardSend(SRIO_T9_TRANS_NET_ID) == TEST_FAIL) {
        testFailed = BENCH_TRUE;
        System_printf("IPC Core %d : FAILED\n", ipcCoreId);
    }

    /* Cleanup any buffers that may be in transmit completion queue */
    TransportSrio_recycleUsedTxBufs(srioT9TransHandle);
#elif defined(MULTIBOARD_CONSUMER)
    System_printf("IPC Core %d : "
                  "*********************************************\n", ipcCoreId);
    System_printf("IPC Core %d : "
                  "SRIO transport Type 9 consumer receive...\n", ipcCoreId);
    if (testRemoteBoardReceive(SRIO_T9_TRANS_NET_ID) == TEST_FAIL) {
        testFailed = BENCH_TRUE;
        System_printf("IPC Core %d : FAILED\n", ipcCoreId);
    }
    System_printf("IPC Core %d : PASSED\n", ipcCoreId);
    System_printf("IPC Core %d : "
                  "*********************************************\n\n",
                  ipcCoreId);
#endif

    /* Create the cleanup task */
    System_printf("Core %d : Creating cleanup task...\n", coreNum);
    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    cleanupTskHandle = Task_create(cleanupTsk, &taskParams, NULL);
}

/**
 *  @b Description
 *  @n
 *      Application initialization task.
 */
void initTsk(UArg arg0, UArg arg1)
{
    Task_Params          taskParams;
    Qmss_StartCfg        qmssStartCfg;
    Cppi_StartCfg        cppiStartCfg;
    Srio_InitConfig      srioInitCfg;
    int32_t              status;
    Error_Block          errorBlock;
    TransportSrio_Params transSrioParamsT11;
    TransportSrio_Params transSrioParamsT9;
    GateMP_Params        gateMpParams;
    HeapBufMP_Params     heapBufParams;
    uint8_t              pathMode;
    uint32_t             srioMaxMTU;
    MessageQ_Params      msgQParams;

    switch (testIterations) {
        case 0:
            /* Port 0 is 1x - from srioPortPathMode_e defined in
             * device_srio.c */
            pathMode = 0;
            srioMaxMTU = SRIO_MTU_SIZE;
            System_printf("Core %d : Configuring SRIO port 0 as 1x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
        case 1:
            /* Port 0 is 2x - from srioPortPathMode_e defined in
             * device_srio.c */
            pathMode = 3;
            srioMaxMTU = SRIO_MTU_SIZE;
            System_printf("Core %d : Configuring SRIO port 0 as 2x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
        case 2:
            /* Port 0 is 4x - from srioPortPathMode_e defined in
             * device_srio.c */
            pathMode = 4;
            srioMaxMTU = SRIO_MTU_SIZE;
            System_printf("Core %d : Configuring SRIO port 0 as 4x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
        case 3:
            /* Port 0 is 1x - from srioPortPathMode_e defined in
             * device_srio.c */
            pathMode = 0;
            srioMaxMTU = SRIO_MTU_SIZE_FRAG;
            System_printf("Core %d : Configuring SRIO port 0 as 1x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
        case 4:
            /* Port 0 is 2x - from srioPortPathMode_e defined in
             * device_srio.c */
            pathMode = 3;
            srioMaxMTU = SRIO_MTU_SIZE_FRAG;
            System_printf("Core %d : Configuring SRIO port 0 as 2x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
        case 5:
            /* Port 0 is 4x - from srioPortPathMode_e defined in
             * device_srio.c */
            pathMode = 4;
            srioMaxMTU = SRIO_MTU_SIZE_FRAG;
            System_printf("Core %d : Configuring SRIO port 0 as 4x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
        default:
            /* Port 0 is 1x for any further test iterations */
            pathMode = 0;
            srioMaxMTU = SRIO_MTU_SIZE;
            System_printf("Core %d : Configuring SRIO port 0 as 1x "
                          "and MTU size as %d\n",
                          coreNum, srioMaxMTU);
            break;
    }

    /* System initializations for each core. */
    if (coreNum == SYSINIT) {
        /* Run SRIO, QMSS, and CPPI system wide initializations */
        System_printf("IPC Core %d : Initialize system peripherals\n",
                      ipcCoreId);
        status = systemInit();
        if (status != 0) {
            System_printf("Error IPC Core %d : "
                          "initializing system peripherals\n", ipcCoreId);
            return;
        }

        /* Power on SRIO peripheral */
        if (enableSrio() < 0) {
            System_printf("Error IPC Core %d : "
                          "SRIO PSC Initialization Failed\n", ipcCoreId);
            return;
        }

        /* Device Specific SRIO Initializations: This should always be called before
         * initializing the SRIO Driver. */
        if (SrioDevice_init(pathMode) < 0) {
            System_printf("Error IPC Core %d : SRIO HW Initialization Failed\n",
                          ipcCoreId);
            return;
        }

        /* Initialize SRIO driver */
        memset(&srioInitCfg, 0, sizeof(srioInitCfg));
        srioInitCfg.rmServiceHandle = rmServiceHandle;
        if (Srio_initCfg(&srioInitCfg) < 0) {      
            System_printf("Error IPC Core %d : "
                          "SRIO Driver Initialization Failed\n", ipcCoreId);
            return;
        }

        System_printf("IPC Core %d : SRIO Driver has been initialized\n",
                      ipcCoreId);

        /* Signal to other DSPs that system init is complete */
        syncInit[SYNC_INDEX_SYS_INIT] = 1;
        wb((void *) &syncInit, sizeof(syncInit));
    } else {
        System_printf("IPC Core %d : Waiting for SRIO to be initialized.\n",
                      ipcCoreId);

        /* Wait for SYSINIT core to complete system init */
        do {
            inv((void *) &syncInit, sizeof(syncInit));
        } while (!syncInit[SYNC_INDEX_SYS_INIT]);

        /* Start Queue Manager SubSystem with RM */
        qmssStartCfg.rmServiceHandle = rmServiceHandle;
        qmssStartCfg.pQmssGblCfgParams = &qmssGblCfgParams;
        status = Qmss_startCfg(&qmssStartCfg);
        if (status != QMSS_SOK) {
            System_printf("Error IPC Core %d : Unable to start the QMSS\n",
                          ipcCoreId);
            return;
        }

        /* Register RM with CPPI */
        cppiStartCfg.rmServiceHandle = rmServiceHandle;
        Cppi_startCfg(&cppiStartCfg);

        System_printf("IPC Core %d : SRIO can now be used.\n", ipcCoreId);
    }

    if (coreNum == SYSINIT) {
        /* Create the heap that will be used to allocate messages. */
        GateMP_Params_init(&gateMpParams);
        gateMpParams.localProtect = GateMP_LocalProtect_INTERRUPT;
        gateMpHandle = GateMP_create(&gateMpParams);
        
        HeapBufMP_Params_init(&heapBufParams);
        heapBufParams.regionId  = 0;
        heapBufParams.name      = SRIO_MSGQ_HEAP_NAME;
        heapBufParams.numBlocks = TEST_BUF_NUM;
        heapBufParams.blockSize = TEST_BUF_SIZE;
        /* GateMP so allocation can take place within SRIO rx interrupt
         * context */
        heapBufParams.gate      = gateMpHandle;
        heapHandle = HeapBufMP_create(&heapBufParams);
        if (heapHandle == NULL) {
            System_printf("Error IPC Core %d : HeapBufMP_create failed\n",
                          ipcCoreId);
            return;
        }
    } else {
        /* Open the heap created by the other processor. Loop until opened. */
        do {
            status = HeapBufMP_open(SRIO_MSGQ_HEAP_NAME, &heapHandle);
        } while (status < 0);
    }

    /* Each core must register this heap with MessageQ */
    MessageQ_registerHeap((IHeap_Handle)heapHandle, SRIO_MSGQ_HEAP_ID);

#ifdef MULTIBOARD_PRODUCER  
    remoteProcId = ipcCoreId + IPC_CLUSTER_OFFSET;
#elif defined(MULTIBOARD_CONSUMER)
    remoteProcId = ipcCoreId - IPC_CLUSTER_OFFSET;
#endif

    System_printf("IPC Core %d : Creating reserved MessageQ %d\n", ipcCoreId,
                  MESSAGEQ_RESERVED_RCV_Q);
    MessageQ_Params_init(&msgQParams);
    msgQParams.queueIndex = MESSAGEQ_RESERVED_RCV_Q;
    /* Create reserved message queue. */
    localMessageQ = MessageQ_create(NULL, &msgQParams);
    if (localMessageQ == NULL) {
        System_printf("Error IPC Core %d : MessageQ_create failed\n",
                      ipcCoreId);
        return;
    }

    System_printf("IPC Core %d : Opening reserved MessageQ %d on IPC core %d\n",
                  ipcCoreId, MESSAGEQ_RESERVED_RCV_Q, remoteProcId);
    remoteQueueId = MessageQ_openQueueId(MESSAGEQ_RESERVED_RCV_Q, remoteProcId);

    /* Create SRIO type 11 & 9 transport instances.  They will be a network
     * transport so won't interfere with default MessageQ transport, shared
     * memory notify transport */

    /* Type 11 configuration */
    TransportSrio_Params_init(&transSrioParamsT11);
    /* Configure common parameters */
    transSrioParamsT11.deviceCfgParams   = &srioTransCfgParams;
    transSrioParamsT11.txMemRegion       = HOST_DESC_MEM_REGION;
    transSrioParamsT11.txNumDesc         = TRANS_SEND_DESC;
    transSrioParamsT11.txNumFragDesc     = TRANS_FRAG_DESC;
    transSrioParamsT11.txMsgQFragHeapId  = SRIO_MSGQ_HEAP_ID;
    transSrioParamsT11.txDescSize        = HOST_DESC_SIZE_BYTES;
    transSrioParamsT11.rxQType           = Qmss_QueueType_HIGH_PRIORITY_QUEUE;
    transSrioParamsT11.rxMemRegion       = HOST_DESC_MEM_REGION;
    transSrioParamsT11.rxNumDesc         = TRANS_RCV_DESC;
    transSrioParamsT11.rxDescSize        = HOST_DESC_SIZE_BYTES;
    transSrioParamsT11.rxMsgQHeapId      = SRIO_MSGQ_HEAP_ID;
    transSrioParamsT11.maxMTU            = srioMaxMTU;
    transSrioParamsT11.rmServiceHandle   = rmServiceHandle;
    /* Must map to a valid channel for each DSP core.  Follow sprugr9h.pdf
     * Table 5-7 for C6678 and Table 5-9 for K2HK */
    transSrioParamsT11.accumCh           = DNUM;
    transSrioParamsT11.accumTimerCount   = 0; 
    transSrioParamsT11.transNetworkId    = SRIO_T11_TRANS_NET_ID;
    transSrioParamsT11.rxIntVectorId     = 8;

    memset(&t11EpParams, 0, sizeof(t11EpParams));
#if (defined(DEVICE_C6657) || defined(DEVICE_C6678))
    /* Core 0 (Producer) MultiProc ID - 0 */
    t11EpParams[0].tt       = 0;
    t11EpParams[0].deviceId = DEVICE_ID1_8BIT;
    t11EpParams[0].mailbox  = 0;
    t11EpParams[0].letter   = 0;
    t11EpParams[0].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 1 (Producer) MultiProc ID - 1 */
    t11EpParams[1].tt       = 0;
    t11EpParams[1].deviceId = DEVICE_ID1_8BIT;
    t11EpParams[1].mailbox  = 0;
    t11EpParams[1].letter   = 1;
    t11EpParams[1].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 0 (Consumer) MultiProc ID - 2 */
    t11EpParams[2].tt       = 0;
    t11EpParams[2].deviceId = DEVICE_ID2_8BIT;
    t11EpParams[2].mailbox  = 0;
    t11EpParams[2].letter   = 0;
    t11EpParams[2].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 1 (Consumer) MultiProc ID - 3 */
    t11EpParams[3].tt       = 0;
    t11EpParams[3].deviceId = DEVICE_ID2_8BIT;
    t11EpParams[3].mailbox  = 0;
    t11EpParams[3].letter   = 1;
    t11EpParams[3].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
#else
    /* Linux Host (Producer) MultiProc ID - 0 */
    t11EpParams[0].tt       = 0;
    t11EpParams[0].deviceId = DEVICE_ID1_8BIT;
    t11EpParams[0].mailbox  = 0;
    t11EpParams[0].letter   = 0;
    t11EpParams[0].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 0 (Producer) MultiProc ID - 1 */
    t11EpParams[1].tt       = 0;
    t11EpParams[1].deviceId = DEVICE_ID1_8BIT;
    t11EpParams[1].mailbox  = 0;
    t11EpParams[1].letter   = 1;
    t11EpParams[1].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 1 (Producer) MultiProc ID - 2 */
    t11EpParams[2].tt       = 0;
    t11EpParams[2].deviceId = DEVICE_ID1_8BIT;
    t11EpParams[2].mailbox  = 0;
    t11EpParams[2].letter   = 2;
    t11EpParams[2].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Linux Host (Consumer) MultiProc ID - 3 */
    t11EpParams[3].tt       = 0;
    t11EpParams[3].deviceId = DEVICE_ID2_8BIT;
    t11EpParams[3].mailbox  = 0;
    t11EpParams[3].letter   = 0;
    t11EpParams[3].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 0 (Consumer) MultiProc ID - 4 */
    t11EpParams[4].tt       = 0;
    t11EpParams[4].deviceId = DEVICE_ID2_8BIT;
    t11EpParams[4].mailbox  = 0;
    t11EpParams[4].letter   = 1;
    t11EpParams[4].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
    /* Core 1 (Consumer) MultiProc ID - 5 */
    t11EpParams[5].tt       = 0;
    t11EpParams[5].deviceId = DEVICE_ID2_8BIT;
    t11EpParams[5].mailbox  = 0;
    t11EpParams[5].letter   = 2;
    t11EpParams[5].segMap   = (sizeof(TstMsg) > 256 ? 1 :0);
#endif

    memset(&t11socketParams, 0, sizeof(t11socketParams));
    t11socketParams.epListSize = NUM_TOTAL_CORES;
    t11socketParams.sockType = TransportSrio_srioSockType_TYPE_11;
    t11socketParams.u.pT11Eps = &t11EpParams[0];

    transSrioParamsT11.sockParams = &t11socketParams;

    Error_init(&errorBlock);
    System_printf("IPC Core %d : "
                  "Creating SRIO Transport instance with Type 11 socket\n",
                  ipcCoreId);
    srioT11TransHandle = TransportSrio_create(&transSrioParamsT11, &errorBlock);
    if (srioT11TransHandle == NULL) {
        System_printf("Error IPC Core %d : "
                      "TransportSrio_create failed with id %d\n", ipcCoreId,
                      errorBlock.id);
        return;
    }

    /* Type 9 configuration */
    TransportSrio_Params_init(&transSrioParamsT9);
    /* Configure common parameters */
    transSrioParamsT9.deviceCfgParams   = &srioTransCfgParams;
    transSrioParamsT9.txMemRegion       = HOST_DESC_MEM_REGION;
    transSrioParamsT9.txNumDesc         = TRANS_SEND_DESC;
    transSrioParamsT9.txNumFragDesc     = TRANS_FRAG_DESC;
    transSrioParamsT9.txMsgQFragHeapId  = SRIO_MSGQ_HEAP_ID;
    transSrioParamsT9.txDescSize        = HOST_DESC_SIZE_BYTES;
    transSrioParamsT9.rxQType           = Qmss_QueueType_HIGH_PRIORITY_QUEUE;
    transSrioParamsT9.rxMemRegion       = HOST_DESC_MEM_REGION;
    transSrioParamsT9.rxNumDesc         = TRANS_RCV_DESC;
    transSrioParamsT9.rxDescSize        = HOST_DESC_SIZE_BYTES;
    transSrioParamsT9.rxMsgQHeapId      = SRIO_MSGQ_HEAP_ID;
    transSrioParamsT9.maxMTU            = srioMaxMTU;
    transSrioParamsT9.rmServiceHandle   = rmServiceHandle;
    /* Type 9 instance specific parameters */
#if defined(DEVICE_C6657)
    /* Must map to a valid channel for each DSP core.  Follow sprugr9h.pdf
     * Table 5-3 & 5-4 for C6657 */
    transSrioParamsT9.accumCh            = DNUM + 2; /* Next valid accum ch is
                                                      * number of device DSP
                                                      * cores away
                                                      * (2 for C6657) */
#else
    /* Must map to a valid channel for each DSP core.
     * Follow sprugr9h.pdf Table 5-7 for C6678 and Table 5-9 for K2HK */
    transSrioParamsT9.accumCh            = DNUM + 8; /* Next valid accum ch is
                                                      * number of device DSP
                                                      * cores away 
                                                      * (8 for C6678 & K2HK) */
#endif
    transSrioParamsT9.accumTimerCount    = 0; 
    transSrioParamsT9.transNetworkId     = SRIO_T9_TRANS_NET_ID;
    transSrioParamsT9.rxIntVectorId      = 9;

    /* Type 9 specific */
    memset(&t9EpParams, 0, sizeof(t9EpParams));
#if (defined(DEVICE_C6657) || defined(DEVICE_C6678))
    /* Core 0 (Producer) MultiProc ID - 0 */
    t9EpParams[0].tt       = 0;
    t9EpParams[0].deviceId = DEVICE_ID1_8BIT;
    t9EpParams[0].cos      = 0;
    t9EpParams[0].streamId = 0;
    /* Core 1 (Producer) MultiProc ID - 1 */
    t9EpParams[1].tt       = 0;
    t9EpParams[1].deviceId = DEVICE_ID1_8BIT;
    t9EpParams[1].cos      = 0;
    t9EpParams[1].streamId = 1;
    /* Core 0 (Consumer) MultiProc ID - 2 */
    t9EpParams[2].tt       = 0;
    t9EpParams[2].deviceId = DEVICE_ID2_8BIT;
    t9EpParams[2].cos      = 0;
    t9EpParams[2].streamId = 0;
    /* Core 1 (Consumer) MultiProc ID - 3 */
    t9EpParams[3].tt       = 0;
    t9EpParams[3].deviceId = DEVICE_ID2_8BIT;
    t9EpParams[3].cos      = 0;
    t9EpParams[3].streamId = 1;
#else
    /* Linux Host (Producer) MultiProc ID - 0 */
    t9EpParams[0].tt       = 0;
    t9EpParams[0].deviceId = DEVICE_ID1_8BIT;
    t9EpParams[0].cos      = 0;
    t9EpParams[0].streamId = 0;
    /* Core 0 (Producer) MultiProc ID - 1 */
    t9EpParams[1].tt       = 0;
    t9EpParams[1].deviceId = DEVICE_ID1_8BIT;
    t9EpParams[1].cos      = 0;
    t9EpParams[1].streamId = 1;
    /* Core 1 (Producer) MultiProc ID - 2 */
    t9EpParams[2].tt       = 0;
    t9EpParams[2].deviceId = DEVICE_ID1_8BIT;
    t9EpParams[2].cos      = 0;
    t9EpParams[2].streamId = 2;
    /* Linux Host (Consumer) MultiProc ID - 3 */
    t9EpParams[3].tt       = 0;
    t9EpParams[3].deviceId = DEVICE_ID2_8BIT;
    t9EpParams[3].cos      = 0;
    t9EpParams[3].streamId = 0;
    /* Core 0 (Consumer) MultiProc ID - 4 */
    t9EpParams[4].tt       = 0;
    t9EpParams[4].deviceId = DEVICE_ID2_8BIT;
    t9EpParams[4].cos      = 0;
    t9EpParams[4].streamId = 1;
    /* Core 1 (Consumer) MultiProc ID - 5 */
    t9EpParams[5].tt       = 0;
    t9EpParams[5].deviceId = DEVICE_ID2_8BIT;
    t9EpParams[5].cos      = 0;
    t9EpParams[5].streamId = 2;
#endif

    memset(&t9socketParams, 0, sizeof(t9socketParams));
    t9socketParams.epListSize = NUM_TOTAL_CORES;
    t9socketParams.sockType = TransportSrio_srioSockType_TYPE_9;
    t9socketParams.u.pT9Eps = &t9EpParams[0];

    transSrioParamsT9.sockParams = &t9socketParams;

    Error_init(&errorBlock);
    System_printf("IPC Core %d : "
                  "Creating SRIO Transport instance with Type 9 socket\n",
                  ipcCoreId);
    srioT9TransHandle = TransportSrio_create(&transSrioParamsT9, &errorBlock);
    if (srioT9TransHandle == NULL) {
        System_printf("Error IPC Core %d : "
                      "TransportSrio_create failed with id %d\n", ipcCoreId,
                      errorBlock.id);
        return;
    }

#if RM_PRINT_STATS
    if (coreNum == SYSINIT) {
        Rm_resourceStatus(rmHandle, 1);
    }
    Rm_instanceStatus(rmHandle);
#endif

    if (coreNum == SYSINIT) {
        int32_t i;

        /* Wait for other cores to complete initialization */
        for (i = 0; i < NUM_LOCAL_DSP_CORES; i++) {
            if (i != coreNum) {
                do {
                    inv((void *) &syncTransInit[i][0], sizeof(syncTransInit) /
                        NUM_LOCAL_DSP_CORES);
                } while (!syncTransInit[i][0]);
            }
        }
    } else {
        /* SYSINIT cannot start testing until all other cores have completed
         * initialization */
        syncTransInit[coreNum][0] = 1;
        wb((void *) &syncTransInit[coreNum][0], sizeof(syncTransInit) /
           NUM_LOCAL_DSP_CORES);
    }

    if (cleanupTskHandle) {
        Task_delete(&cleanupTskHandle);
        cleanupTskHandle = NULL;
    }

    /* Create the test task */
    System_printf("IPC Core %d : Creating test task...\n", ipcCoreId);
    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    testTskHandle = Task_create(testTsk, &taskParams, NULL);
}

/**
 *  @b Description
 *  @n
 *      Proxy task for the initTsk.  The initTsk needs a larger stack.  The
 *      setupRmTransConfig function does not allow specification of post-setup
 *      task stack size.  This function takes care of that for the initTsk.
 */
void proxyInitTsk(UArg arg0, UArg arg1)
{
    Task_Params taskParams;

    /* Create the initialization task */
    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    taskParams.stackSize = 4096;
    initTskHandle = Task_create(initTsk, &taskParams, NULL);
}

/**
 *  @b Description
 *  @n
 *      Main - Initialize the system and start BIOS
 */
void main(int32_t argc, char *argv[])
{
    int32_t    status;
    Rm_InitCfg rmInitCfg;
    char       rmInstName[RM_NAME_MAX_CHARS];
    int32_t    rmResult;

    coreNum = CSL_chipReadReg(CSL_CHIP_DNUM);
    ipcCoreId = MultiProc_self();
    ipcNumLocalDspCores = NUM_LOCAL_DSP_CORES;
    testIterations = 0;
    testFailed = BENCH_FALSE;
    /* Init the sync mechanisms */
    memset(&syncInit, 0, sizeof(syncInit));
    memset(&syncCleanup, 0, sizeof(syncCleanup));

    System_printf("IPC Core %d : "
                  "*********************************************************\n",
                  ipcCoreId);
#ifdef MULTIBOARD_PRODUCER
    System_printf("IPC Core %d : "
                  "*** IPC SRIO Transport Multi-Board Example (Producer) ***\n",
                  ipcCoreId);
#elif defined(MULTIBOARD_CONSUMER)
    System_printf("IPC Core %d : "
                  "*** IPC SRIO Transport Multi-Board Example (Consumer) ***\n",
                  ipcCoreId);
#else
    System_printf("Error IPC Core %d : "
                  "IPC SRIO Transport Multi-Board Example not compiled as "
                  "producer or consumer\n", ipcCoreId);
    System_printf("IPC Core %d : "
                  "*********************************************************\n",
                  ipcCoreId);
    System_printf("IPC Core %d : Exiting...\n", ipcCoreId);
    return;
#endif
    System_printf("IPC Core %d : "
                  "*********************************************************\n",
                  ipcCoreId);

    System_printf("IPC Core %d : IPC Core ID:               %d\n", ipcCoreId,
                  ipcCoreId);
    System_printf("IPC Core %d : Number local processors:   %d\n", ipcCoreId,
                  ipcNumLocalDspCores);
    System_printf("IPC Core %d : Number of test iterations: %d\n", ipcCoreId,
                  TEST_ITERATIONS_TOT);
    System_printf("IPC Core %d : Starting IPC core with name (\"%s\")\n",
                  ipcCoreId, MultiProc_getName(ipcCoreId));

    status = Ipc_start();
    if (status < 0) {
        System_printf("Error IPC Core %d : Ipc_start failed\n", ipcCoreId);
        return;
    }

    /* Initialize RM */
    if (coreNum == SYSINIT) { 
        /* Create the Server instance */
        memset((void *)&rmInitCfg, 0, sizeof(Rm_InitCfg));
        System_sprintf(rmInstName, "RM_Server");
        rmInitCfg.instName = rmInstName;
        rmInitCfg.instType = Rm_instType_SERVER;
        rmInitCfg.instCfg.serverCfg.globalResourceList = (void *)rmGlobalResourceList;
#if defined(LINUX_NO_BOOT) || (defined(DEVICE_C6657) || defined(DEVICE_C6678))
        rmInitCfg.instCfg.serverCfg.globalPolicy = (void *)rmDspOnlyPolicy;
#else
        rmInitCfg.instCfg.serverCfg.globalPolicy = (void *)rmDspPlusArmPolicy;
#endif
        rmHandle = Rm_init(&rmInitCfg, &rmResult);
        if (rmResult != RM_OK) {
            System_printf("Error IPC Core %d : "
                          "Initializing Resource Manager error code : %d\n",
                          ipcCoreId, rmResult);
            return;
        }

        syncInit[SYNC_INDEX_RM_INIT] = 1;
        wb((void *) &syncInit, sizeof(syncInit));
    } else {
        /* Create a RM Client instance */
        memset((void *)&rmInitCfg, 0, sizeof(Rm_InitCfg));
        System_sprintf(rmInstName, "RM_Client%d", coreNum);
        rmInitCfg.instName = rmInstName;
        rmInitCfg.instType = Rm_instType_CLIENT;
        rmHandle = Rm_init(&rmInitCfg, &rmResult);
        if (rmResult != RM_OK) {
            System_printf("Error IPC Core %d : "
                          "Initializing Resource Manager error code : %d\n",
                          ipcCoreId, rmResult);
            return;
        }

        /* Wait for RM Server to start */
        do {
            inv((void *) &syncInit, sizeof(syncInit));
        } while (!syncInit[SYNC_INDEX_RM_INIT]);
    }

    rmServiceHandle = Rm_serviceOpenHandle(rmHandle, &rmResult);
    if (rmResult != RM_OK) {
        System_printf("Error IPC Core %d : "
                      "Creating RM service handle error code : %d\n", ipcCoreId,
                      rmResult);
        return;
    }

    /* Configure the RM transport.  Will jump to specified task upon completion */
    System_printf("IPC Core %d : Configuring RM transport via IPC...\n",
                  ipcCoreId);
    if (setupRmTransConfig(ipcNumLocalDspCores, SYSINIT, proxyInitTsk) < 0) {
        System_printf("Error IPC Core %d : Transport setup for RM error\n",
                      ipcCoreId);
        return;
    }

    System_printf("IPC Core %d : Starting BIOS...\n", ipcCoreId);
    BIOS_start();
}

/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the IPC SRIO Transport library
 *
 * Copyright (C) 2011-2018, Texas Instruments, Inc.
 *****************************************************************************/

package ti.transport.ipc.c66.srio [2,3,0,1] {
    module Settings;
    module TransportSrio;
}

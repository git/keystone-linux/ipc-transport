/**
 *   @file  c6678/src/TransportSrio_device.c
 *
 *   @brief   
 *      This file contains the device specific configuration data
 *      for SRIO IPC Transport.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/* Transport SRIO includes */
#include <ti/transport/ipc/c66/srio/TransportSrio.h>

#define MAX_CORES 8
#define MAX_ACCUM_CHANNELS 32

/* Contains the system event to accumulator channel mappings for
 * each DSP core */
static UInt8 chan_to_event_map[MAX_CORES * MAX_ACCUM_CHANNELS] = {
    /* Core 0 mappings */
    48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0, 0, 0, 0, 0, 0,
    50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0, 0, 0, 0, 0, 0,
    /* Core 1 mappings */
     0,48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0, 0, 0, 0, 0,
     0,50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0, 0, 0, 0, 0,
    /* Core 2 mappings */
     0, 0,48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0, 0, 0, 0,
     0, 0,50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0, 0, 0, 0,
    /* Core 3 mappings */
     0, 0, 0,48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0, 0, 0,
     0, 0, 0,50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0, 0, 0,
    /* Core 4 mappings */
     0, 0, 0, 0,48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0, 0,
     0, 0, 0, 0,50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0, 0,
    /* Core 5 mappings */
     0, 0, 0, 0, 0,48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0,
     0, 0, 0, 0, 0,50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0,
    /* Core 6 mappings */
     0, 0, 0, 0, 0, 0,48, 0, 0, 0, 0, 0, 0, 0,49, 0,
     0, 0, 0, 0, 0, 0,50, 0, 0, 0, 0, 0, 0, 0,51, 0,
    /* Core 7 mappings */
     0, 0, 0, 0, 0, 0, 0,48, 0, 0, 0, 0, 0, 0, 0,49,
     0, 0, 0, 0, 0, 0, 0,50, 0, 0, 0, 0, 0, 0, 0,51
};

/* TransportSrio initialization parameters */
TransportSrio_DeviceConfigParams srioTransCfgParams =
{
    /* Number of DSP cores on device */
    MAX_CORES,
    /* Number of accumulator channels on device */
    MAX_ACCUM_CHANNELS,
    /** Accumulator channel to GEM event mapping table */
    &chan_to_event_map[0]
};


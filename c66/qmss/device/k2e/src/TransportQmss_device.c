/**
 *   @file  k2e/src/TransportQmss_device.c
 *
 *   @brief   
 *      This file contains the device specific configuration data
 *      for QMSS IPC Transport.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/* QMSS includes */
#include <ti/drv/qmss/qmss_qm.h>

/* CSL includes */
#include <ti/csl/csl_qm_queue.h>

/* Transport QMSS includes */
#include <ti/transport/ipc/c66/qmss/TransportQmss.h>

#define MAX_DSP_CORES      1
#define MAX_PDSPS          8
#define MAX_ACCUM_CHANNELS 32

/* Contains the system event to accumulator channel mappings for INTD1 for
 * each DSP core */
static UInt8 intd1ChanToEventMap[MAX_DSP_CORES * MAX_ACCUM_CHANNELS] = {
    /* Core 0 mappings */
    48, 0, 0, 0, 0, 0, 0, 0,49, 0, 0, 0, 0, 0, 0, 0,
    50, 0, 0, 0, 0, 0, 0, 0,51, 0, 0, 0, 0, 0, 0, 0,
};

/* Contains the system event to accumulator channel mappings for INTD2 for
 * each DSP core */
static UInt8 intd2ChanToEventMap[MAX_DSP_CORES * MAX_ACCUM_CHANNELS] = {
    /* Core 0 mappings */
    52, 0, 0, 0, 0, 0, 0, 0,53, 0, 0, 0, 0, 0, 0, 0,
    54, 0, 0, 0, 0, 0, 0, 0,55, 0, 0, 0, 0, 0, 0, 0,
};

/* PDSP configuration */
static TransportQmss_pdspCfg pdspCfg[MAX_PDSPS] = {
    {0, &intd1ChanToEventMap[0]}, /* PDSP 1 */
    {0, &intd1ChanToEventMap[0]}, /* PDSP 2 */
    {1, &intd2ChanToEventMap[0]}, /* PDSP 3 */
    {1, &intd2ChanToEventMap[0]}, /* PDSP 4 */
    {0, &intd1ChanToEventMap[0]}, /* PDSP 5 */
    {0, &intd1ChanToEventMap[0]}, /* PDSP 6 */
    {1, &intd2ChanToEventMap[0]}, /* PDSP 7 */
    {1, &intd2ChanToEventMap[0]}  /* PDSP 8 */
};

/* CIC0 secondary interrupt to QMSS Qpend queue mappings */
static TransportQmss_intcQInfo cic0QInfo[] = {
    { 652,  47,  1},
    { 653,  91,  1},
    { 654,  93,  1},
    { 655,  95,  1},
    { 656,  97,  1},
    { 657, 151,  9},
    { 666, 292, 26},
    /* Last entry must be {-1, 0, 0} to signify end of list */
    {  -1,   0,  0}
};

/* DSP to INTC configurations */
static TransportQmss_dspIntcCfg dspIntcCfg[MAX_DSP_CORES] = {
    {0, &cic0QInfo[0]}  /* DSP 0 */
};

/* This table maps a DSP core system (or GEM) event to a CIC Host interrupt
 * number.  The mappings are pulled directly from Table 7-22 System Event
 * Mapping in 66ak2e02.pdf */
static TransportQmss_hostIntMod hostIntMods[] = {
    { 23, 35,  0},
    { 26, 68,  0},
    { 27, 69,  0},
    { 28, 70,  0},
    { 29, 71,  0},
    { 30, 72,  0},
    { 31, 73,  0},
    { 32, 16,  0},
    { 33, 17,  0},
    { 34, 18,  0},
    { 35, 19,  0},
    { 36, 20,  0},
    { 37, 21,  0},
    { 38, 22,  0},
    { 39, 23,  0},
    { 40, 32,  0},
    { 41, 33,  0},
    { 42, 13,  0},
    { 43, 14,  0},
    { 44, 15,  0},
    { 45, 64,  0},
    { 46, 65,  0},
    { 47, 66,  0},
    { 56,  0,  0},
    { 57,  1,  0},
    { 58,  2,  0},
    { 59,  3,  0},
    { 60,  4,  0},
    { 61,  5,  0},
    { 62,  6,  0},
    { 63,  7,  0},
    { 74,  8,  0},
    { 75,  9,  0},
    { 76, 10,  0},
    { 77, 11,  0},
    { 95, 67,  0},
    {107, 12,  0},
    {108, 34,  0},
    /* Last entry must be {-1, 0, 0} to signify end of list */
    { -1,  0,  0}
};

/* TransportQmss initialization parameters */
TransportQmss_DeviceConfigParams qmssTransCfgParams =
{
    /* Maximum number of QMSS PDSPs */
    MAX_PDSPS,
    /* QMSS infrastructure queue block base */
    QMSS_INFRASTRUCTURE_QUEUE_BASE,
    /* Max QMSS infrastructure queues */
    QMSS_MAX_INFRASTRUCTURE_QUEUE,
    /* Number of accumulator channels on device */
    MAX_ACCUM_CHANNELS,
    /* Pointer to PDSP configuration table */
    &pdspCfg[0],
    /* Pointer to INTC configuration table for each DSP */
    &dspIntcCfg[0],
    /* Additive and multiplicative modifiers used to calculate System (GEM)
     * event to CIC Host interrupt mappings.  These modifiers are common across
     * all DSP cores */
    &hostIntMods[0]
};


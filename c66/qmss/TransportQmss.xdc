/* File: TransportQmss.xdc
 *
 * Purpose: TransportQmss RTSC module specification.  The module error codes
 *          and initialization parameters are specified here
 *
 * Copyright (c) 2011-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 *  ======== TransportQmss.xdc ========
 */
import xdc.runtime.Assert;
import xdc.rov.ViewInfo;

import ti.sdo.utils.MultiProc;
import ti.sdo.ipc.MessageQ;

/*!
 *  ======== TransportQmss ========
 *  Transport for MessageQ that acts with QMSS.
 *
 */
@InstanceFinalize
@InstanceInitError

module TransportQmss inherits ti.sdo.ipc.interfaces.INetworkTransport
{
    /*!
     *  ======== A_txMsgQFreeError ========
     *  The MessageQ message attached to a transmit descriptor could not be 
     *  freed back to MessageQ.
     */    
    config Assert.Id A_txMsgQFreeError  = {
        msg: "A_txMsgQFreeError: Could not free transmit MessageQ buffer."
    };

    /*!
     *  ======== A_newRxBufNull ========
     *  The newly allocated receive MessageQ buffer is NULL
     */
    config Assert.Id A_newRxBufNull  = {
        msg: "A_newRxBufNull: New allocated receive buffer is NULL."
    };

    /*! Transport operation completed successfully */
    const Int OK                                       = 0;
    /*! QMSS returned an error when attempting to allocate the transmit
     *  completion queue */
    const Int ERROR_COULD_NOT_OPEN_TX_COMP_Q           = 64;
    /*! CPPI returned an error when attempting to initalize the tx descriptors
     *  and allocate the transmit free queue */
    const Int ERROR_TX_DESCRIPTOR_INIT_FAILED          = 65;
    /*! QMSS returned an error when attempting to allocate the receive free
     *  queue */
    const Int ERROR_COULD_NOT_OPEN_RX_FREE_Q           = 66;
    /*! QMSS returned an error when attempting to allocate the receive queue
     *  used by the accumulator */
    const Int ERROR_COULD_NOT_OPEN_RX_ACCUM_Q          = 67;
    /*! CPPI returned an error when attempting to initalize the rx
     *  descriptors */
    const Int ERROR_RX_DESCRIPTOR_INIT_FAILED          = 68;
    /*! MessageQ returned a NULL buffer when pre-allocating buffers for the
     *  SRIO receive descriptors */
    const Int ERROR_ALLOC_FROM_RX_MESSAGEQ_HEAP_FAILED = 69;
    /*! A NULL buffer was returned when trying to allocate the accumulator list
     *  ping/pong buffer */
    const Int ERROR_ALLOC_OF_ACCUM_LIST_FAILED         = 70;
    /*! The given accumulator channel does not map to a DSP GEM event on this
     *  core.  Please reference the Multicore Navigator spec for the proper DSP
     *  core to channel mappings on the device */
    const Int ERROR_INVALID_ACCUMULATOR_CH             = 71;
    /*! An attempt to open a second TransportQmss instance.  Only one
     *  TransportQmss instance can be created per DSP core */
    const Int ERROR_INST_ALREADY_EXISTS                = 72;
    /*! The transport configuration parameter, queueRcvConfig, contains an
     *  invalid receive queue configuration type.  Valid receive queue
     *  configurations are specified within the queueRcvType enum */
    const Int ERROR_INVALID_QUEUE_RCV_TYPE             = 73;
    /*! A deviceCfgParams structure pointer provided as part of the
     *  TransportQmss_create initialization parameters is invalid */
    const Int ERROR_INVALID_DEVICE_CFG_PARAMS          = 74;
    /*! Invalid network transport ID.  The transport ID can have a value
     *  between 1 and 7 */
    const Int ERROR_INVALID_TRANSPORT_ID               = 75;
    /*! The SRIO transport instance could not be registered with MessageQ */
    const Int ERROR_COULD_NOT_REGISTER_TRANSPORT       = 76;
    /*! QMSS returned an error when attempting to open a QPEND queue during
     *  instance initialization */
    const Int ERROR_COULD_NOT_OPEN_RX_QPEND_Q          = 77;
    /*! A valid, or unused, QPEND queue could not be opened for TransportQmss
     *  instance initialization */
    const Int ERROR_COULD_NOT_FIND_VALID_RX_QPEND_Q    = 78;
    /*! Instance initialization parameters contained a PDSP number greater than
     *  the number of PDSPs on the device */
    const Int ERROR_INVALID_QMSS_PDSP                  = 79;
    /*! An error occurred when trying to program the QMSS accumulator during
     *  TransportQmss instance initialization */
    const Int ERROR_COULD_NOT_PROGRAM_ACCUM            = 80;
    /*! CPPI returned a NULL handle when trying to open the QMSS CPDMA */
    const Int ERROR_COULD_NOT_OPEN_INFRA_CPDMA         = 81;
    /*! CPPI returned a NULL rx flow handle when trying to open the a rx
     *  flow */
    const Int ERROR_COULD_NOT_OPEN_CPPI_RX_FLOW        = 82;
    /*! CPPI returned an error when trying to enabled the CPPI tx channel */
    const Int ERROR_COULD_NOT_ENABLE_CPPI_TX_CH        = 83;
    /*! CPPI returned an error when trying to enabled the CPPI rx channel */
    const Int ERROR_COULD_NOT_ENABLE_CPPI_RX_CH        = 84;
    /*! A system event to CIC host interrupt modification entry could not be
     *  found in the device parameter hostIntModMap list. */
    const Int ERROR_INVALID_SYSTEM_EVENT               = 85;
    /*! The RM Service Handle was NULL.  An RM Service Handle must be provided
     *  for TransportQmss instance initialization */
    const Int ERROR_RM_SERVICE_HANDLE_IS_NULL          = 86;
    /*! The TransportQmss's flow cache list could not be allocated */
    const Int ERROR_ALLOC_OF_FLOW_CACHE                = 87;
    /*! Could not find a valid QMSS infrastructure triplet.  The infrastructure
     *  triplet is the combination of QMSS infrastructure queue, CPPI QMSS
     *  CPDMA transmit channel and CPPI QMSS CPDMA receive channel */
    const Int ERROR_COULD_NOT_FIND_INFRA_TRIPLET       = 88;

    /*!
     * QMSS PDSP configuration
     *
     *  @p(blist)
     * -intd         -> The INTD to which the PDSP is connected
     * -intdChEvtMap -> Pointer to an array that contains the accumulator
     *                  channel to system (GEM) event mappings for the INTD
     *                  connected to the PDSP
     *  @p
    */
    struct pdspCfg {
        UInt32  intd;
        UInt8  *intdChEvtMap;
    };

    /*!
     *  INTC QMSS queue range specification structure.
     *
     *  @p(blist)
     * -baseQ      -> Base of the INTC queue range
     * -baseSecInt -> Base of the CIC Secondary interrupt (System interrupt in
     *                BIOS CpIntc module) range that directly maps to the queue
     *                range starting at baseQ
     * -len        -> Number of contiguous QMSS INTC queues and CIC Secondary
     *                interrrupts within the range
     *  @p
    */
    struct intcQInfo {
        Int32 baseQ;
        Int32 baseSecInt;
        Int32 len;
    };

    /*!
     * DSP to INTC configuration
     *
     *  @p(blist)
     * -cic       -> The CIC to which the DSP is connected
     * -intcQInfo -> Pointer to an array that contains INTC queue information
     *               for the CIC to which the DSP is connected
     *  @p
    */
    struct dspIntcCfg {
        UInt32     cic;
        intcQInfo *intcInfo;
    };

    /*!
     * System (or GEM) event modifiers used to calculate the CIC Host interrupt
     * tied to the System event.  Formula for conversion of system (GEM) event
     * to CIC host interrupt is hostInt = offset * (multiplier * N) where N is
     * the DSP core number.
     *
     *  @p(blist)
     * -sysEvt     -> System event
     * -offset     -> Calculation offset used to generated CIC Host interrupt
     *                number that is tied to the system event
     * -multiplier -> Calculation multiplier used to generated CIC Host
     *                interrupt number that is tied to the system event
     *  @p
    */
    struct hostIntMod {
        Int32 sysEvt;     /* DSP core system (or GEM) event */
        Int32 offset;     /* Additive offset */
        Int32 multiplier; /* Multiplier */
    };

    /*!
     *  Device specific configuration parameters for the QMSS transport.
     *
     *  @p(blist)
     * -maxPdsps       -> Maximum number of QMSS PDSPs on the device
     * -qmssInfraQBase -> QMSS Infrastructure queue block base value
     * -qmssInfraQMax  -> Max number of QMSS Infrastructure queues
     * -numAccumCh     -> Number of accumulator channels supported
     * -pdspCfg        -> Pointer to table containing the configurations for
     *                    each PDSP on the device
     * -dspIntcCfg     -> Pointer to table that contains the INTC information
     *                    for each DSP on the device
     * -hostIntModMap  -> Additive and multiplicative modifiers used to
     *                    calculate System (GEM) event to CIC Host interrupt
     *                    mappings.  These modifiers are common across all DSP
     *                    cores
     *  @p
    */
    struct DeviceConfigParams {
        UInt16      maxPdsps;
        Int32       qmssInfraQBase;
        Int32       qmssInfraQMax;
        UInt16      numAccumCh;
        pdspCfg    *pdspCfg;
        dspIntcCfg *dspIntcCfg;
        hostIntMod *hostIntModMap;
    };

    /*!
     *  Destination flow cache sorted list node.  Each node maps a remote
     *  MessageQ queue ID to a CPPI rx flow ID.  Sending to a rx flow ID via
     *  QMSS will allow the destination to route the sent MessageQ message to
     *  the associated MessageQ queue ID
     *
     *  @p(blist)
     * -queueId -> MessageQ queue ID
     * -flow    -> CPPI rx flow ID
     *  @p
    */
    struct FlowCacheNode {
        UInt32 queue_id;
        UInt16 flow;
    };

    /*!
     *  Data structure that tracks the usage of the flow cache sorted list
     *
     *  @p(blist)
     * -total_nodes -> Total number of flow cache nodes pointed to by node_list
     * -used_nodes  -> Number of flow cache nodes in use out of total pointed to
     *                 by node_list
     * -node_list   -> Pointer to an array of flow cache nodes sorted by
     *                 queue_id.  The array is of size total_nodes
     *  @p
    */
    struct FlowCacheList {
        UInt32         total_nodes;
        UInt32         used_nodes;
        FlowCacheNode *node_list;
    };

    /*!
     *  QMSS transport instance queue reception type. These values are passed
     *  as a configuration parameter when creating a new QMSS transport.  They
     *  define the type of queue used to receive descriptors */
    enum QueueRcvType {
        queueRcvType_INVALID,
        queueRcvType_ACCUMULATOR,
        queueRcvType_QPEND
    };

    /*!
     *  Accumulator queue reception specific configuration parameters
     *
     *  @p(blist)
     * -rxAccQType  -> The QMSS queue type that will be opened and used with the
     *                 receive accumulator.  The queue type does not matter.
     *                 Accumulator GEM events are mapped to the accumulator
     *                 channels, not the queue type+value.
     * -qmPdsp      -> QMSS PDSP from which an accumulator channel should be
     *                 allocated.  The PDSP firmware must be downloaded prior to
     *                 creating the TransportQmss instance since the PDSP number
     *                 only used to program an accumulator channel.
     * -accCh       -> The accumulator channel used for packet reception.  The
     *                 GEM event for the accumulator interrupt will be derived
     *                 from the provided accumulator channel and the DSP core
     *                 number.
     * -accTimerCnt -> Number of global timer ticks to delay the periodic
     *                 accumulator interrupt.  A value of zero will cause an
     *                 interrupt immediately upon a descriptor being placed in
     *                 the accumulator ping/pong buffer.
     *  @p
    */
    struct AccumRcvParams {
        UInt32 rxAccQType;
        UInt32 qmPdsp;
        UInt8  accCh;
        UInt32 accTimerCnt;
    };

    /*!
     *  QPEND queue reception specific configuration parameters
     *
     *  @p(blist)
     * -systemEvent -> The system (GEM) event that is to be tied to a CIC host
     *                 interrupt when hooking up the QPEND interrupt.  QPEND
     *                 queues generate secondary interrupts that are routed
     *                 into the CICs.  The secondary event is mapped to a host
     *                 interrupt which is selected based on the system event
     *                 chosen.
     *  @p
    */
    struct QpendRcvParams {
        UInt systemEvent;
    };

    /*!
     *  Parameters that define the type of receive queue that will be used by
     *  the transport.
     *
     *  @p(blist)
     * -qType -> QMSS queue reception configuration.  QMSS can be configured
     *           to receive descriptors over different queue types which
     *           offer accumulation, direct interrupt features.
     * -accum -> Accumulator-specific configuration parameters if the
     *           receive queue is selected to be an accumulator queue
     * -qpend -> QPEND-specific configuration parameters if the receive
                 queue is selected to be an QPEND queue
     *  @p
    */
    struct QueueRcvParams {
        QueueRcvType   qType;
        AccumRcvParams accum;
        QpendRcvParams qpend;
    };

instance:

    /*!
     *  ======== deviceCfgParams ========
     *  Pointer to the device specific SRIO transport configuration parameters
     */
    config DeviceConfigParams *deviceCfgParams = null;

    /*!
     *  ======== txMemRegion ========
     *  QMSS memory region from which the transmit host descriptors will be
     *  allocated from.
     */
    config Int                 txMemRegion = -1;

    /*!
     *  ======== txNumDesc ========
     *  Number of host descriptors to pre-allocate for transmit operations
     */
    config UInt32              txNumDesc = 2;

    /*!
     *  ======== txDescSize ========
     *  Size of the transmit descriptors.  This size is used for cache coherence
     *  operations and should be a multiple a cache line.
     */
    config UInt32              txDescSize = 0;

    /*!
     *  ======== rxMemRegion ========
     *  Qmss memory region from which the receive host descriptors will be
     *  allocated from.
     */
    config Int                 rxMemRegion = -1;

    /*!
     *  ======== rxNumDesc ========
     *  Number of host descriptors to pre-allocate for receive operations
     */
    config UInt32              rxNumDesc = 1;

    /*!
     *  ======== rxDescSize ========
     *  Size of the receive descriptors.  This size is used for cache coherence
     *  operations and should be a multiple a cache line.
     */
    config UInt32              rxDescSize = 0;

    /*!
     *  ======== rxMsgQHeapId ========
     *  Rx-side MessageQ Heap ID.  MessageQ buffers are pre-allocated out of
     *  this heap and attached to descriptors used to receive packets through
     *  the QMSS hardware.
     */
    config UInt16              rxMsgQHeapId = ~1u;

    /*!
     *  ======== maxMTU ========
     *  Maximum transmittable unit in bytes that will be handled by the QMSS
     *  transport.  Used to handle cache coherence operations and pre-allocation
     *  of receive side MessageQ buffers.
     */
    config UInt32              maxMTU = 256;

    /*!
     *  ======== rcvQParams ========
     *  Parameters that define the type of receive QMSS queue that will be used
     *  by the transport.
     */
    config QueueRcvParams      rcvQParams = {
            qType: queueRcvType_INVALID,
            accum: {rxAccQType: 0,
                    qmPdsp: 0,
                    accCh: 0,
                    accTimerCnt: 0},
            qpend: {systemEvent: 0}
        };

    /*!
     *  ======== rmServiceHandle ========
     *  RM service handle.  The RM NameServer is used to store and propagate
     *  MessageQ queueID to rxFlow ID mappings
     */
    config Void               *rmServiceHandle = null;

    /*!
     *  ======== rxIntVectorId ========
     *  Interrupt vector ID to tie to the receive side accumulator operation
     */
    config UInt                rxIntVectorId = ~1u;

    /*!
     *  ======== transNetworkId ========
     *  The transport instance will be registered with MessageQ as the supplied
     *  network transport ID.  MessageQ messages with a matching transport ID in
     *  their MessageQ header will be sent over the transport instance.
     *
     *  The MessageQ transport network ID can have a value between 1 and 7.
     */
    config UInt                transNetworkId = 0;

    /*!
     *  ======== recycleUsedTxBufs ========
     *  Calling this function will recycle all descriptors and buffers in the
     *  transmit completion Q.  The _put() process attempts to recycle one
     *  descriptor and buffer at a time.  However, this functionality may
     *  fall behind the number of descriptors in the completion queue if many
     *  packets are sent in a short period of time.  Use this function to
     *  clean backed up descriptors in the transmit completion queue.
     *
     *  Returns the number of buffers recycled
     */
    Int recycleUsedTxBufs();

    /*!
     *  ======== flushDstCache ========
     *  Flush the destination address cache stored within the
     *  transport instance.  An entry is inserted into the cache
     *  for each new destination the transport must send to.  The
     *  cache will need to be flushed if a destination is transport
     *  instance is closed.  Currently, there's no backpath for
     *  informing of instance closures.  As a result, the
     *  destination cache must be flushed manually.
     *
     *  The cache can be flushed of a single entry by providing a
     *  destination MessageQ queue ID that was previously sent to but
     *  has since been closed.
     *
     *  The entire destination cache will be flushed if
     *  MessageQ_INVALIDMESSAGEQ is provided for the MessageQ queue ID
     */
    Bool flushDstCache(UInt32 queueId);

internal:

    /*!
     *  Handles packet reception over a QMSS accumulator queue and routing to
     *  destination MessageQ queues
     */
    Void Accum_isr(UArg arg);

    /*!
     *  Handles packet reception over a QMSS QPEND queue and routing to
     *  destination MessageQ queues
     */
    Void Qpend_isr(UArg arg);

    /* Per core state information */
    struct Module_State {
        UInt instCreated;
    }

    /* Accumulator specific receive configuration state variables */
    struct Accum_State {
        Int32   rxAccQ;
        UInt8   accCh;
        UInt32  accListSize;
        UInt32 *accList;
        Int     usePing;
        Int32   pdsp;
    }

    /* QPEND specific receive configuration state variables */
    struct Qpend_State {
        Int32 rxQpendQ;
        UInt  cicSecInt;
        UInt  sysEvt;
    }
    
    /* Instance State object - Per transport instance (multiple per core if
     *                         multiple connections) */
    struct Instance_State {
        DeviceConfigParams *devCfg;
        Int32               txCompletionQ;
        Int32               txQ;
        Int32               txFreeQ;
        Int32               rxFreeQ;
        UInt32              maxMTU;
        UInt16              rxMsgQHeapId;
        UInt32              txDescSize;
        UInt32              rxDescSize;
        UInt32             *cppiHnd;
        UInt32             *txCppiHnd;
        UInt32             *rxCppiHnd;
        UInt32             *rxFlowHnd;
        Void               *hwiHandle;
        QueueRcvType        queueRcvConfig;
        union {
            Accum_State     accum;
            Qpend_State     qpend;
        } u;
        UInt32              rxIntVectorId;
        Void               *rmServiceHandle;
        FlowCacheList       flowCache;
        UInt16              transNetId;
    }
}


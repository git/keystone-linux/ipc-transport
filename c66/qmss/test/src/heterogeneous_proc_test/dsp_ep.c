/*
 *   dsp_client.c
 *
 *   DSP portion of Resource Manager ARM+DSP test that uses RPMSG and sockets to
 *   allow a DSP application to to request RM services from a RM Server running
 *   from Linux User-space.
 *
 *  ============================================================================
 *
 * Copyright (c) 2012-2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <string.h>

#include <xdc/std.h>
#include <xdc/cfg/global.h>

/* XDC include */
#include <xdc/runtime/System.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Timestamp.h>
#include <xdc/runtime/Error.h>

/* BIOS6 include */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* IPC include */
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/ipcmgr/IpcMgr.h>
#include <ti/ipc/GateMP.h>
#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/SharedRegion.h>

/* CSL include */
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_pscAux.h>
#include <ti/csl/csl_chip.h>

/* QMSS LLD */
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/qmss/qmss_firmware.h>

/* CPPI LLD */
#include <ti/drv/cppi/cppi_drv.h>

/* RM LLD */
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>

/* Transport QMSS includes */
#include <ti/transport/ipc/c66/qmss/TransportQmss.h>

/************************ USER DEFINES ********************/

#define SYSINIT                 0
#ifdef DEVICE_K2E
#define NUM_DSP_CORES           1
#else
#define NUM_DSP_CORES           2
#endif
#define NUM_TEST_ITERATIONS     100

#define HOST_DESC_SIZE_BYTES    128
#define HOST_DESC_NUM           256
#define HOST_DESC_MEM_REGION    1

#define QMSS_MTU_SIZE_BYTES     4096

#define RM_MSGQ_HEAP_ID         0

#define QMSS_MSGQ_HEAP_NAME     "qmssIpcHeapBuf"
#define QMSS_MSGQ_HEAP_ID       1

#define QMSS_TRANS_NET_ID       1

#define KILL_RM_HUB_MSG_ID      1

#define TEST_PASS               0
#define TEST_FAIL               1

/* MessageQ queue name for DSP RM Clients */
#define MESSAGEQ_RM_NAME_PREFIX   "RM_Client_DSP_"
/* MessageQ queue name for test */
#define MESSAGEQ_TEST_NAME_PREFIX "TEST_MsgQ_Proc_"

#define FILL_DATA_BYTE(proc, idx) (uint8_t)(((proc & 0xF) << 4) | (idx & 0xF))

typedef struct {
    MessageQ_MsgHeader header; /* 32 bytes */
    uint8_t            data[QMSS_MTU_SIZE_BYTES - sizeof(MessageQ_MsgHeader)];
} MsgQ_TestMsg;

/* IPC MessageQ RM packet encapsulation structure */
typedef struct {
    /* IPC MessageQ header (must be first element in structure) */
    MessageQ_MsgHeader msgQHeader;
    /* Pointer to RM packet */
    Rm_Packet          rmPkt;
} MsgQ_RmPacket;

/************************ GLOBAL VARIABLES ********************/

uint32_t testIterations;
uint32_t testFailed;

/* Queue parameters */
char localQueueName[32];
char remoteQueueName[32];

/* Core definitions according to IPC */
uint16_t ipcCoreId = 0;
uint16_t ipcPrevCoreId = 0;
uint16_t ipcNumDspCores = 0;

/* Core number according to the device (DNUM) */
uint32_t coreNum;

Task_Handle         rmTransSetupTskHandle = NULL;
Task_Handle         rmReceiveTskHandle = NULL;
Task_Handle         initTskHandle = NULL;
Task_Handle         testTskHandle = NULL;
Task_Handle         cleanupTskHandle = NULL;
Task_Handle         rmCleanupTskHandle = NULL;
int                 killRmRcvTask;
int                 rmCleanupDone;

/* Handle for heap that RM packets will be allocated from */
HeapBufMP_Handle    rmPktHeapHandle = NULL;

MessageQ_Handle      locRmMsgQ = NULL;
MessageQ_QueueId     rmServerQueueId;

MessageQ_Handle      localMessageQ = NULL;
MessageQ_QueueId     remoteQueueId;
GateMP_Handle        gateMpHandle;
HeapBufMP_Handle     heapHandle;
TransportQmss_Handle qmssTransHandle = NULL;

/* Descriptors aligned and padded to cache line */
#pragma DATA_SECTION (hostDesc, ".desc");
#pragma DATA_ALIGN (hostDesc, 128)
/* Total size must be multiple of cache line */
uint8_t hostDesc[HOST_DESC_NUM * HOST_DESC_SIZE_BYTES];

/* Sync indices into the syncInit array */
#define SYNC_INDEX_SYS_INIT  0
#define SYNC_INDEX_RM_INIT   1
/* Sync block for initialization barriers */
#pragma DATA_SECTION (syncInit, ".sync");
#pragma DATA_ALIGN (syncInit, 128)
volatile uint8_t syncInit[128];

/* Sync block for cleanup barriers */
#pragma DATA_SECTION (syncCleanup, ".sync");
#pragma DATA_ALIGN (syncCleanup, 128)
volatile uint8_t syncCleanup[NUM_DSP_CORES][128];

Rm_Handle           rmHandle = NULL;
Rm_ServiceHandle   *rmServiceHandle = NULL;
Rm_TransportHandle  serverTransHandle;

/*************** EXTERN VARIABLES & FUNCTIONS *****************/
/* QMSS device specific configuration */
extern Qmss_GlobalConfigParams qmssGblCfgParams;
/* CPPI device specific configuration */
extern Cppi_GlobalConfigParams cppiGblCfgParams;

/* TransportQmss device specification configuration */
extern TransportQmss_DeviceConfigParams qmssTransCfgParams;

/*************************** FUNCTIONS ************************/

static void wb(void *addr, uint32_t sizeBytes)
{
#ifdef L2_CACHE
    /* Writeback L2 */
    CACHE_wbL2(addr, sizeBytes, CACHE_WAIT);
#else
    /* Writeback L1D */
    CACHE_wbL1d(addr, sizeBytes, CACHE_WAIT);
#endif
}

static void inv(void *addr, uint32_t sizeBytes)
{
#ifdef L2_CACHE
    /* Invalidate L2 */
    CACHE_invL2(addr, sizeBytes, CACHE_WAIT);
#else
    CACHE_invL1d(addr, sizeBytes, CACHE_WAIT);
#endif
}

/**
 *  @b Description
 *  @n
 *      Configures the descriptor region and initializes CPPI and QMSS.
 *      This function should only be called once across all DSP cores.
 *
 *  @retval
 *      Success     - 0
 *  @retval
 *      Error       - <0
 */
static int32_t systemInit(void)
{
    Qmss_InitCfg    qmssInitConfig; /* QMSS configuration */
    Qmss_MemRegInfo memInfo; /* Memory region configuration information */
    Qmss_Result     result;
    Cppi_StartCfg   cppiStartCfg;

    System_printf("Core %d : L1D cache size %d. L2 cache size %d.\n", coreNum,
                  CACHE_getL1DSize(), CACHE_getL2Size());

    /* Start the timestamp counter */
    TSCL = 0;

    memset((void *) &qmssInitConfig, 0, sizeof (Qmss_InitCfg));
    /* Set up the linking RAM. Use the internal Linking RAM. LLD will configure
     * the internal linking RAM address and maximum internal linking RAM size
     * if a value of zero is specified.  Linking RAM1 is not used */
    qmssInitConfig.linkingRAM0Base = 0;
    qmssInitConfig.linkingRAM0Size = 0;
    qmssInitConfig.linkingRAM1Base = 0;
    qmssInitConfig.maxDescNum      = HOST_DESC_NUM;
    /* Download PDSP3 firmware.  PDSP1 will not be used to avoid potential
     * firmware lockup caused by usage contention with Linux */
#ifdef xdc_target__bigEndian
    qmssInitConfig.pdspFirmware[2].pdspId = Qmss_PdspId_PDSP3;
    qmssInitConfig.pdspFirmware[2].firmware = (void *) &acc48_be;
    qmssInitConfig.pdspFirmware[2].size = sizeof (acc48_be);
#else
    qmssInitConfig.pdspFirmware[2].pdspId = Qmss_PdspId_PDSP3;
    qmssInitConfig.pdspFirmware[2].firmware = (void *) &acc48_le;
    qmssInitConfig.pdspFirmware[2].size = sizeof (acc48_le);
#endif

    qmssGblCfgParams.qmRmServiceHandle = rmServiceHandle;
    /* Bypass hardware initialization as it is done within Kernel */
    qmssInitConfig.qmssHwStatus = QMSS_HW_INIT_COMPLETE;

    /* Initialize Queue Manager SubSystem */
    result = Qmss_init(&qmssInitConfig, &qmssGblCfgParams);
    if (result != QMSS_SOK) {
        System_printf("Error Core %d : "
                      "Initializing Queue Manager SubSystem error code : %d\n",
                      coreNum, result);
        return(-1);
    } else {
        System_printf("Core %d : QMSS initialized\n", coreNum);
    }

    /* Start the QMSS. */
    if (Qmss_start() != QMSS_SOK) {
        System_printf("Error Core %d : Unable to start the QMSS\n", coreNum);
        return(-1);
    }

    result = Cppi_init(&cppiGblCfgParams);
    if (result != CPPI_SOK) {
        System_printf("Error Core %d : "
                      "Initializing CPPI LLD error code : %d\n", coreNum,
                      result);
    } else {
        System_printf("Core %d : CPPI initialized\n", coreNum);
    }

    /* Register RM with CPPI */
    cppiStartCfg.rmServiceHandle = rmServiceHandle;
    Cppi_startCfg(&cppiStartCfg);

    /* Setup memory region for host descriptors */
    memset((void *) &hostDesc, 0, HOST_DESC_NUM * HOST_DESC_SIZE_BYTES);
    memset((void *) &memInfo, 0, sizeof(memInfo));
    memInfo.descBase       = (uint32_t *) hostDesc;
    memInfo.descSize       = HOST_DESC_SIZE_BYTES;
    memInfo.descNum        = HOST_DESC_NUM;
    memInfo.manageDescFlag = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
    memInfo.startIndex     = QMSS_START_INDEX_NOT_SPECIFIED;
    memInfo.memRegion      = (Qmss_MemRegion)HOST_DESC_MEM_REGION;
    result = Qmss_insertMemoryRegion(&memInfo);
    if (result < QMSS_SOK) {
        System_printf("Error Core %d : "
                      "Inserting memory region, error code : %d\n", coreNum,
                      result);
        return(-1);
    } else {
        System_printf("Core %d : Memory region %d inserted\n", coreNum,
                      HOST_DESC_MEM_REGION);
    }

    /* Writeback the descriptor pool. */
    wb((void *)hostDesc, HOST_DESC_NUM * HOST_DESC_SIZE_BYTES);

    return(0);
}

/**
 *  @b Description
 *  @n
 *      Deletes descriptor region and shuts down CPPI and QMSS.
 *      This function should only be called once across all DSP cores.
 *
 *  @retval
 *      Success     - 0
 *  @retval
 *      Error       - <0
 */
static int32_t systemDeInit(void)
{
    Qmss_Result qmssResult;
    Cppi_Result cppiResult;

    /* Remove the memory region. */
    qmssResult = Qmss_removeMemoryRegion(HOST_DESC_MEM_REGION, 0);
    if (qmssResult < QMSS_SOK) {
        System_printf("Error Core %d : Removing memory region: %d\n", coreNum,
                      qmssResult);
        return(-1);
    }

    /* De Initialize CPPI CPDMA */
    cppiResult = Cppi_exit();
    if (cppiResult != CPPI_SOK) {
        System_printf("Error Core %d : Deinitializing CPPI error code : %d\n",
                      coreNum, cppiResult);
        return(-1);
    }

    return(0);
}

static Rm_Packet *transportAlloc(Rm_AppTransportHandle appTransport,
                                 uint32_t pktSize, Rm_PacketHandle *pktHandle)
{
    MsgQ_RmPacket *rmMsg = NULL;
    Rm_Packet     *rmPkt = NULL;

    /* Allocate a messageQ message for containing the RM packet */
    rmMsg = (MsgQ_RmPacket *)MessageQ_alloc(RM_MSGQ_HEAP_ID, sizeof(*rmMsg));
    if (rmMsg) {
        MessageQ_setReplyQueue(locRmMsgQ, (MessageQ_Msg)rmMsg);
        rmPkt = &(rmMsg->rmPkt);
        rmPkt->pktLenBytes = pktSize;
        *pktHandle = (Rm_PacketHandle)rmMsg;
    } else {
        System_printf("Error Core %d : "
                      "MessageQ_alloc failed to allocate message: %d\n",
                      coreNum);
        *pktHandle = NULL;
    }
    return(rmPkt);
}

static void transportFree(MessageQ_Msg rmMsgQMsg)
{
    int32_t  status;

    status = MessageQ_free(rmMsgQMsg);
    if (status < 0) {
        System_printf("Error Core %d : "
                      "MessageQ_free failed to free message: %d\n",
                      coreNum, status);
    }
}

static int32_t transportSend(Rm_AppTransportHandle appTransport,
                             Rm_PacketHandle pktHandle)
{
    MessageQ_QueueId *remoteQueueId = (MessageQ_QueueId *)appTransport;
    MsgQ_RmPacket    *rmMsg = pktHandle;
    int32_t           status;

    /* Send the message to RM Server */
    status = MessageQ_put(*remoteQueueId, (MessageQ_Msg)rmMsg);
    if (status < 0) {
        System_printf("Error Core %d : "
                      "MessageQ_put failed to send message: %d\n",
                      coreNum, status);
    }
    return(0);
}

static void transportReceive(void)
{
    int32_t       numPkts;
    MessageQ_Msg  rmMsg = NULL;
    Rm_Packet    *rmPkt = NULL;
    int32_t       status;
    uint32_t      i;

    /* Check if any packets available */
    numPkts = (int32_t) MessageQ_count(locRmMsgQ);

    /* Process all available packets */
    for (i = 0; i < numPkts; i++) {
        status = (int32_t) MessageQ_get(locRmMsgQ, &rmMsg, MessageQ_FOREVER);
        if (rmMsg == NULL) {
            System_printf("Error Core %d : "
                          "MessageQ_get failed, returning a NULL packet\n",
                          coreNum);
        } else {
            /* Extract the Rm_Packet from the RM msg */
            rmPkt = &(((MsgQ_RmPacket *)rmMsg)->rmPkt);

            /* Provide packet to RM for processing */
            if (status = Rm_receivePacket(serverTransHandle, rmPkt)) {
                System_printf("Error Core %d : "
                              "RM failed to process received packet: %d\n",
                              coreNum, status);
            }

            /* Free RM packet buffer and messageQ message */
            transportFree(rmMsg);
        }
    }
}

void rmCleanupTsk(UArg arg0, UArg arg1)
{
    int32_t rmResult;
    Int     status;

    System_printf("Core %d : Cleaning up RM Client\n", coreNum);

    if (rmReceiveTskHandle) {
        Task_delete(&rmReceiveTskHandle);
        rmReceiveTskHandle = NULL;
    }

    rmResult = Rm_transportUnregister(serverTransHandle);
    if (rmResult != RM_OK) {
        System_printf ("Error Core %d : "
                       "Error %d when unregistering transport from RM Client\n",
                       coreNum, rmResult);
        testFailed = TEST_FAIL;
        goto rm_cleanup_error;
    }

    status = MessageQ_delete(&locRmMsgQ);
    if (status != MessageQ_S_SUCCESS) {
        System_printf ("Error Core %d : MessageQ_delete of local RM queue\n",
                       coreNum);
        testFailed = TEST_FAIL;
        goto rm_cleanup_error;
    }

    rmResult = Rm_serviceCloseHandle(rmServiceHandle);
    if (rmResult != RM_OK) {
        System_printf("Error Core %d : "
                      "Closing RM service handle error code : %d\n", coreNum,
                      rmResult);
        testFailed = TEST_FAIL;
        goto rm_cleanup_error;
    }

    rmResult = Rm_delete(rmHandle, 1);
    if (rmResult != RM_OK) {
        System_printf("Error Core %d : "
                      "Deleting RM Client error code : %d\n",
                      coreNum, rmResult);
        testFailed = TEST_FAIL;
        goto rm_cleanup_error;
    }

    System_printf("Core %d : RM Client cleanup complete\n", coreNum);

rm_cleanup_error:
    rmCleanupDone = 1;
}

void rmReceiveTsk(UArg arg0, UArg arg1)
{
    Task_Params taskParams;

    while(!killRmRcvTask) {
        transportReceive();
        /* Sleep for 1ms so that the main test tasks can run */
        Task_sleep(1);
    }

    System_printf("Core %d : Creating RM cleanup task...\n", coreNum);
    Task_Params_init (&taskParams);
    taskParams.priority = 1;
    rmCleanupTskHandle = Task_create(rmCleanupTsk, &taskParams, NULL);
}

void cleanupTsk(UArg arg0, UArg arg1)
{
    Int         status;
    Qmss_Result qmssResult;
    int32_t     i;
    
    System_printf("Core %d : "
                  "================== CLEANUP ==================\n", coreNum);

    if (rmTransSetupTskHandle) {
        Task_delete(&rmTransSetupTskHandle);
        rmTransSetupTskHandle = NULL;
    }

    if (initTskHandle) {
        Task_delete(&initTskHandle);
        initTskHandle = NULL;
    }

    if (testTskHandle) {
        Task_delete(&testTskHandle);
        testTskHandle = NULL;
    }

    System_printf("Core %d : Closing remote MessageQ\n", coreNum);
    status = MessageQ_close(&remoteQueueId);
    if (status != MessageQ_S_SUCCESS) {
        testFailed = TEST_FAIL;
        System_printf ("Error Core %d : MessageQ_close\n", coreNum);
        goto cleanup_error;
    }
    System_printf("Core %d : Deleting local MessageQ\n", coreNum);
    status = MessageQ_delete(&localMessageQ);
    if (status != MessageQ_S_SUCCESS) {
        testFailed = TEST_FAIL;
        System_printf ("Error Core %d : MessageQ_delete\n", coreNum);
        goto cleanup_error;
    }

    System_printf("Core %d : Deleting TransportQmss instance\n", coreNum);
    TransportQmss_delete(&qmssTransHandle);
    qmssTransHandle = NULL;

    System_printf("Core %d : Unregistering MessageQ heap\n", coreNum);
    status = MessageQ_unregisterHeap(QMSS_MSGQ_HEAP_ID);
    if (status != MessageQ_S_SUCCESS) {
        testFailed = TEST_FAIL;
        System_printf ("Error Core %d : MessageQ_unregisterHeap\n", coreNum);
        goto cleanup_error;
    }

    if (coreNum == SYSINIT) {
        /* Wait for other cores to complete cleanup */
        for (i = 0; i < NUM_DSP_CORES; i++) {
            if (i != coreNum) {
                do {
                    inv((void *) &syncCleanup[i][0], sizeof(syncCleanup) /
                        NUM_DSP_CORES);
                } while (!syncCleanup[i][0]);
            }
        }

        System_printf("Core %d : Closing HeapBufMP\n", coreNum);
        status = HeapBufMP_close(&heapHandle);
        if (status != HeapBufMP_S_SUCCESS) {
            testFailed = TEST_FAIL;
            System_printf("Error Core %d : HeapBufMP_close\n", coreNum);
            goto cleanup_error;
        }

        System_printf("Core %d : Deleting GateMP used by HeapBufMP\n", coreNum);
        status = GateMP_delete(&gateMpHandle);
        if (status != GateMP_S_SUCCESS) {
            testFailed = TEST_FAIL;
            System_printf("Error Core %d : GateMP_close\n", coreNum);
            goto cleanup_error;
        }

        System_printf("Core %d : De-initializing QMSS and CPPI\n", coreNum);
        status = systemDeInit();
        if (status < 0) {
            testFailed = TEST_FAIL;
            goto cleanup_error;
        }

        /* Exit qmss */
        System_printf ("Core %d : exit QMSS\n", coreNum);
        if ((qmssResult = Qmss_exit())) {
            testFailed = TEST_FAIL;
            System_printf("Error Core %d : exit error code : %d\n", coreNum,
                          qmssResult);
            goto cleanup_error;
        }
    } else {
        System_printf("Core %d : Closing HeapBufMP\n", coreNum);
        status = HeapBufMP_close(&heapHandle);
        if (status != HeapBufMP_S_SUCCESS) {
            testFailed = TEST_FAIL;
            System_printf("Error Core %d : HeapBufMP_close\n", coreNum);
            goto cleanup_error;
        }
    }

    /* Tell RM receive task to kill itself and cleanup */
    rmCleanupDone = 0;
    killRmRcvTask = 1;
    while(!rmCleanupDone) {
        /* Sleep so rmCleanupTsk can execute to completion */
        Task_sleep(1);
    }

    if (rmCleanupTskHandle) {
        Task_delete(&rmCleanupTskHandle);
        rmCleanupTskHandle = NULL;
    }

    /* Tell Linux RM request hub to shut down.  Hub will know to
     * shutdown when it receives a message with the proper msgId */
    if (coreNum == SYSINIT) {
        MessageQ_Msg killHubMsg = NULL;

        killHubMsg = MessageQ_alloc(RM_MSGQ_HEAP_ID, sizeof(*killHubMsg));
        if (killHubMsg) {
            System_printf("Core %d : Signaling Linux to kill RM message hub\n",
                          coreNum);
            MessageQ_setMsgId(killHubMsg, KILL_RM_HUB_MSG_ID);
            /* Send the message to RM message hub */
            status = MessageQ_put(rmServerQueueId, killHubMsg);
            if (status < 0) {
                System_printf("Error Core %d : "
                              "Failed to send kill message to RM msg hub: %d\n",
                              coreNum, status);
                testFailed = TEST_FAIL;
                goto cleanup_error;
            }
        } else {
            System_printf("Error core %d : "
                          "Could not allocate msg to kill RM message hub\n",
                          coreNum);
            testFailed = TEST_FAIL;
            goto cleanup_error;
        }
    }

    status = Ipc_stop();
    if (status < 0) {
        System_printf("Error Core %d: Ipc_stop failed with error %d\n",
                      coreNum, status);
        testFailed = TEST_FAIL;
    }

cleanup_error:

    /* SYSINIT cannot cleanup until all other cores have completed cleanup */
    if (coreNum != SYSINIT) {
        syncCleanup[coreNum][0] = 1;
        wb((void *) &syncCleanup[coreNum][0], sizeof(syncCleanup) /
                                                     NUM_DSP_CORES);
    }

    System_printf("Core %d : *********************************************\n",
                  coreNum);
    System_printf("Core %d : Completed Test - ", coreNum);
    if (testFailed) {
        System_printf("FAILED\n");
    } else {
        System_printf("PASSED\n");
    }
    System_printf("Core %d : Cleaning up and exiting...\n", coreNum);
    System_printf("Core %d : *********************************************\n",
                  coreNum);
}

void testTsk(UArg arg0, UArg arg1)
{
    Task_Params   taskParams;
    Int           status;
    MsgQ_TestMsg *msg = NULL;
    int           i, j;
    uint8_t       expected;

    System_printf("Core %d : "
                  "===== Start Host-DSP Bidirectional Test =====\n", coreNum);

    System_printf("Core %d : Waiting for test messages from Host on Qid 0x%x\n",
                  coreNum, MessageQ_getQueueId(localMessageQ));
    for (i = 0; i < NUM_TEST_ITERATIONS; i++) {
        System_printf("Core %d : ### Round Trip - %4d ###\n", coreNum, i+1);
        status = MessageQ_get(localMessageQ, (MessageQ_Msg *)&msg,
                              MessageQ_FOREVER);
        if (status < 0) {
            System_printf("Error Core %d : MessageQ_get failed\n",
                          coreNum);
            testFailed = TEST_FAIL;
            goto cleanup;
        }

        for (j = 0; j < sizeof(msg->data); j++) {
            expected = FILL_DATA_BYTE(MultiProc_getBaseIdOfCluster(), j);
            if (msg->data[j] != expected) {
                System_printf("Error Core %d : Bad data byte %4d\n"
                              "                Got:      0x%2x\n"
                              "                Expected: 0x%2x\n",
                              coreNum, i, msg->data[j], expected);
                testFailed = TEST_FAIL;
                goto cleanup;
            }
        }
        System_printf("Core %d : Received msg with good data from %s\n",
                      coreNum, MultiProc_getName(msg->header.srcProc));

        for (j = 0; j < sizeof(msg->data); j++) {
            msg->data[j] = FILL_DATA_BYTE(MultiProc_self(), j);
        }
        System_printf("Core %d : Sending msg back to %s\n", coreNum,
                      MultiProc_getName(msg->header.srcProc));
        MessageQ_setTransportId(msg, QMSS_TRANS_NET_ID);
        status = MessageQ_put(remoteQueueId, (MessageQ_Msg)msg);
        if (status < 0) {
            System_printf("Error Core %d : MessageQ_put failed\n",
                          coreNum);
            testFailed = TEST_FAIL;
            goto cleanup;
        }
        msg = NULL;
    }

cleanup:
    if (msg) {
        MessageQ_free((MessageQ_Msg)msg);
    }

    /* Create the cleanup task */
    System_printf("Core %d : Creating cleanup task...\n", coreNum);
    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    cleanupTskHandle = Task_create(cleanupTsk, &taskParams, NULL);
}

void initTsk(UArg arg0, UArg arg1)
{
    Task_Params           taskParams;
    Qmss_StartCfg         qmssStartCfg;
    Cppi_StartCfg         cppiStartCfg;
    int32_t               status;
    Error_Block           errorBlock;
    TransportQmss_Params  transQmssParams;
    GateMP_Params         gateMpParams;
    HeapBufMP_Params      heapBufParams;

    /* System initializations for each core. */
    if (coreNum == SYSINIT) {
        /* Run QMSS and CPPI system wide initializations */
        System_printf("Core %d : Initialize system peripherals\n", coreNum);
        status = systemInit();
        if (status != 0) {
            System_printf("Error Core %d : initializing system peripherals\n",
                          coreNum);
            return;
        }

        /* Signal to other DSPs that system init is complete */
        syncInit[SYNC_INDEX_SYS_INIT] = 1;
        wb((void *) &syncInit, sizeof(syncInit));
    } else {
        System_printf("Core %d : Waiting for QMSS to be initialized.\n",
                      coreNum);

        /* Wait for SYSINIT core to complete system init */
        do {
            inv((void *) &syncInit, sizeof(syncInit));
        } while (!syncInit[SYNC_INDEX_SYS_INIT]);

        /* Start Queue Manager SubSystem with RM */
        qmssStartCfg.rmServiceHandle = rmServiceHandle;
        qmssStartCfg.pQmssGblCfgParams = &qmssGblCfgParams;
        status = Qmss_startCfg(&qmssStartCfg);
        if (status != QMSS_SOK) {
            System_printf("Error Core %d : Unable to start the QMSS\n",
                          coreNum);
            return;
        }

        /* Register RM with CPPI */
        cppiStartCfg.rmServiceHandle = rmServiceHandle;
        Cppi_startCfg(&cppiStartCfg);

        System_printf("Core %d : QMSS can now be used.\n", coreNum);
    }

    if (coreNum == SYSINIT) {
        /* Create the heap that will be used to allocate messages. */
        GateMP_Params_init(&gateMpParams);
        gateMpParams.localProtect = GateMP_LocalProtect_INTERRUPT;
        gateMpHandle = GateMP_create(&gateMpParams);
        
        HeapBufMP_Params_init(&heapBufParams);
        heapBufParams.regionId  = 0;
        heapBufParams.name      = QMSS_MSGQ_HEAP_NAME;
        heapBufParams.numBlocks = HOST_DESC_NUM;
        heapBufParams.blockSize = QMSS_MTU_SIZE_BYTES;
        /* GateMP so allocation can take place within QMSS rx interrupt
         * context */
        heapBufParams.gate      = gateMpHandle;
        heapHandle = HeapBufMP_create(&heapBufParams);
        if (heapHandle == NULL) {
            System_printf("Error Core %d : HeapBufMP_create failed\n", coreNum);
            return;
        }
    } else {
        /* Open the heap created by the other processor. Loop until opened. */
        do {
            status = HeapBufMP_open(QMSS_MSGQ_HEAP_NAME, &heapHandle);
        } while (status < 0);
    }

    /* Each core must register this heap with MessageQ */
    MessageQ_registerHeap((IHeap_Handle)heapHandle, QMSS_MSGQ_HEAP_ID);

    /* Create QMSS Accumulator or QPEND transport instance.  They will be
     * network transports so won't interfere with default MessageQ transport,
     * shared memory notify transport */
    TransportQmss_Params_init(&transQmssParams);
    /* Configure common parameters */
    transQmssParams.deviceCfgParams = &qmssTransCfgParams;
    transQmssParams.txMemRegion     = HOST_DESC_MEM_REGION;
    /* Descriptor pool divided between all cores.  Account for send/receive
     * (divide by 2) */
    transQmssParams.txNumDesc       = (HOST_DESC_NUM / 2) / NUM_DSP_CORES;
    transQmssParams.txDescSize      = HOST_DESC_SIZE_BYTES;
    transQmssParams.rxMemRegion     = HOST_DESC_MEM_REGION;
    /* Descriptor pool divided between all cores.  Account for send/receive
     * (divide by 2) */
    transQmssParams.rxNumDesc       = (HOST_DESC_NUM / 2) / NUM_DSP_CORES;
    transQmssParams.rxDescSize      = HOST_DESC_SIZE_BYTES;
    transQmssParams.rxMsgQHeapId    = QMSS_MSGQ_HEAP_ID;
    transQmssParams.maxMTU          = QMSS_MTU_SIZE_BYTES;
    transQmssParams.rmServiceHandle = rmServiceHandle;
    transQmssParams.rxIntVectorId   = 8;
    transQmssParams.transNetworkId  = QMSS_TRANS_NET_ID;

    /* Receive type specific parameters */
    if (coreNum == SYSINIT) {
        /* SYSINIT core creates TransportQmss instance with QPEND receive
         * logic */
        transQmssParams.rcvQParams.qType             = TransportQmss_queueRcvType_QPEND;
        /* Choose an arbitrary system event from Table 6-22 System Event
         * Mapping in tci6638k2k.pdf.  System event can be anything that is not
         * already in use and maps to a different CIC host interrupt per DSP */
        transQmssParams.rcvQParams.qpend.systemEvent = 43;

        System_printf("Core %d : "
                      "Creating QMSS Transport instance with rx QPEND queue\n",
                      coreNum);
    } else {
        /* Other cores create TransportQmss instance with accumulator
         * receive logic */
        transQmssParams.rcvQParams.qType             = TransportQmss_queueRcvType_ACCUMULATOR;
        transQmssParams.rcvQParams.accum.rxAccQType  = Qmss_QueueType_HIGH_PRIORITY_QUEUE;
        /* Use PDSP3 since Linux uses PDSP1.  Using the same PDSP as Linux can
         * cause a potential PDSP firmware lockup since Linux does not use the
         * critical section preventing commands being sent to a PDSP
         * simultaneously */
        transQmssParams.rcvQParams.accum.qmPdsp      = (UInt32)Qmss_PdspId_PDSP3;
        /* Must map to a valid channel for each DSP core.  Follow sprugr9f.pdf
         * Table 5-9 */
        transQmssParams.rcvQParams.accum.accCh       = DNUM;
        transQmssParams.rcvQParams.accum.accTimerCnt = 0;

        System_printf("Core %d : "
                      "Creating QMSS Transport instance with rx Accumulator "
                      "queue\n", coreNum);
    }

    Error_init(&errorBlock);
    qmssTransHandle = TransportQmss_create(&transQmssParams, &errorBlock);
    if (qmssTransHandle == NULL) {
        System_printf("Error Core %d : "
                      "TransportQmss_create failed with id %d\n", coreNum,
                      errorBlock.id);
        return;
    }

    /* Create MessageQs after transport instances have been created since
     * the queues will be bound to the transport instances during
     * MessageQ_create */
    System_sprintf(localQueueName, "%s%d", MESSAGEQ_TEST_NAME_PREFIX,
                   MultiProc_self());
    System_sprintf(remoteQueueName, "%s%d", MESSAGEQ_TEST_NAME_PREFIX,
                   MultiProc_getBaseIdOfCluster());

    System_printf("Core %d : "
                  "localQueueName=%s. remoteQueueName=%s\n",
                  coreNum, localQueueName, remoteQueueName);

    /* Create a message queue. */
    localMessageQ = MessageQ_create(localQueueName, NULL);
    if (localMessageQ == NULL) {
        System_printf("Error Core %d : MessageQ_create failed\n", coreNum);
        return;
    }

    /* Open the 'next' remote message queue. Spin until it is ready. */
    do {
        status = MessageQ_open(remoteQueueName, &remoteQueueId);
    } while (status < 0);

    /* Create the test task */
    System_printf("Core %d : Creating test task...\n", coreNum);
    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    testTskHandle = Task_create(testTsk, &taskParams, NULL);
}

void rmTransSetupTsk(UArg arg0, UArg arg1)
{
    Char             localQueueName[32];
    MessageQ_Msg     msg;
    Rm_TransportCfg  rmTransCfg;
    int32_t          result;
    Task_Params      taskParams;

    /* Create the RM message queue */
    System_sprintf(localQueueName, "%s%d", MESSAGEQ_RM_NAME_PREFIX,
                   MultiProc_self());
    locRmMsgQ = MessageQ_create(localQueueName, NULL);
    if (locRmMsgQ == NULL) {
        System_printf("Error Core %d : MessageQ_create failed\n", coreNum);
        return;
    } else {
        System_printf("Core %d : Created MessageQ queue %s\n", coreNum,
                      localQueueName);
    }

    /* Wait for handshake from Host */
    MessageQ_get(locRmMsgQ, &msg, MessageQ_FOREVER);
    rmServerQueueId = MessageQ_getReplyQueue(msg);
    System_printf("Core %d : "
                  "Received Host handshake.  Sending reply to QID 0x%x\n",
                  coreNum, rmServerQueueId);
    MessageQ_put(rmServerQueueId, msg);
    /* Wait for ready msg from Host */
    MessageQ_get(locRmMsgQ, &msg, MessageQ_FOREVER);
    System_printf("Core %d : Received Host ready msg\n", coreNum);
    MessageQ_free(msg);

    /* Register the Server with the Client instance */
    rmTransCfg.rmHandle = rmHandle;
    rmTransCfg.appTransportHandle = (Rm_AppTransportHandle)&rmServerQueueId;
    rmTransCfg.remoteInstType = Rm_instType_SERVER;
    rmTransCfg.transportCallouts.rmAllocPkt = transportAlloc;
    rmTransCfg.transportCallouts.rmSendPkt = transportSend;
    serverTransHandle = Rm_transportRegister(&rmTransCfg, &result);
    if (result != RM_OK) {
        System_printf("Error Core %d : "
                      "RM transport registration failed with error %d\n",
                      coreNum, result);
        return;
    }
    
    /* Create the RM receive task with higher priority than other tasks */
    killRmRcvTask = 0;
    System_printf("Core %d : Creating RM receive task...\n", coreNum);
    Task_Params_init (&taskParams);
    taskParams.priority = 2;
    rmReceiveTskHandle = Task_create(rmReceiveTsk, &taskParams, NULL);

    System_printf("Core %d : Creating test init task...\n", coreNum);
    Task_Params_init (&taskParams);
    taskParams.priority = 1;
    initTskHandle = Task_create(initTsk, &taskParams, NULL);
}

void main(int32_t argc, char *argv[])
{
    Rm_InitCfg  rmInitCfg;
    char        rmInstName[RM_NAME_MAX_CHARS];
    int32_t     rmResult;
    Task_Params taskParams;
    Int         status;

    coreNum = CSL_chipReadReg(CSL_CHIP_DNUM);
    ipcCoreId = MultiProc_self();
    ipcNumDspCores = NUM_DSP_CORES;
    testIterations = 0;
    testFailed = TEST_PASS;

    System_printf("Core %d : "
                  "******************************************************\n",
                  coreNum);
    System_printf("Core %d : "
                  "SYS/BIOS DSP TransportQmss Heterogeneous Test (DSP EP)\n",
                  coreNum);
    System_printf("Core %d : "
                  "******************************************************\n",
                  coreNum);
    System_printf("Core %d : Device name:               %s\n", coreNum,
                  DEVICENAME);
    System_printf("Core %d : Processor names:           %s\n", coreNum,
                  PROCNAMES);
    System_printf("Core %d : IPC Core ID:               %d\n", coreNum,
                  ipcCoreId);
    System_printf("Core %d : Number of DSPs             %d\n", coreNum,
                  ipcNumDspCores);
    System_printf("Core %d : Number of test iterations: %d\n", coreNum,
                  NUM_TEST_ITERATIONS);
    System_printf("Core %d : Starting IPC core %d with name (\"%s\")\n",
                  coreNum, ipcCoreId, MultiProc_getName(ipcCoreId));

    /* Setup TransportRpmsg for host communication */
    IpcMgr_ipcStartup();
    /* Setup IPC for DSP to DSP communication */
    status = Ipc_start();
    if (status < 0) {
        System_printf("Error Core %d: Ipc_start failed\n", coreNum);
        return;
    }

    /* Initialize RM */
    memset(&rmInitCfg, 0, sizeof(rmInitCfg));
    System_sprintf(rmInstName, "RM_Client%d", ipcCoreId);
    rmInitCfg.instName = rmInstName;
    rmInitCfg.instType = Rm_instType_CLIENT;
    rmHandle = Rm_init(&rmInitCfg, &rmResult);
    if (rmResult != RM_OK) {
        System_printf("Error Core %d : "
                      "Initializing Resource Manager error code : %d\n",
                      coreNum, rmResult);
        return;
    }

    rmServiceHandle = Rm_serviceOpenHandle(rmHandle, &rmResult);
    if (rmResult != RM_OK) {
        System_printf("Error Core %d : "
                      "Creating RM service handle error code : %d\n", coreNum,
                      rmResult);
        return;
    }

    /* Create the RM transport setup task */
    System_printf("Core %d : Creating RM transport setup task...\n", coreNum);
    Task_Params_init (&taskParams);
    rmTransSetupTskHandle = Task_create(rmTransSetupTsk, &taskParams, NULL);

    System_printf("Core %d : Starting BIOS...\n", coreNum);
    BIOS_start();
}

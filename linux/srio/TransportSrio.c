/**
 *   @file  TransportSrio.c
 *
 *   @brief
 *      INetworkTransport interface implementation for IPC ARMv7 Linux
 *      MessageQ over MPM-Transport SRIO
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/*
 *  ======== TransportSrio.c ========
 */

/* Standard includes */
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/interfaces/INetworkTransport.h>

#include "TransportSrio.h"

/* LLD includes */
#include <ti/drv/rm/rm_services.h>

/* MPM-Transport includes */
#include <mpm_transport.h>

/* Transport version includes */
#include <../Transportver.h>

/* Global Variable that describes the Linux IPC Transport Version Information */
const char  transportSrioVersionStr[] = TRANSPORT_VERSION_STR ":" __DATE__  ":" __TIME__;

/* TransportSrio Type 11 instance lock.  Only one TransportSrio Type 11 
 * instance can exist on a Linux Host across all processes */
#define TRANSSRIO_T11_INST_LOCK      "TRANSSRIO_T11_CREATED"
/* TransportSrio Type 9 instance lock.  Only one TransportSrio Type 9 
 * instance can exist on a Linux Host across all processes */
#define TRANSSRIO_T9_INST_LOCK       "TRANSSRIO_T9_CREATED"
/* Initial allocation size of sorted list used for destination MessageQ queue
 * ID cache */
#define QUEUEID_CACHE_INIT_SIZE      8
/* Return value for MessageQ queue IDs not found in the queue cache */
#define UNCACHED_QUEUE_ID            (-1)
/* Default stack size for transportSrioRcv thread */
#define RX_THREAD_DEFAULT_STACK_SIZE 0x8000

/* SRIO HW max MTU */
#define SRIO_HW_MAX_MTU              4096

/* Data stucture that tracks the usage of the MessageQ queue ID cache sorted
 * list */
typedef struct {
    /* Total number elements in the queue cache list */
    uint32_t  total;
    /* Number of used entries in the queue cache list */
    uint32_t  used;
    /* Pointer to an array storing the cache of sorted MessageQ queue IDs */
    uint32_t *qid_list;
} qid_cache_t;

/* Fragmentation/reassembly header for packets greater than or equal to
 * 4096 bytes */
typedef struct {
    uint8_t seqn;
    uint8_t max_seqn;
    uint8_t tag;
    uint8_t reserved;
} frag_hdr_t;

/* Source endpoint fragmentation stream tracker */
typedef struct {
    uint8_t   tag;
    int8_t    prev_seqn;
    uint16_t  buf_len;
    uint8_t  *rcv_buf;
} frag_ep_t;

/*
 *  ======== Params Structure Evolution ========
 */
 
/* Initial version of TransportSrio_Params defined in TransportSrio.h.  Please
 * see TransportSrio.h for parameter element definitions */
 typedef struct {
    Int                          __version;
    Char                        *mpm_trans_inst_name;
    Void                        *rm_service_h;
    TransportSrio_SocketParams  *sock_params;
    Int                          max_mtu;
    Int                          reroute_tid;
    Int                        (*srio_device_init) (Void *init_cfg,
                                                    Void *srio_base_addr,
                                                    UInt32 serdes_addr);
    Void                        *init_cfg;
    Int                        (*srio_device_deinit) (Void *deinit_cfg,
                                                      Void *srio_base_addr);
    Void                        *deinit_cfg;
    Int                          mpm_trans_init_qmss;
 } TransportSrio_Params_Version1;
 
/*
 *  ======== TransportSrio_Object ========
 *  TransportSrio instance object definition
 */
typedef struct {
    /* TransportSrio is a INetworkTransport so must inherit the
     * INetworkTransport object */
    INetworkTransport_Object    base;
    /* Pointer to SRIO MPM Transport instance */
    void                       *mpm_transport_handle;
    /* Pointer to RM instance service handle needed for RM NameServer usage */
    Rm_ServiceHandle           *rm_service_h;
    /* Maximum size, in bytes, of data packets sent and received over
     * SRIO MPM-Transport.  MessageQ messages greater than equal to or greater
     * than max_mtu bytes will be fragmented into segments of max_mtu size prior
     * to sending. max_mtu cannot exceed the SRIO HW's max MTU size of 4096 */
    int                         max_mtu;
    /* Thread handle for the transportSrioRcv data reception thread */
    pthread_t                   rx_thread_h;
    /* Stores a copy of the SRIO socket parameters.  The parameters include
     * endpoint addresses for all processors in the system indexed based on
     * the IPC MultiProc ID for each processor.  The destination processor ID
     * in send MessageQ messages is used to find the SRIO endpoint address 
     * (Type 11 or Type 9, based on the TransportSrio object configuration)
     * for the destination processor. */
    TransportSrio_SocketParams  sockets;
    /* Transport network ID for the network transport that is to be used to
     * reroute MessageQ messages received but are destined for a MessageQ
     * queue that was opened within a different Linux process. */
    int                         reroute_tid;
    qid_cache_t                 qid_cache;
    /* Tracks the last allocated MessageQ message in the transportSrioRcv
     * thread.  The thread can be cancelled before the receive message has been
     * passed to the application or properly freed within the context of the
     * thread.  The main process will free the message within the cleanup logic
     * if the latter occurs */
    MessageQ_Msg                allocd_rcv_msg;
    /* Pointer to array of source endpoint fragmentation stream tracking
     * structures.  The number of elements in the array is equivalent to the
     * number of SRIO endpoints in the system */
    frag_ep_t                  *frag_srcs;
    /* Current fragmentation stream tag being reassembled. */
    uint8_t                     frag_msg_tag;
} TransportSrio_Object;

/*
 *  ======== Instance Function Declarations ========
 */
Int TransportSrio_bind(Void *handle, UInt32 queueId);
Int TransportSrio_unbind(Void *handle, UInt32 queueId);
Bool TransportSrio_put(Void *handle, Ptr msg);

/*
 *  ======== TransportSrio_Fxns ========
 *  The instance function table.  Functions are not part of external API.
 *  instead they are called under the hood of IPC MessageQ:
 * TransportSrio_bind   --> Called within MessageQ_create()
 * TransportSrio_unbind --> Called within MessageQ_delete()
 * TransportSrio_put    --> Called within MessageQ_put()
 */
INetworkTransport_Fxns TransportSrio_Fxns = {
    TransportSrio_bind,
    TransportSrio_unbind,
    TransportSrio_put
};


/* FUNCTION PURPOSE: Utility to feed a command to RM
 ***********************************************************************
 * DESCRIPTION: Formulates a RM service request and returns the response
 *              data value and resultant state
 */
static int32_t rm_cmd(Rm_ServiceHandle *service_h, const char *name,
                      int32_t *val, Rm_ServiceType type, int retry)
{
    Rm_ServiceReqInfo  rmServiceReq;
    Rm_ServiceRespInfo rmServiceResp;
    int                succeed;

    memset((void *)&rmServiceReq, 0, sizeof(rmServiceReq));
    memset((void *)&rmServiceResp, 0, sizeof(rmServiceResp));
    
    rmServiceReq.type           = type;
    rmServiceReq.resourceName   = name;
    rmServiceReq.resourceNsName = name;
    rmServiceReq.resourceLength = 1;
    if (val) {
        rmServiceReq.resourceBase = *val;
    }

    /* RM will block until resource is returned since callback is NULL */
    rmServiceReq.callback.serviceCallback = NULL;
    do {
        service_h->Rm_serviceHandler(service_h->rmHandle, &rmServiceReq,
                                     &rmServiceResp);
        succeed = (rmServiceResp.serviceState == RM_SERVICE_APPROVED) ||
                  (rmServiceResp.serviceState == RM_SERVICE_APPROVED_STATIC);
        if (retry && (!succeed)) {
            sleep(1);
        }
    } while (retry && (!succeed));

    if (succeed) {
        if ((type == Rm_service_RESOURCE_GET_BY_NAME) && (val)) {
            *val = rmServiceResp.resourceBase;
        }
    } else {
        if (rmServiceResp.serviceState < RM_ERROR_BASE) {
            fprintf(stderr, "RM transaction failed %d %s %d (%d)\n", type, name,
                    val ? *val : 0, rmServiceResp.serviceState);
        } else {
            fprintf(stderr, "RM transaction denied %d %s %d (%d)\n", type, name,
                    val ? *val : 0, rmServiceResp.serviceState);
        }
    }

    return(rmServiceResp.serviceState);
}

/* lookup function not currently used.  #if zero out to avoid compiler
 * warning and preserve for any potential uses in future */
#if 0
/* FUNCTION PURPOSE: Utility to lookup a name in RM NameServer
 ***********************************************************************
 * DESCRIPTION: Sends a name lookup request to RM NameServer.  Value
 *              associated with the name is returned through the val pointer
 *              if the name exists.  The service request state (pass, deny, or
 *              fail) is returned.
 */
static int32_t rm_name_lookup(Rm_ServiceHandle *service_h, const char *name,
                               int32_t *val)
{
    *val = RM_RESOURCE_BASE_UNSPECIFIED;
    
    return(rm_cmd(service_h, name, val, Rm_service_RESOURCE_GET_BY_NAME, 1));
}
#endif

/* FUNCTION PURPOSE: Utility to set a name in RM NameServer
 ***********************************************************************
 * DESCRIPTION: Sends a name set request to RM NameServer.  Value to be
 *              associated with the name is passed through the val pointer.
 *              The service request state (pass, deny, or fail) is returned.
 */
static int32_t rm_name_set(Rm_ServiceHandle *service_h, const char *name,
                           int32_t *val)
{
    return(rm_cmd(service_h, name, val, Rm_service_RESOURCE_MAP_TO_NAME, 0));
}

/* FUNCTION PURPOSE: Utility to delete a name from the RM NameServer
 ***********************************************************************
 * DESCRIPTION: Sends a name delete request to RM NameServer.  The service
 *              request state (pass, deny, or fail) is returned.
 */
static int32_t rm_name_del(Rm_ServiceHandle *service_h, const char *name)
{
    return(rm_cmd(service_h, name, NULL, Rm_service_RESOURCE_UNMAP_NAME, 0));
}

/* FUNCTION PURPOSE: Utility to lookup MessageQ queue ID in the queue cache
 ***********************************************************************
 * DESCRIPTION: Looks up a MessageQ queue ID within the queue cache.  Assumes 
 *              the queue ID list is a sorted list.
 *              UNCACHED_QUEUE_ID will be returned for a failed lookup.
 */
static int32_t qcache_lookup(qid_cache_t *cache, uint32_t queue_id)
{
    uint16_t min = 0;
    uint16_t max = cache->used;
    uint16_t mid;

    while (max >= min) {
        mid = min + ((max - min) / 2);
        if (cache->qid_list[mid] == queue_id) {
            return ((int32_t) cache->qid_list[mid]);
        } else if (cache->qid_list[mid] < queue_id) {
            min = mid + 1;
        } else {
            max = mid - 1;
        }
    }
    return(UNCACHED_QUEUE_ID);
}

/* FUNCTION PURPOSE: Utility to add a MessageQ queue ID to the queue cache
 ***********************************************************************
 * DESCRIPTION: Adds a MessageQ queue ID to the queue cache sorted list.
 *              Total nodes will be expanded by a power of 2 if 
 *              used_nodes = total_nodes prior to adding the new entry
 */
static void qcache_add(qid_cache_t *cache, uint32_t queue_id)
{
    uint32_t *tmp_list;
    uint32_t  i;
    
    if (cache->total == cache->used) {
        /* Need to expand cache by power of 2 */
        tmp_list = cache->qid_list;
        cache->total *= 2;
        cache->qid_list = calloc(cache->total, sizeof(*(cache->qid_list)));
        for (i = 0; i < cache->used; i++) {
            cache->qid_list[i] = tmp_list[i];
        }
        free(tmp_list);
    }

    for (i = cache->used; (i > 0) && (queue_id < cache->qid_list[i - 1]); i--) {
        cache->qid_list[i] = cache->qid_list[i - 1];
    }
    cache->qid_list[i] = queue_id;
    cache->used++;
}

/* FUNCTION PURPOSE: Utility to remove a MessageQ queue ID from the queue cache
 ***********************************************************************
 * DESCRIPTION: Deletes a MessageQ queue ID from the queue cache sorted list.
 *              Total nodes will be contracted by a power of 2 if
 *              used_nodes = (total_nodes/2) post entry removal
 */
static void qcache_del(qid_cache_t *cache, uint32_t queue_id)
{
    uint32_t *tmp_list;
    uint32_t  i;

    for (i = 0; i < (cache->used - 1); i++) {
        if (cache->qid_list[i] >= queue_id)
            cache->qid_list[i] = cache->qid_list[i+1];
    }
    cache->qid_list[i] = 0;
    cache->used--;

    if (cache->used == (cache->total / 2)) {
        /* Need to shrink list by power of 2 */
        tmp_list = cache->qid_list;
        cache->total /= 2;
        cache->qid_list = calloc(cache->total, sizeof(*(cache->qid_list)));
        for (i = 0; i < cache->used; i++) {
            cache->qid_list[i] = tmp_list[i];
        }
        free(tmp_list);
    }
}

/* FUNCTION PURPOSE: Utility to create a thread
 ***********************************************************************
 * DESCRIPTION: Creates a thread that runs thread_routine with input
 *              arguments *args.
 */
static int thread_create(void *(thread_routine)(void *), void *args,
                         void *handle)
{
    int                max_priority, err;
    pthread_t          thread;
    pthread_attr_t     attr;
    struct sched_param param;

    max_priority = sched_get_priority_max(SCHED_FIFO);
    err = pthread_attr_init(&attr);
    if (err) {
        fprintf(stderr, "pthread_attr_init failed: (%s)\n", strerror(err));
        return(err);
    }
    err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    if (err) {
        fprintf(stderr, "pthread_attr_setdetachstate failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setstacksize(&attr, RX_THREAD_DEFAULT_STACK_SIZE);
    if (err) {
        fprintf(stderr, "pthread_attr_setstacksize failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (err) {
        fprintf(stderr, "pthread_attr_setinheritsched failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (err) {
        fprintf(stderr, "pthread_attr_setschedpolicy failed: (%s)\n",
                strerror(err));
        return(err);
    }
    memset(&param, 0, sizeof(param));
    param.sched_priority = max_priority;
    err = pthread_attr_setschedparam(&attr, &param);
    if (err) {
        fprintf(stderr, "pthread_attr_setschedparam failed: (%s)\n",
                strerror(err));
        return(err);
    }
    if (err) return err;
    err = pthread_create(&thread, &attr, thread_routine, args);
    if (err) {
        fprintf(stderr, "pthread_create failed: (%s)\n", strerror(err));
        return(err);
    }
    if (err)
        return(err);
    
    *(pthread_t *)handle = thread;
    return(0);
}

/* FUNCTION PURPOSE: Deletes an executing thread
 ***********************************************************************
 * DESCRIPTION: Deletes the thread pointed to by handle by cancelling it.
 *              The thread will be cancelled at the next cancellation point.
 *              This function will block on pthread_join until the thread
 *              completes the cancellation request
 */
static void *thread_delete(void *handle)
{
    void *ret_val = NULL;
    
    pthread_cancel((pthread_t) handle);
    /* Wait for thread to cancel */
    pthread_join((pthread_t) handle, &ret_val);
    if (ret_val != PTHREAD_CANCELED) {
        fprintf(stderr, "pthread_join did not return PTHREAD_CANCELED when"
                        "after cancelling thread\n");
    }
    return(ret_val);
}

/* FUNCTION PURPOSE: Cleans up objects created for TransportSrio object
 ***********************************************************************
 * DESCRIPTION: Cleans up any objects or allocated data structures associated
 *              with the TransportSrio object.
 */
static void transportSrioCleanup(TransportSrio_Object *obj)
{
    int i;

    if (obj->qid_cache.qid_list)
        free(obj->qid_cache.qid_list);

    if (obj->mpm_transport_handle)
        mpm_transport_close(obj->mpm_transport_handle);

    for (i = 0; i < obj->sockets.num_eps; i++) {
        if (obj->frag_srcs[i].rcv_buf) {
            MessageQ_free((MessageQ_Msg)obj->frag_srcs[i].rcv_buf);
        }
    }

    if (obj->frag_srcs) {
        free(obj->frag_srcs);
    }

    if (obj->sockets.sock_type == sock_TYPE_11) {
        free(obj->sockets.u.t11_eps);
        /* Delete T11 instance lock from RM */
        rm_name_del(obj->rm_service_h, TRANSSRIO_T11_INST_LOCK);
    } else if (obj->sockets.sock_type == sock_TYPE_9) {
        free(obj->sockets.u.t9_eps);
        /* Delete T9 instance lock from RM */
        rm_name_del(obj->rm_service_h, TRANSSRIO_T9_INST_LOCK);
    }

    if (obj->allocd_rcv_msg)
        MessageQ_free(obj->allocd_rcv_msg);
}

/* FUNCTION PURPOSE: Reassembles fragmented packets received over SRIO
 ***********************************************************************
 * DESCRIPTION: Routine used to reassemble full message from fragments
 *              received over the SRIO interface.
 */
static MessageQ_Msg transportSrioFragmentRcv(TransportSrio_Object *obj,
                                             MessageQ_Msg frag_msg,
                                             mpm_transport_packet_addr_t *src_addr)
{
    uint32_t const  frag_data_size = obj->max_mtu -
                                     sizeof(MessageQ_MsgHeader) -
                                     sizeof(frag_hdr_t);
    uint8_t        *frag_buf = (uint8_t *)frag_msg;
    frag_hdr_t     *frag_hdr;
    uint8_t        *data;
    uint8_t         max_seqn = 0;
    uint8_t         seqn = 0;
    uint8_t         msg_tag;
    frag_ep_t      *frag_srcs = (frag_ep_t *)obj->frag_srcs;
    int             i;
    frag_ep_t      *src_ep = NULL;
    uint8_t        *rcv_buf_offset;
    MessageQ_Msg    rcv_msg = NULL;

    /* Set fragment header which is placed after the MessageQ header */
    frag_hdr = (frag_hdr_t *)&frag_buf[sizeof(MessageQ_MsgHeader)];
    data     = (uint8_t *)&frag_buf[sizeof(MessageQ_MsgHeader) +
                                    sizeof(frag_hdr_t)];
    max_seqn = frag_hdr->max_seqn;
    seqn     = frag_hdr->seqn;
    msg_tag  = frag_hdr->tag;

    for (i = 0; i < obj->sockets.num_eps; i++) {
        TransportSrio_Type11EpParams *t11_ep = obj->sockets.u.t11_eps;
        TransportSrio_Type9EpParams  *t9_ep = obj->sockets.u.t9_eps;

        switch (obj->sockets.sock_type) {
            case sock_TYPE_11:
                if ((src_addr->addr.srio.type11.id == t11_ep[i].device_id) &&
                    (src_addr->addr.srio.type11.letter == t11_ep[i].letter) &&
                    (src_addr->addr.srio.type11.mailbox == t11_ep[i].mailbox)) {
                    src_ep = &frag_srcs[i];
                }
                break;
            case sock_TYPE_9:
                if ((src_addr->addr.srio.type9.id == t9_ep[i].device_id) &&
                    (src_addr->addr.srio.type9.cos == t9_ep[i].cos) &&
                    (src_addr->addr.srio.type9.streamId ==
                     t9_ep[i].stream_id)) {
                    src_ep = &frag_srcs[i];
                }
                break;
            default:
                fprintf(stderr, "transportSrioFragmentRcv : "
                        "(MsgTag: %d) Received fragment with an invalid "
                        "socket type\n", msg_tag);
                goto error_exit;
        }

        if (src_ep) {
            break;
        }
    }

    if (src_ep == NULL) {
        if (obj->sockets.sock_type == sock_TYPE_11) {
            fprintf(stderr, "transportSrioFragmentRcv T11 : "
                    "(MsgTag: %d) Fragment source not recognized for "
                    "ID: 0x%x, letter: %d, mailbox: %d\n", msg_tag,
                    src_addr->addr.srio.type11.id,
                    src_addr->addr.srio.type11.letter,
                    src_addr->addr.srio.type11.mailbox);
        } else {
            fprintf(stderr, "transportSrioFragmentRcv T9 : "
                    "(MsgTag: %d) Fragment source not recognized for "
                    "ID: 0x%x, cos: %d, streamID: %d\n", msg_tag,
                    src_addr->addr.srio.type9.id,
                    src_addr->addr.srio.type9.cos,
                    src_addr->addr.srio.type9.streamId);
        }
    } else {
        if (seqn == 0) {
            /* Start of new fragment stream.  Get length of entire stream from
             * MessageQ header in first fragment. */
            UInt32 total_len = MessageQ_getMsgSize(data);

            if (src_ep->rcv_buf) {
                /* Previous fragment stream hasn't completed.  Drop reassembly
                 * buffer */
                fprintf(stderr, "transportSrioFragmentRcv : "
                        "Received start of new stream with tag %d before "
                        "stream with tag %d completed.  Dropping old "
                        "fragments\n", msg_tag, src_ep->tag);
                MessageQ_free((MessageQ_Msg)src_ep->rcv_buf);
            }

            src_ep->prev_seqn = -1;
            src_ep->tag       = msg_tag;
            src_ep->buf_len   = total_len;
            src_ep->rcv_buf   = (uint8_t *)MessageQ_alloc(0, total_len);
            if (src_ep->rcv_buf == NULL) {
                fprintf(stderr, "transportSrioFragmentRcv : "
                        "(MsgTag: %d) Could not allocate receive buffer "
                        "of size %d bytes\n", msg_tag, total_len);
                goto error_exit;
            }
        }

        if (((src_ep->prev_seqn + 1) != seqn) && (src_ep->tag == msg_tag)) {
            /* Out of order packet in current tag stream.  Drop current and
             * future fragments until seqn zero is received */
            MessageQ_free((MessageQ_Msg)src_ep->rcv_buf);
            src_ep->rcv_buf = NULL;
            fprintf(stderr, "transportSrioFragmentRcv : "
                    "(MsgTag: %d) Out of order fragment.  Dropping "
                    "entire stream\n", msg_tag);

        /* Don't reset entire stream if current stream's tag is different from
         * tag in received fragment (zero will reset) */
        } else if (src_ep->tag == msg_tag) {
            uint32_t copy_size = frag_data_size;

            rcv_buf_offset = src_ep->rcv_buf + (seqn * frag_data_size);
            if (seqn == max_seqn) {
                /* Don't copy padding from last fragment */
                copy_size = src_ep->buf_len - (frag_data_size * seqn);
                /* All fragments assembled after following copy */
                rcv_msg = (MessageQ_Msg)src_ep->rcv_buf;
                src_ep->rcv_buf = NULL;
            }
            memcpy(rcv_buf_offset, data, copy_size);

            if (seqn == 0) {
                MessageQ_Msg hdr = (MessageQ_Msg)src_ep->rcv_buf;

                hdr->heapId  = 0;
            }
            src_ep->prev_seqn = seqn;
        } else {
            fprintf(stderr, "transportSrioFragmentRcv : "
                    "Received inconsistent tag.  Assembling msgTag %d "
                    "but received msgTag %d.  Dropping fragment\n",
                    src_ep->tag, msg_tag);
        }
    }

error_exit:
    /* Recycle fragment */
    MessageQ_free(frag_msg);
    return(rcv_msg);
}

/* FUNCTION PURPOSE: MessageQ message receive thread
 ***********************************************************************
 * DESCRIPTION: Receives incoming MessageQ messages via MPM-Transport
 *              Calls to mpm_transport_packet_recv will block until data
 *              is received.
 *
 *              Input args pointer is the TransportSrio_Object pointer passed 
 *              through the pthread_create function when creating the receive 
 *              thread.
 *
 *              The MessageQ queue ID cache will be queried with the receive
 *              message's destination queue ID information stored within the
 *              MessageQ header.  The message will be rerouted via the
 *              reroute_tid if the queue ID does not exist within the cache
 *              (i.e. the destination queue ID does not exist within the same
 *              process as the TransportSrio object
 *
 *              Thread can be sent a cancellation request at any time but will
 *              not officially cancel until a cancellation point is reached.
 *              The alloc_rcv_msg object variable tracks the currently
 *              allocated receive MessageQ message.  The main process cancelling
 *              thread will free allocd_rcv_msg if a cancellation occurs before
 *              the allocd_rcv_msg is passed to the application or freed
 */
static void *transportSrioRcv(void *args)
{
    TransportSrio_Object *obj = (TransportSrio_Object *) args;

    while (1) {
        MessageQ_Msg                rx_msg;
        int                         rx_msg_bytes = obj->max_mtu;
        int                         status;
        mpm_transport_recv_t        rx_cfg;
        mpm_transport_packet_addr_t src_addr;
        int                         drop_msg = 0;

        /* Allocate local MessageQ message for eventual memcpy of received data.
         * Needs to be done at the moment since MessageQ heap has no way to
         * register an application-specific heap */
        rx_msg = MessageQ_alloc(0, obj->max_mtu);
        /* Store pointer to allocated msg in obj in case thread is cancelled
         * prior to full reception */
        obj->allocd_rcv_msg = rx_msg;

        memset(&rx_cfg, 0, sizeof(rx_cfg));
        /* Tell MPM to block until packet received */
        rx_cfg.wait_opt = packet_recv_BLOCKING_INTERRUPT;

        memset(&src_addr, 0, sizeof(src_addr));
        if (obj->sockets.sock_type == sock_TYPE_11) {
            src_addr.addr_type = packet_addr_type_SRIO_TYPE11;
        } else if (obj->sockets.sock_type == sock_TYPE_9) {
            src_addr.addr_type = packet_addr_type_SRIO_TYPE9;
        }

        /* MPM-Transport will memcpy recieved data into rx_msg */
        status = mpm_transport_packet_recv(obj->mpm_transport_handle,
                                           (char **) &rx_msg, &rx_msg_bytes,
                                           &src_addr, &rx_cfg);
        if (status < 0) {
            fprintf(stderr, "transportSrioRcv : "
                            "mpm_transport_packet_recv err: %d\n", status);
        } else {
            if (((int)MessageQ_getMsgSize(rx_msg)) == obj->max_mtu) {
                /* Fragmented packet */
                rx_msg = transportSrioFragmentRcv(obj, rx_msg, &src_addr);
            } else {
                /* Reset MessageQ msgSize to what was allocated above for
                 * standard packet */
                rx_msg->msgSize = obj->max_mtu;
            }

            if (rx_msg) {
                /* Reset heapId and msgSize */
                rx_msg->heapId = 0;

                if (MultiProc_self() != rx_msg->dstProc) {
                    fprintf(stderr, "transportSrioRcv: Destination processor %d\n"
                                    "                  Local processor       %d\n"
                                    "DROPPING MESSAGE\n",
                                    rx_msg->dstProc, MultiProc_self());
                    drop_msg = 1;
                    goto drop;
                }

                /* Check if message destined for local MessageQ */
                if (qcache_lookup(&(obj->qid_cache), 
                                  MessageQ_getDstQueue(rx_msg)) == 
                    UNCACHED_QUEUE_ID) {
                    if (obj->reroute_tid) {
                        /* Route to process who opened the queue via 
                         * TransportLinuxQmss */
                        MessageQ_setTransportId(rx_msg, obj->reroute_tid);
                    } else {
                        fprintf(stderr, "transportSrioRcv : "
                                        "Message destined for MessageQ on another "
                                        "reroute Network Transport ID is NULL\n"
                                        "DROPPING MESSAGE\n");
                        drop_msg = 1;
                        goto drop;
                    }
                }

drop:
                if (drop_msg)
                    MessageQ_free(rx_msg);
                else
                    MessageQ_put(MessageQ_getDstQueue(rx_msg), rx_msg);
            }
        }
        /* Clear since msg was either freed or given to MessageQ_put */
        obj->allocd_rcv_msg = NULL;
    }

    return(NULL);
}

/* FUNCTION PURPOSE: Fragments and sends large MessageQ messages
 ***********************************************************************
 * DESCRIPTION: Routine used to fragment and send a MessageQ message that is
 *              greater than the configured maximum MTU
 */
static Bool transportSrioFragmentSend(TransportSrio_Object *obj, Ptr msg,
                                      mpm_transport_packet_addr_t *send_addr,
                                      mpm_transport_send_t *send_cfg)
{
    UInt32   const  msg_size = MessageQ_getMsgSize(msg);
    uint8_t         max_seqn = 0;
    uint32_t const  frag_data_size = obj->max_mtu -
                                     sizeof(MessageQ_MsgHeader) -
                                     sizeof(frag_hdr_t);
    uint8_t        *msg_q_msg = (uint8_t *)msg;
    uint8_t         seqn = 0;
    uint32_t        msg_offset = 0;
    Bool            status = TRUE;

    /* Calculate number of fragments needed (base zero) */
    max_seqn = ((msg_size + frag_data_size - 1) / frag_data_size) - 1;
    /* Update fragmented message tag.  Overflow back to zero is okay */
    obj->frag_msg_tag++;

    while(msg_offset < msg_size) {
        uint8_t    *send_buf = NULL;
        int         buf_len = obj->max_mtu;
        uint32_t    frag_bytes;
        frag_hdr_t *frag_hdr;
        int         mpm_status;

        send_buf = (uint8_t *)MessageQ_alloc(0, buf_len);

        /* Calculate number of bytes that need to be copied from msg to
         * this fragment */
        frag_bytes = (((seqn + 1) * frag_data_size) > msg_size) ?
                    msg_size - (seqn * frag_data_size) : frag_data_size;

        /* Set fragment header which is placed after the MessageQ header */
        frag_hdr = (frag_hdr_t *)&send_buf[sizeof(MessageQ_MsgHeader)];
        frag_hdr->seqn     = seqn++;
        frag_hdr->max_seqn = max_seqn;
        frag_hdr->tag      = obj->frag_msg_tag;
        memcpy((void *)&send_buf[sizeof(MessageQ_MsgHeader) +
                                 sizeof(frag_hdr_t)],
               (void *)&msg_q_msg[msg_offset], frag_bytes);
        msg_offset += frag_bytes;

        if ((mpm_status = mpm_transport_packet_send(obj->mpm_transport_handle,
                                                    (char **)&send_buf,
                                                    &buf_len, send_addr,
                                                    send_cfg))) {
            status = FALSE;
            MessageQ_free((MessageQ_Msg)send_buf);
            fprintf(stderr, "TransportSrio_put : "
                            "MPM-Transport packet send returned error %d "
                            "for fragment %d with stream tag %d\n",
                            mpm_status, seqn, obj->frag_msg_tag);
            goto error_exit;
        }

        MessageQ_free((MessageQ_Msg)send_buf);
    }

error_exit:
    return(status);
}

/* FUNCTION PURPOSE: Initializes the TransportSrio creation parameters
 ***********************************************************************
 * DESCRIPTION: Initializes the TransportSrio creation parameters based on
 *              parameter structure version.
 */
Void TransportSrio_Params_init__S(TransportSrio_Params *params, Int version)
{
    TransportSrio_Params_Version1 *params1;

    if (params == NULL) {
        fprintf(stderr, "TransportSrio_Params_init : "
                        "Params structure is NULL\n");
        assert(FALSE);
        return;
    }

    switch (version) {
        case TransportSrio_Params_VERSION_1:
            params1 = (TransportSrio_Params_Version1 *)params;
            params1->__version = TransportSrio_Params_VERSION_1;
            params1->mpm_trans_inst_name = NULL;
            params1->rm_service_h        = NULL;
            params1->max_mtu             = 0;
            params1->sock_params         = NULL;
            params1->reroute_tid         = 0;
            params1->srio_device_init    = NULL;
            params1->init_cfg            = NULL;
            params1->srio_device_deinit  = NULL;
            params1->deinit_cfg          = NULL;
            params1->mpm_trans_init_qmss = 0;
            break;
        default:
            fprintf(stderr, "TransportSrio_Params_init : "
                    "Invalid Params version\n");
            assert(FALSE);
            break;
    }
}

/* FUNCTION PURPOSE: Creates a TransportSrio object
 ***********************************************************************
 * DESCRIPTION: Creates an instance of TransportSrio.  A MPM-Transport
 *              instance will be opened configured for SRIO.
 *
 *              The transportSrioRcv thread will be created as part of this
 *              routine.  A separate thread is created for each TransportSrio
 *              instance.
 *
 *              Only a single instance of TransportSrio Type 11 and Type 9 can
 *              be created per Linux Host.  This restriction is enforced by
 *              storing an instance lock within the RM NameServer which
 *              serializes accesses across all Linux processes.  This
 *              restriction is in place because SRIO endpoints are addressed
 *              based on the IPC Multiproc ID, which is per core.  MessageQ
 *              messages that are destined for processes not existing within
 *              the same process as the TransportSrio object will be rerouted
 *              via the supplied reroute network transport ID.
 */
TransportSrio_Handle TransportSrio_create(TransportSrio_Params *pp)
{
    TransportSrio_Params_Version1 *pp1;
    TransportSrio_Params           params;
    TransportSrio_Object          *obj = NULL;
    mpm_transport_open_t           cfg;
    UInt16                         proc = MultiProc_self();
    int32_t                        status;
    int32_t                        lock = 1;

    if (pp == NULL) {
        fprintf(stderr, "TransportSrio_create : Params structure cannot be "
                        "NULL\n");
        goto err_exit;
    }

    /* initialize local params structure */
    TransportSrio_Params_init(&params);

    /* initialize local params using caller's given params */
    switch (pp->__version) {
        case TransportSrio_Params_VERSION_1:
            pp1 = (TransportSrio_Params_Version1 *)pp;
            params.mpm_trans_inst_name = pp1->mpm_trans_inst_name;
            params.rm_service_h        = pp1->rm_service_h;
            params.sock_params         = pp1->sock_params;
            params.max_mtu             = pp1->max_mtu;
            params.reroute_tid         = pp1->reroute_tid;
            params.srio_device_init    = pp1->srio_device_init;
            params.init_cfg            = pp1->init_cfg;
            params.srio_device_deinit  = pp1->srio_device_deinit;
            params.deinit_cfg          = pp1->deinit_cfg;
            params.mpm_trans_init_qmss = pp1->mpm_trans_init_qmss;
            break;
        default:
            fprintf(stderr, "TransportSrio_create : Invalid Params version\n");
            goto err_exit;
    }

    /* Error checking */
    if (strlen(params.mpm_trans_inst_name) == 0) {
        fprintf(stderr, "TransportSrio_create : mpm_trans_inst_name "
                        "cannot be NULL\n");
        goto err_exit;
    }
    if ((params.max_mtu == 0) || (params.max_mtu > SRIO_HW_MAX_MTU)) {
        fprintf(stderr, "TransportSrio_create : max_mtu must be greater than"
                        "zero and less than or equal to %d\n", SRIO_HW_MAX_MTU);
        goto err_exit;
    }
    if (params.rm_service_h == NULL) {
        fprintf(stderr, "TransportSrio_create : rm_service_h cannot be NULL\n");
        goto err_exit;
    }
    if (params.sock_params == NULL) {
        fprintf(stderr, "TransportSrio_create : sock_params cannot be NULL\n");
        goto err_exit;
    }

    /* Attempt to register TransportSrio instance with RM NameServer.
     * There can only be one Type 11 and one Type 9 TransportSrio instance
     * opened per Linux host.  TransportSrio_create will exit immediately if
     * a TransportSrio instance for a configured type already exists on the 
     * host */
    if (params.sock_params->sock_type == sock_TYPE_11) {
        status = rm_name_set(params.rm_service_h, TRANSSRIO_T11_INST_LOCK,
                             &lock);
        if (status == RM_SERVICE_DENIED_NAME_EXISTS_IN_NS) {
            fprintf(stderr, "TransportSrio_create : "
                            "Type 11 Instance already exists\n");
            goto err_exit;
        } else if ((status != RM_SERVICE_APPROVED) && 
                   (status != RM_SERVICE_APPROVED_STATIC)) {
            fprintf(stderr, "TransportSrio_create : "
                            "Error checking Type 11 instance lock\n");
            goto err_exit;
        }
    } else if (params.sock_params->sock_type == sock_TYPE_9) {
        status = rm_name_set(params.rm_service_h, TRANSSRIO_T9_INST_LOCK,
                             &lock);
        if (status == RM_SERVICE_DENIED_NAME_EXISTS_IN_NS) {
            fprintf(stderr, "TransportSrio_create : "
                            "Type 9 Instance already exists\n");
            goto err_exit;
        } else if ((status != RM_SERVICE_APPROVED) && 
                   (status != RM_SERVICE_APPROVED_STATIC)) {
            fprintf(stderr, "TransportSrio_create : "
                            "Error checking Type 9 instance lock\n");
            goto err_exit;
        }
    } else {
        fprintf(stderr, "TransportSrio_create : Invalid socket type\n");
        goto err_exit;
    }
    
    /* allocate the instance object */
    obj = calloc(1, sizeof(TransportSrio_Object));

    /* initialize the inherited interface */
    obj->base.base.interfaceType = INetworkTransport_TypeId;
    obj->base.fxns               = &TransportSrio_Fxns;
    obj->rm_service_h            = params.rm_service_h;
    obj->max_mtu                 = params.max_mtu;
    obj->reroute_tid             = params.reroute_tid;
    obj->allocd_rcv_msg          = NULL;
    obj->frag_srcs               = NULL;
    obj->frag_msg_tag            = 0;
    /* Save the socket parameters */
    obj->sockets.sock_type = params.sock_params->sock_type;
    obj->sockets.num_eps   = params.sock_params->num_eps;
    if (obj->sockets.sock_type == sock_TYPE_11) {
        obj->sockets.u.t11_eps = calloc(obj->sockets.num_eps,
                                        sizeof(*(obj->sockets.u.t11_eps)));
        memcpy(obj->sockets.u.t11_eps, params.sock_params->u.t11_eps,
               obj->sockets.num_eps * sizeof(*(obj->sockets.u.t11_eps)));
    } else if (obj->sockets.sock_type == sock_TYPE_9) {
        obj->sockets.u.t9_eps = calloc(obj->sockets.num_eps,
                                       sizeof(*(obj->sockets.u.t9_eps)));
        memcpy(obj->sockets.u.t9_eps, params.sock_params->u.t9_eps,
               obj->sockets.num_eps * sizeof(*(obj->sockets.u.t9_eps)));
    }

    /* Allocate and initialize fragmentation source endpoint tracking array */
    obj->frag_srcs = calloc(obj->sockets.num_eps, sizeof(frag_ep_t));
    if (obj->frag_srcs == NULL) {
        fprintf(stderr, "TransportSrio_create : Failed to allocate "
                        "fragmentation source array\n");
        goto err_exit;
    }

    /* Open MPM transport instance for SRIO */
    memset(&cfg, 0, sizeof(cfg));
    cfg.open_mode                     = (O_SYNC|O_RDWR);
    cfg.rm_info.rm_client_handle      = obj->rm_service_h;
    if (params.mpm_trans_init_qmss) {
        cfg.transport_info.qmss.qmss_init = 1;
    }

    /* Configure the SRIO socket based on the local MultiProc ID */
    switch (obj->sockets.sock_type) {
        case sock_TYPE_11:
            cfg.transport_info.srio.type           = packet_addr_type_SRIO_TYPE11;
            cfg.transport_info.srio.type11.tt      = obj->sockets.u.t11_eps[proc].tt;
            cfg.transport_info.srio.type11.id      = obj->sockets.u.t11_eps[proc].device_id;
            cfg.transport_info.srio.type11.letter  = obj->sockets.u.t11_eps[proc].letter;
            cfg.transport_info.srio.type11.mailbox = obj->sockets.u.t11_eps[proc].mailbox;
            cfg.transport_info.srio.type11.segmap  = obj->sockets.u.t11_eps[proc].seg_map;
            break;
        case sock_TYPE_9:
            cfg.transport_info.srio.type           = packet_addr_type_SRIO_TYPE9;
            cfg.transport_info.srio.type9.tt       = obj->sockets.u.t9_eps[proc].tt;
            cfg.transport_info.srio.type9.id       = obj->sockets.u.t9_eps[proc].device_id;
            cfg.transport_info.srio.type9.cos      = obj->sockets.u.t9_eps[proc].cos;
            cfg.transport_info.srio.type9.streamId = obj->sockets.u.t9_eps[proc].stream_id;
            break;
        default:
            fprintf(stderr, "TransportSrio_create : Invalid socket type\n");
            goto err_exit;
    }

    cfg.transport_info.srio.deviceInit   = params.srio_device_init;
    cfg.transport_info.srio.initCfg      = params.init_cfg;
    cfg.transport_info.srio.deviceDeinit = params.srio_device_deinit;
    cfg.transport_info.srio.deinitCfg    = params.deinit_cfg;

    obj->mpm_transport_handle = mpm_transport_open(params.mpm_trans_inst_name,
                                                   &cfg);
    if (obj->mpm_transport_handle == NULL) {
        fprintf(stderr, "TransportSrio_create : mpm_transport_open failed\n");
        goto err_exit;
    }

    /* Create the MessageQ queue ID cache */
    obj->qid_cache.total = QUEUEID_CACHE_INIT_SIZE;
    obj->qid_cache.used = 0;
    obj->qid_cache.qid_list = calloc(QUEUEID_CACHE_INIT_SIZE,
                                     sizeof(*(obj->qid_cache.qid_list)));

    /* Create receive thread that will monitor the FD tied to the transport's
     * SRIO receive queue interrupt */
    if (thread_create(transportSrioRcv, (void *)obj,
                      &(obj->rx_thread_h))) {
        /* Error creating receive thread */
        fprintf(stderr, "TransportSrio_create : "
                        "Failed to create receive thread\n");
        goto err_exit;
    }

    return((TransportSrio_Handle)obj);

err_exit:
    /* Cleanup */
    if (obj) {
        transportSrioCleanup(obj);
        free(obj);
        obj = NULL;
    }
    return((TransportSrio_Handle)obj);
}

/* FUNCTION PURPOSE: Deletes a TransportSrio object
 ***********************************************************************
 * DESCRIPTION: Deletes the specified TransportSrio object.  The receive thread
 *              associated to the object will be cancelled and the object will
 *              be set to NULL upon return.
 */
Void TransportSrio_delete(TransportSrio_Handle *hp)
{
    TransportSrio_Object *obj;

    obj = *(TransportSrio_Object **)hp;

    /* Cancel receive thread */
    thread_delete((void *)obj->rx_thread_h);
    /* Cleanup transport specific parameters */
    transportSrioCleanup(obj);
    free(obj);
    *hp = NULL;
}

/* FUNCTION PURPOSE: TransportSrio_Handle upcast to INetworkTransport_Handle
 ***********************************************************************
 * DESCRIPTION: Upcasts a TransportSrio_Handle to its inherited
 *              INetworkTransport_Handle
 */
INetworkTransport_Handle TransportSrio_upCast(TransportSrio_Handle inst)
{
    return((INetworkTransport_Handle)inst);
}

/* FUNCTION PURPOSE: INetworkTransport_Handle downcast to TransportSrio_Handle
 ***********************************************************************
 * DESCRIPTION: downcasts an INetworkTransport_Handle to the 
 *              TransportSrio_Handle that inheritied it
 */
TransportSrio_Handle TransportSrio_downCast(INetworkTransport_Handle base)
{
    return((TransportSrio_Handle)base);
}

/* FUNCTION PURPOSE: Binds a created MessageQ queue ID
 ***********************************************************************
 * DESCRIPTION: Registers a new MessageQ queue ID, created within the same
 *              context as the TransportSrio object, with the transport's
 *              queue cache.
 *
 *              The queue cache will be used within the transportSrioRcv thread
 *              to discover if the received message was destined for the process
 *              containing the TransportSrio object or another process.  The
 *              message will be rerouted if it was destined for a MessageQ 
 *              queue not created within the same process as the TransportSrio
 *              object.
 */
Int TransportSrio_bind(void *handle, UInt32 queueId)
{
    TransportSrio_Object *obj = (TransportSrio_Object *)handle;
    Int                   status = 0;

    /* Cache the local queue IDs for received messages */
    qcache_add(&(obj->qid_cache), queueId);
    return(status);
}

/* FUNCTION PURPOSE: Unbinds a deleted MessageQ queue ID
 ***********************************************************************
 * DESCRIPTION: Unbinds a MessageQ queue ID, deleted within the same context
 *              as the TransportSrio object, by removing the associated
 *              MessageQ queue ID from the queue cache.
 */
Int TransportSrio_unbind(void *handle, UInt32 queueId)
{
    TransportSrio_Object *obj = (TransportSrio_Object *)handle;
    Int                   status = 0;

    if (qcache_lookup(&(obj->qid_cache), queueId) != UNCACHED_QUEUE_ID)
        qcache_del(&(obj->qid_cache), queueId);
    return(status);
}

/* FUNCTION PURPOSE: Sends a MessageQ message over TransportSrio
 ***********************************************************************
 * DESCRIPTION: Sends a MessageQ message by referencing the destination
 *              MessageQ queue ID in the message against the socket parameter
 *              endpoint address list to get the Type 11 or Type 9 address
 *              parameters.  Those parameters and the message are then provided
 *              to MPM-Transport for the send operation.
 */
Bool TransportSrio_put(void *handle, Ptr msg)
{
    TransportSrio_Object        *obj = (TransportSrio_Object *)handle;
    Bool                         status = TRUE;
    mpm_transport_packet_addr_t  send_addr;
    MessageQ_Msg                 msg_q_msg = (MessageQ_Msg) msg;
    uint16_t                     dst_proc = msg_q_msg->dstProc;
    int                          msg_size = MessageQ_getMsgSize(msg_q_msg);
    char                        *send_msg;
    mpm_transport_send_t         send_cfg;
    int                          mpm_status;

    memset(&send_addr, 0, sizeof(send_addr));

    /* Find destination SRIO endpoint address based on remote MultiProc ID */
    switch (obj->sockets.sock_type) {
        case sock_TYPE_11:
            send_addr.addr_type = packet_addr_type_SRIO_TYPE11;
            send_addr.addr.srio.type11.tt      = obj->sockets.u.t11_eps[dst_proc].tt;
            send_addr.addr.srio.type11.id      = obj->sockets.u.t11_eps[dst_proc].device_id;
            send_addr.addr.srio.type11.letter  = obj->sockets.u.t11_eps[dst_proc].letter;
            send_addr.addr.srio.type11.mailbox = obj->sockets.u.t11_eps[dst_proc].mailbox;
            break;
        case sock_TYPE_9:
            send_addr.addr_type = packet_addr_type_SRIO_TYPE9;
            send_addr.addr.srio.type9.tt       = obj->sockets.u.t9_eps[dst_proc].tt;
            send_addr.addr.srio.type9.id       = obj->sockets.u.t9_eps[dst_proc].device_id;
            send_addr.addr.srio.type9.cos      = obj->sockets.u.t9_eps[dst_proc].cos;
            send_addr.addr.srio.type9.streamId = obj->sockets.u.t9_eps[dst_proc].stream_id;
            break;
        default:
            status = FALSE;
            fprintf(stderr, "TransportSrio_put : Invalid socket type\n");
            goto err_exit;
    }

    memset(&send_cfg, 0, sizeof(send_cfg));
    /* MPM-Transport configured to memcpy buffer since MessageQ doesn't provide
     * means for registering an application-specific heap */
    send_cfg.buf_opt = packet_send_MEMCPY_BUFFER;

    if (msg_size >= obj->max_mtu) {
        /* Fragment packet.  Fragments will always be SRIO max MTU in size.
         * Last fragment will be padded.  This is to distinguish fragmented
         * packets from non-fragmented. */
        status = transportSrioFragmentSend(obj, msg, &send_addr, &send_cfg);
    } else {
        send_msg = (char *)msg_q_msg;
        if ((mpm_status = mpm_transport_packet_send(obj->mpm_transport_handle,
                                                    (char **)&send_msg,
                                                    &msg_size, &send_addr,
                                                    &send_cfg))) {
            status = FALSE;
            fprintf(stderr, "TransportSrio_put : "
                            "MPM-Transport packet send returned error %d\n",
                            mpm_status);
        }
    }

err_exit:
    MessageQ_free(msg_q_msg);
    return(status);
}

/* FUNCTION PURPOSE: Returns Linux IPC Transport version information
 ***********************************************************************
 */
UInt32 TransportSrio_getVersion(Void)
{
    return TRANSPORT_VERSION_ID;
}

/* FUNCTION PURPOSE: Returns Linux IPC Transport version string
 ***********************************************************************
 */
const Char *TransportSrio_getVersionStr(Void)
{
    return transportSrioVersionStr;
}

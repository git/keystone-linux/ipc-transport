/**
 *   @file  TransportSrio.h
 *
 *   @brief
 *      ARMv7 Linux MessageQ TransportSrio public API
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
 */

/*
 *  ======== TransportSrio.h ========
 */

#ifndef TRANSPORTSRIO_H_
#define TRANSPORTSRIO_H_
#include <ti/ipc/interfaces/INetworkTransport.h>

/**  @mainpage ARMv7 Linux TransportSrio
 *
 *   @section intro  Introduction
 *
 *   TransportSrio is a SRIO-based transport for ARMv7 Linux IPC MessageQ.
 *   TransportSrio can be registered with MessageQ as an INetworkTransport.
 *   Once registered, TransportSrio allows MessageQ-based communication between
 *   Linux Host processors and DSP processors running an instance of the
 *   C66 MessageQ-based, TransportSrio.
 *
 *   Only one instance of each SRIO Type (9/11) can be opened at a time on a
 *   Linux host.  MessageQ messages received by the TransportSrio instance
 *   will be reouted to the Linux process that owns the destination MessageQ
 *   queue via the network transport specified through the reroute_tid
 *   initialization parameter.  Currently, in all cases, the reroute_tid value
 *   will correspond to the networkd transport ID for the TransportQmss 
 *   transport registered with MessageQ.
 */

/*
 *  ======== TransportSrio_Params_VERSION_1 ========
 *  TransportSrio ver 1.0.0.0
 *
 *  Initial implementation of the params structure.
 */
#define TransportSrio_Params_VERSION_1 1

/*
 *  ======== TransportSrio_Params_VERSION ========
 *  Defines the current params structure version. Internal use only.
 */
#define TransportSrio_Params_VERSION TransportSrio_Params_VERSION_1

/*
 *  ======== TransportSrio_Handle ========
 **
 *  @brief Opaque handle to TransportSrio instance object.
 */
typedef struct TransportSrio_Object *TransportSrio_Handle;

/*
 *  ======== TransportSrio_SockType ========
 **
 *  @brief SRIO socket types
 */
typedef enum {
    /** Initialization socket value - Invalid */
    sock_TYPE_INVALID = 0,
    /** SRIO Type 9 socket type */
    sock_TYPE_9,
    /** SRIO Type 11 socket type */
    sock_TYPE_11
} TransportSrio_SockType;

/*
 *  ======== TransportSrio_Type11EpParams ========
 **
 *  @brief SRIO Type 11 endpoint address parameters
 */
typedef struct {
    /** Defines the number of bits in the device ID */
    UInt16 tt;
    /** Endpoint device ID */
    UInt16 device_id;
    /** Endpiont letter */
    UInt16 letter;
    /** Endpiont mailbox */
    UInt16 mailbox;
    /** Endpiont segmentation map.  Value depends on the number of data bytes
     *  that will be sent over the SRIO interface
     *  0 - data bytes to send <= 256 bytes
     *  1 - data bytes to send > 256 bytes */
    UInt16 seg_map;
} TransportSrio_Type11EpParams;

/*
 *  ======== TransportSrio_Type9EpParams ========
 **
 *  @brief SRIO Type 9 endpoint address parameters
 */
typedef struct {
    /** Defines the number of bits in the device ID */
    UInt16 tt;
    /** Endpoint device ID */
    UInt16 device_id;
    /** Endpoint class of service */
    UInt8  cos;
    /** Endpoint steam identifier */
    UInt16 stream_id;
} TransportSrio_Type9EpParams;

/*
 *  ======== TransportSrio_SocketParams ========
 **
 *  @brief Socket parameters to be provided to the TransportSrio for 
 *         configuration and proper routing of send packets
 */
typedef struct {
    /** SRIO socket type to configure the TransportSrio instance as */
    TransportSrio_SockType            sock_type;
    /** Totale number of system endpoints that have corresponding SRIO address
     *  information.  The number of system endpoints must be
     *  equivalent to total number of processors in the system tracked by
     *  the IPC MultiProc module.  SRIO address information will be indexed by
     *  MultiProc ID of the local processor and destination processor */
    UInt16                            num_eps;
    /** SRIO Type specific endpoint address information */
    union {
        /** Pointer to a list of Type 11 endpoint addresses.  The list must be
         *  of size num_eps */
        TransportSrio_Type11EpParams *t11_eps;
        /** Pointer to a list of Type 9 endpoint addresses.  The list must be
         *  of size num_eps */
        TransportSrio_Type9EpParams  *t9_eps;
    } u;
} TransportSrio_SocketParams;

/*
 *  ======== TransportSrio_Params ========
 **
 *  @brief TransportSrio initialization parameters
 */
typedef struct {
    /* Tracks the implementation evolution.  Internal use only. */
    Int                          __version;
    /** MPM-Transport instance name.  This string must match a "slaves" name
     *  string defined within the mpm_config.json file used in the Linux
     *  filesystem */
    Char                        *mpm_trans_inst_name;
    /** RM instance service handle needed by MPM-Transport to request hardware
     *  resources */
    Void                        *rm_service_h;
    /** Pointer to SRIO socket parameter structure.  This structure will be
     *  replicated within the TransportSrio instance.  So structure pointed
     *  to can be allocated from temporary memory. */
    TransportSrio_SocketParams  *sock_params;
    /* Maximum size, in bytes, of data packets sent and received over
     * SRIO MPM-Transport.  MessageQ messages greater than equal to or greater
     * than max_mtu bytes will be fragmented into segments of max_mtu size prior
     * to sending. max_mtu cannot exceed the SRIO HW's max MTU size of 4096 */
    UInt32                       max_mtu;
    /** Network Transport ID of ARMv7 Linux TransportQmss instance.  Will be
     *  used to reroute received MessageQ messages destined for a MessageQ
     *  queue that was opened from another Linux process.  Messages received
     *  for a MessageQ queue opened from another Linux process will be dropped
     *  if this value is left NULL */
    Int                          reroute_tid;
    /** Pointer to the SRIO device initialization routine.  This routine
     *  initializes the SRIO hardware with routing and address information.
     *  This routine should be run once per device.  This function pointer
     *  should be NULL if another process or device core was the first to
     *  initialize the usage of SRIO. A pointer to this function must be
     *  provided so MPM-Transport can properly initialize SRIO. The init_cfg
     *  input parameter can be a pointer to an application specific input
     *  parameter to the srio_device_init function */
    Int                        (*srio_device_init) (Void *init_cfg,
                                                    Void *srio_base_addr,
                                                    UInt32 serdes_addr);
    /** Application specific input parameter to the srio_device_init
     *  implementation. */
    Void                        *init_cfg;
    /** Pointer to the SRIO device de-initialization routine.  Will be
     *  invoked by MPM-Transport when de-initializing the SRIO hardware within
     *  deletion routines.  This routine should be run once per device.  This
     *  function pointer should be NULL if another process or device core will
     *  be the last to delete its usage of SRIO.  The deinit_cfg input
     *  parameter can be a pointer to an application specific input parameter
     *  to the srio_device_deinit function. */
    Int                        (*srio_device_deinit) (Void *deinit_cfg,
                                                      Void *srio_base_addr);
    /** Application specific input parameter to the srio_device_deinit
     *  implementation */
    Void                        *deinit_cfg;
    /** Controls whether MPM-Transport will initialize the QMSS hardware upon
     *  opening.
     *  0 - MPM-Transport will not initialize QMSS.  Set if another entity
     *      within the system initialized the QMSS hardware.
     *  1 - MPM-Transport will initialize QMSS.  Set if this is the first
     *      system entity being created that used QMSS and QMSS has not been
     *      initialized */
    Int                          mpm_trans_init_qmss;
} TransportSrio_Params;

/*
 *  ======== TransportSrio_Params_init__S ========
 *  Internal system function, not to be called by the user.
 */
Void TransportSrio_Params_init__S(TransportSrio_Params *params, Int version);

/*
 *  ======== TransportSrio_Params_init ========
 *  Using a static inline method binds the parameter structure version
 *  at the calls site. This allows new library implementations to recognize
 *  old call sites and thus to dereference the parameter structure correctly.
 **
 *  @b Description
 *  @n  
 *      Initializes the TransportSrio initialization parameter structure
 *      to its default values.
 *
 *  @param[in]  params
 *      Pointer to TransportSrio initialization parameter structure
 */
static inline Void TransportSrio_Params_init(TransportSrio_Params *params)
{
    if (params != NULL) {
        TransportSrio_Params_init__S(params, TransportSrio_Params_VERSION);
    }
}

/*
 *  ======== TransporSrio_create ========
 **
 *  @b Description
 *  @n  
 *      Creates an instance of TransporSrio.  The returned transport handle
 *      must be registered with MessageQ using the 
 *      MessageQ_registerTransportId API before the transport can be used
 *      to route MessageQ messages.
 *
 *  @param[in]  params
 *      Pointer to the TransporSrio initialization structure
 *
 *  @retval
 *      Success - Non-zero TransporSrio_Handle
 *  @retval
 *      Failure - NULL TransporSrio_Handle
 */
TransportSrio_Handle TransportSrio_create(TransportSrio_Params *params);

/*
 *  ======== TransporSrio_delete ========
 **
 *  @b Description
 *  @n  
 *      Finalizes a TransporSrio instance.  The TransporSrio_Handle will
 *      be made NULL upon return.
 *
 *  @param[in]  hp
 *      Pointer to the TransporSrio_Handle to be deleted
 *  @param[out] hp
 *       Dereferenced TransporSrio_Handle value will be made NULL upon 
 *       successful finalization
 */
Void TransportSrio_delete(TransportSrio_Handle *hp);

/*
 *  ======== TransporSrio_upCast ========
 **
 *  @b Description
 *  @n  
 *      Converts a TransporSrio instance handle to its inherited
 *      INetworkTransport_Handle
 *
 *  @param[in]  TransporSrio_Handle
 *      TransporSrio_handle
 *
 *  @retval
 *      INetworkTransport_Handle stored within the TransporSrio_Handle
 */
INetworkTransport_Handle TransportSrio_upCast(TransportSrio_Handle inst);

/*
 *  ======== TransporSrio_downCast ========
 **
 *  @b Description
 *  @n  
 *      Converts an INetworkTransport_Handle instance handle to its
 *      corresponding TransporSrio_Handle.
 *
 *      It is the caller's responsibility to ensure the underlying object
 *      is of the correct type.
 *
 *  @param[in]  INetworkTransport_Handle corresponding to a
 *      TransporSrio_handle
 *
 *  @retval
 *      Opaque TransporSrio_Handle
 */
TransportSrio_Handle TransportSrio_downCast(INetworkTransport_Handle base);

/*
 *  ======== TransportSrio_getVersion ========
 **
 *  @b Description
 *  @n  
 *      The function is used to get the version information of TransportSrio.
 *
 *  @retval
 *      Version Information.
 */
UInt32 TransportSrio_getVersion(Void);

/*
 *  ======== TransportSrio_getVersionStr ========
 **
 *  @b Description
 *  @n  
 *      The function is used to get the version string for TransportSrio.
 *
 *  @retval
 *      Version String.
 */
const Char *TransportSrio_getVersionStr(Void);

#endif /* TRANSPORTSRIO_H_ */

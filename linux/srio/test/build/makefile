#
# Macro definitions referenced below
#

empty =
space =$(empty) $(empty)

ARMV7OBJDIR ?= ./obj
ARMV7BINDIR ?= ./bin/$(DEVICE)
ARMV7LIBDIR ?= ./lib

ARMV7OBJDIR := $(ARMV7OBJDIR)/test
ARMV7BINDIR := $(ARMV7BINDIR)/test

#Cross tools
ifdef CROSS_TOOL_INSTALL_PATH
# Support backwards compatibility with KeyStone1 approach
 CC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
 AC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)as
 AR = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)ar
 LD = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
endif

ifdef DEVKIT_INSTALL_PATH
  IPCLIBDIRS := -L$(DEVKIT_INSTALL_PATH)/lib/
else
  IPCLIBDIRS :=
endif

# INCLUDE Directories
TRANS_SRIO_INC_DIR = $(ROOTDIR)

TRANS_SRIO_SRC_DIR = $(ROOTDIR)
TRANS_SRIO_TEST_DIR = $(TRANS_SRIO_SRC_DIR)/test
TRANS_SRIO_ARM_LIN_TEST_DIR = $(TRANS_SRIO_TEST_DIR)

INCDIR := $(PDK_INSTALL_PATH);$(TRANS_SRIO_INC_DIR);$(TRANS_SRIO_ARM_LIN_TEST_DIR)

# Libraries
IPC_LIBS = -ltitransportrpmsg -ltiipc -ltiipcutils
# Reparse srio using whole-archive since mpm-transport contains weak symbols that must be replaced with strong symbols from libsrio
LLD_LIBS = -lqmss_$(DEVICE) -lcppi_$(DEVICE) -Wl,--whole-archive -lsrio -Wl,--no-whole-archive -lrm -ledma3 -ledma3rm
LLD_LIBS_SO = -lqmss_device -lcppi_device -lsrio -lrm -ledma3 -ledma3rm
MMAP_LIB = -lkeystonemmap
CMEM_LIB = -lticmem
MPM_LIB = -lmpmtransport
TRANSPORTQMSS_LIB = -lTransportQmss
TRANSPORTSRIO_LIB = -lTransportSrio

ifeq ($(USEDYNAMIC_LIB), yes)
#presuming ARM executable would depend on dynamic library dependency
EXE_EXTN = _so
LIBS     = $(IPC_LIBS) $(MPM_LIB) $(LLD_LIBS_SO) $(MMAP_LIB) $(CMEM_LIB) $(TRANSPORTQMSS_LIB) $(TRANSPORTSRIO_LIB)
else
#forcing ARM executable to depend on static LLD libraries, kernel libraries remain shared
EXE_EXTN =
LIBS     = -Wl,-Bstatic $(IPC_LIBS) $(MPM_LIB) $(LLD_LIBS) $(TRANSPORTQMSS_LIB) $(TRANSPORTSRIO_LIB) -Wl,-Bdynamic $(MMAP_LIB) $(CMEM_LIB)
endif

# convert $(DEVICE) to a define without depending on tr to upcase
ifeq ($(DEVICE),k2h)
  DEVICE_DEFINE=-DDEVICE_K2H
else 
  ifeq ($(DEVICE),k2k)
    DEVICE_DEFINE=-DDEVICE_K2K
  else
    ifeq ($(DEVICE),k2l)
      DEVICE_DEFINE=-DDEVICE_K2L
    else  
      ifeq ($(DEVICE),k2e)
        DEVICE_DEFINE=-DDEVICE_K2E
      else  
        $(error unknown device)
      endif  
    endif  
  endif
endif

# Compiler options
INTERNALDEFS = $(DEBUG_FLAG) -D__ARMv7 $(DEVICE_DEFINE) -D_VIRTUAL_ADDR_SUPPORT -D__LINUX_USER_SPACE -D_LITTLE_ENDIAN=1 -DMAKEFILE_BUILD -D_GNU_SOURCE

# Linker options
INTERNALLINKDEFS = -Wl,--start-group -L$(ARMV7LIBDIR) $(LIBS) -lrt -lm -Wl,--end-group -pthread $(LDFLAGS)

PRODUCER_EXE=producer$(EXE_EXTN).out
CONSUMER_EXE=consumer$(EXE_EXTN).out

OBJEXT = o 

SRCDIR = $(TRANS_SRIO_ARM_LIN_TEST_DIR)

INCS = -I. -I$(strip $(subst ;, -I,$(INCDIR)))

VPATH=$(SRCDIR)

#List the Source Files
PRODUCER_SRC = \
    producer.c \
    producer_device_srio.c \
    sockutils.c \
    test_common.c

CONSUMER_SRC = \
    consumer.c \
    consumer_device_srio.c \
    sockutils.c \
    test_common.c
    
# FLAGS for the SourceFiles
SRC_CFLAGS = -I. $(CFLAGS)

# Make Rule for the SRC Files
PRODUCER_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(PRODUCER_SRC))
CONSUMER_SRC_OBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(CONSUMER_SRC))

all:$(ARMV7BINDIR)/$(PRODUCER_EXE) $(ARMV7BINDIR)/$(CONSUMER_EXE)

$(ARMV7BINDIR)/$(PRODUCER_EXE): $(PRODUCER_SRC_OBJS) $(ARMV7BINDIR)/.created
	@echo linking $(PRODUCER_SRC_OBJS) into $@ ...
	$(CC) $(PRODUCER_SRC_OBJS) $(INTERNALLINKDEFS) -o $@
	
$(ARMV7BINDIR)/$(CONSUMER_EXE): $(CONSUMER_SRC_OBJS) $(ARMV7BINDIR)/.created
	@echo linking $(CONSUMER_SRC_OBJS) into $@ ...
	$(CC) $(CONSUMER_SRC_OBJS) $(INTERNALLINKDEFS) -o $@
	
$(ARMV7OBJDIR)/%.$(OBJEXT): %.c $(ARMV7OBJDIR)/.created
	@echo compiling $< ...
	$(CC) -c $(SRC_CFLAGS) $(INTERNALDEFS) $(INCS)  $< -o $@

$(ARMV7OBJDIR)/.created:
	@mkdir -p $(ARMV7OBJDIR)
	@touch $(ARMV7OBJDIR)/.created

$(ARMV7BINDIR)/.created:
	@mkdir -p $(ARMV7BINDIR)
	@touch $(ARMV7BINDIR)/.created

clean:
	@rm -fr $(ARMV7OBJDIR)
	@rm -fr $(ARMV7BINDIR)


/*
 *   test_common.h
 *
 *   Common defines and data structures for the SRIO test code
 *
 *  ============================================================================
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __TEST_COMMON_H__
#define __TEST_COMMON_H__

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>

/* RM includes */
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>

/* Socket utility includes */
#include "sockutils.h"

/* Number of processes on consumer to test MessageQ transmission to */
#define CONSUMER_PROCESSES         2
/* MultiProc ID of Linux Host processor on producer device */
#define PRODUCER_HOST_PROC_ID      0
/* MultiProc ID of Linux Host processor on consumer device */
#define CONSUMER_HOST_PROC_ID      9
/* Number of processors for two k2hk devices */
#define MAX_SYSTEM_PROCESSORS      18
/* Number of messages sent to each consumer process by producer */
#define MAX_TEST_MSGS_SENT         100
/* SRIO path mode
 * 0 - 4 1x ports
 * 1 - 2 1x and 1 2x ports
 * 2 - 1 2x and 2 1x ports
 * 3 - 2 2x ports
 * 4 - 1 4x port */
#define SRIO_PATH_MODE             4

#define TEST_PASS                  0
#define TEST_FAIL                  1
#define TEST_TIMEOUT               2

#define MPM_INST_NAME_LEN          20
#define MSGQ_Q_NAME_LEN            32

/* Number of round trips packet should travel */
#define TEST_ITERATIONS            1

#define SRIO_MAX_MTU               4096
#define MAX_PACKET_SIZE_STD        2048
#define MAX_PACKET_SIZE_FRAG       4096

#define TRANS_QMSS_NET_ID          1
#define TRANS_SRIO_T11_NET_ID      2
#define TRANS_SRIO_T9_NET_ID       3

#define MESSAGEQ_RESERVED_RCV_Q    0

/* Default stack size for transportQmssRcv thread */
#define THREAD_DEFAULT_STACK_SIZE  0x8000

/* Application's registered RM transport indices */
#define SERVER_TO_CLIENT_MAP_ENTRY 0
/* Maximum number of registered RM transports */
#define MAX_MAPPING_ENTRIES        1

/* Client socket name */
#define MAX_CLIENT_SOCK_NAME       32

#define FILL_DATA_BYTE(proc, idx) (uint8_t)(((proc & 0xF) << 4) | (idx & 0xF))

typedef struct {
    MessageQ_MsgHeader header; /* 32 bytes */
    uint32_t           info;
    uint32_t           flags;
#define NULL_FLAG       0
#define READY_FLAG      1
#define READY_ACK_FLAG  2
#define COMPLETION_FLAG 3
    uint8_t            data[MAX_PACKET_SIZE_STD -
                            sizeof(MessageQ_MsgHeader) -
                            sizeof(uint32_t) -
                            sizeof(uint32_t)];
} test_msg_t;

typedef struct {
    MessageQ_MsgHeader header; /* 32 bytes */
    uint32_t           info;
    /* Same flags defined in test_msg_t apply here */
    uint32_t           flags;
    uint8_t            data[MAX_PACKET_SIZE_FRAG -
                            sizeof(MessageQ_MsgHeader) -
                            sizeof(uint32_t) -
                            sizeof(uint32_t)];
} frag_test_msg_t;

typedef struct trans_map_entry_s {
    Rm_TransportHandle  trans_handle;
    sock_name_t        *remote_sock;
} trans_map_entry_t;

Rm_Packet *transportAlloc(Rm_AppTransportHandle appTransport, uint32_t pktSize,
                          Rm_PacketHandle *pktHandle);
void transportFree(Rm_Packet *rm_pkt);
void transportReceive(void);
int32_t transportSendRcv (Rm_AppTransportHandle appTransport,
                          Rm_PacketHandle pktHandle);
int thread_create(void *(thread_routine)(void *), void *args, void *handle);
int rm_connection_setup(void);
Rm_ServiceHandle *init_rm (void);

int signal_remote(MessageQ_QueueId remote_q_id, int tid, uint8_t flag_val,
                  uint32_t info);
int signal_wait(MessageQ_Handle loc_q_h, uint8_t flag_val, uint timeout,
                uint32_t *info);

#endif

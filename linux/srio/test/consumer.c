/*
 *   consumer.c
 *
 *   Multi-process TransportSrio test that sends messages between
 *   Linux processes on different devices using IPC MessageQ.
 *   The messages are transmitted between the processes by way 
 *   of the TransportSrio transport.
 *
 *  ============================================================================
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Shut off: remark #880-D: parameter "appTransport" was never referenced
 *
 * This is better than removing the argument since removal would break
 * backwards compatibility
 */
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
 * warning: unused parameter �deinitCfg, argc, and argv� [-Wunused-parameter]
 */
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/* Standard includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>
#include <ti/ipc/transports/TransportQmss.h>
#include <ti/ipc/transports/TransportSrio.h>

/* CSL SRIO Functional Layer */
#include <ti/csl/csl_srio.h>

/* RM includes */
#include <ti/drv/rm/rm_services.h>

/* Common test includes */
#include "test_common.h"

int                   local_process;
int                   next_process;

/* RM globals */
Rm_ServiceHandle     *rm_service_h = NULL;

/* Transport globals */
TransportQmss_Handle  qmss_trans_h = NULL;
TransportSrio_Handle  srio_trans_t11_h = NULL;
TransportSrio_Handle  srio_trans_t9_h = NULL;
MessageQ_Handle       loc_msg_q_h = NULL;
MessageQ_Handle       srio_msg_q_h = NULL;
MessageQ_QueueId      next_process_q_id = MessageQ_INVALIDMESSAGEQ;
MessageQ_QueueId      rem_srio_q_id = MessageQ_INVALIDMESSAGEQ;

/* These are the device identifiers used in the test Application */
uint32_t DEVICE_ID1_16BIT = 0xBEEF;
uint32_t DEVICE_ID2_16BIT = 0x4560;
uint32_t DEVICE_ID3_16BIT = 0x1234;
uint32_t DEVICE_ID4_16BIT = 0x5678;
uint32_t DEVICE_ID1_8BIT  = 0xAB;
uint32_t DEVICE_ID2_8BIT  = 0xCD;
uint32_t DEVICE_ID3_8BIT  = 0x12;
uint32_t DEVICE_ID4_8BIT  = 0x34;

uint8_t path_mode         = SRIO_PATH_MODE;

extern int32_t SrioDevice_init(CSL_SrioHandle hSrio, uint32_t srioSerdesVAddr,
                               uint8_t pathMode);
extern int32_t SrioDevice_deinit(CSL_SrioHandle hSrio);

/* FUNCTION PURPOSE: SrioDevice_init wrapper
 ***********************************************************************
 * DESCRIPTION: Wraps test implementation of SrioDevice_init function so it's
 *              compatible with the function pointer format required by
 *              MPM-Transport.
 */
int mySrioDevice_init(void *initCfg, void *srioAddr, uint32_t serdesAddr)
{
    uint8_t *p_path_mode = (uint8_t *)initCfg;
    
    return (SrioDevice_init)(srioAddr, serdesAddr, *p_path_mode);
}

/* FUNCTION PURPOSE: SrioDevice_deinit wrapper
 ***********************************************************************
 * DESCRIPTION: Wraps test implementation of SrioDevice_deinit function so it's
 *              compatible with the function pointer format required by
 *              MPM-Transport.
 */
int mySrioDevice_deinit(void *deinitCfg, void *srioAddr)
{
    return (SrioDevice_deinit)(srioAddr);
}

/* FUNCTION PURPOSE: Tests multi-processor connectivity over TransportSrio
 ***********************************************************************
 * DESCRIPTION: Receives MessageQ messages from a producer application on
 *              another device.
 *
 *              test_frag: 0 - standard packet send/rcv testing
 *                         1 - fragmented packet send/rcv testing
 */
int connectivity_test_rcv(int test_frag)
{
    MessageQ_Msg msg_q_msg = NULL;
    int          total_rcvd;
    int          i;
    int          ret = TEST_FAIL;

    total_rcvd = 0;

    for (i = 0; i < MAX_TEST_MSGS_SENT;) {
        int status;

        status = MessageQ_get(srio_msg_q_h, &msg_q_msg, MessageQ_FOREVER);
        if (status < 0) {
            printf("ERROR Process %d : MessageQ_get failed with error %d\n",
                   local_process, status);
            goto err_exit;
        }

        total_rcvd++;

        if (msg_q_msg->flags == READY_FLAG) {
            printf("Process %d : "
                   "Dropping message because flag value was not NULL",
                   local_process);
            /* Drop residual message from sync prior to test */
            MessageQ_free(msg_q_msg);
            /* Don't increment receive counter by continuing */
            continue;
        }

        if (test_frag) {
            frag_test_msg_t *large_msg = (frag_test_msg_t *)msg_q_msg;
            size_t           j;

            for (j = 0; j < sizeof(large_msg->data); j++) {
                uint8_t expected = FILL_DATA_BYTE(0, j);

                if (large_msg->data[j] != expected) {
                    printf("ERROR Process %d : Bad data byte %4d\n"
                           "                   Got:      0x%2x\n"
                           "                   Expected: 0x%2x\n",
                           local_process, j, large_msg->data[i], expected);
                    goto err_exit;
                }
            }
        } else {
            test_msg_t *msg = (test_msg_t *)msg_q_msg;
            size_t      j;

            for (j = 0; j < sizeof(msg->data); j++) {
                uint8_t expected = FILL_DATA_BYTE(0, j);

                if (msg->data[j] != expected) {
                    printf("ERROR Process %d : Bad data byte %4d\n"
                           "                   Got:      0x%2x\n"
                           "                   Expected: 0x%2x\n",
                           local_process, j, msg->data[i], expected);
                    goto err_exit;
                }
            }
        }
        i++;
        printf("Process %d : Message receive stats\n"
               "             Expected valid - %d\n"
               "             Received valid - %d\n"
               "             Received total - %d\n",
               local_process, MAX_TEST_MSGS_SENT, i, total_rcvd);
    }

    ret = TEST_PASS;
err_exit:
    if (msg_q_msg) {
        MessageQ_free(msg_q_msg);
    }
    return (ret);
}

int init_messageQs(void)
{
    Int32           status = 0;
    MessageQ_Params msg_params;
    char            msg_q_name[MSGQ_Q_NAME_LEN];
    char            remote_q_name[MSGQ_Q_NAME_LEN];

    /* Create local, inter-process MessageQ */
    snprintf(msg_q_name, MSGQ_Q_NAME_LEN, "Process_%d_MsgQ", local_process);
    MessageQ_Params_init(&msg_params);
    loc_msg_q_h = MessageQ_create(msg_q_name, &msg_params);
    if (loc_msg_q_h == NULL) {
        printf("ERROR Process %d : Failed to create MessageQ\n", local_process);
        status = -1;
        goto err_exit;
    }
    printf("Process %d : Created Local MessageQ: %s, QId: 0x%x\n",
           local_process, msg_q_name, MessageQ_getQueueId(loc_msg_q_h));

    /* Open next process's MessageQ */
    snprintf(remote_q_name, MSGQ_Q_NAME_LEN, "Process_%d_MsgQ",
             next_process);
    printf ("Process %d : Attempting to open next process queue: %s\n",
            local_process, remote_q_name);
    do {
        status = MessageQ_open(remote_q_name, &next_process_q_id);
        sleep(1);
    } while ((status == MessageQ_E_NOTFOUND) || (status == MessageQ_E_TIMEOUT));
    if (status < 0) {
        printf("ERROR Process %d : Error %d when opening next process MsgQ\n",
               local_process, status);
        status = -1;
        goto err_exit;
    } else {
        printf("Process %d : Opened Remote queue: %s, QId: 0x%x\n",
               local_process, remote_q_name, next_process_q_id);
    }

    /* Make sure main process is first to create reserved MessageQ queue */
    MessageQ_Params_init(&msg_params);
    msg_params.queueIndex = MESSAGEQ_RESERVED_RCV_Q + local_process;
    srio_msg_q_h = MessageQ_create(NULL, &msg_params);
    if (srio_msg_q_h == NULL) {
        printf("ERROR Process %d : Failed to create MessageQ\n",
               local_process);
        status = -1;
        goto err_exit;
    }
    printf("Process %d : "
           "Created remote device reception MessageQ with QId: 0x%x\n",
           local_process, MessageQ_getQueueId(srio_msg_q_h));

    if (local_process == 0) {
        /* Open Producer Linux Host MessageQ queue */
        rem_srio_q_id = MessageQ_openQueueId(MESSAGEQ_RESERVED_RCV_Q,
                                             PRODUCER_HOST_PROC_ID);
        printf("Process %d : Opened remote device QId: 0x%x\n",
               local_process, rem_srio_q_id);
    }

err_exit:
    return(status);
}

/* FUNCTION PURPOSE: TransportSrio instance initialization
 ***********************************************************************
 * DESCRIPTION: Creates the TransportSrio Type 11 and Type 9 instances and
 *              registers them with MessageQ.
 */
int srio_trans_init(void)
{
    Int32                        status = 0;
    Char                         mpm_inst_name[MPM_INST_NAME_LEN];
    TransportSrio_Params         srio_trans_params;
    TransportSrio_SocketParams   sock_params;
    TransportSrio_Type11EpParams t11_params[MAX_SYSTEM_PROCESSORS];
    TransportSrio_Type9EpParams  t9_params[MAX_SYSTEM_PROCESSORS];
    INetworkTransport_Handle     net_trans_h;
    ITransport_Handle            base_trans_h;

    /* Type 11 instance */

    TransportSrio_Params_init(&srio_trans_params);
    snprintf(mpm_inst_name, MPM_INST_NAME_LEN, "arm-srio-generic");
    srio_trans_params.mpm_trans_inst_name = mpm_inst_name;
    srio_trans_params.rm_service_h        = rm_service_h;
    srio_trans_params.max_mtu             = SRIO_MAX_MTU;
    srio_trans_params.reroute_tid         = TRANS_QMSS_NET_ID;
    srio_trans_params.srio_device_init    = &mySrioDevice_init;
    srio_trans_params.init_cfg            = (void *)&path_mode;
    srio_trans_params.srio_device_deinit  = &mySrioDevice_deinit;
    srio_trans_params.deinit_cfg          = NULL;
    /* TransportQmss informs mpm-transport to init QMSS */
    srio_trans_params.mpm_trans_init_qmss = 0;
    
    /* Configure producer's static socket parameters. Structures can
     * be local since TransportSrio will make a copy */
    memset(&t11_params, 0, sizeof(t11_params));
    sock_params.num_eps   = MAX_SYSTEM_PROCESSORS;
    sock_params.sock_type = sock_TYPE_11;
    
    /* Linux Host (Producer) */
    t11_params[0].tt        = 0;
    t11_params[0].device_id = DEVICE_ID1_8BIT;
    t11_params[0].letter    = 0;
    t11_params[0].mailbox   = 0;
    t11_params[0].seg_map   = (MAX_PACKET_SIZE_STD > 256) ? 1 : 0;
    
    /* Linux Host (Consumer) */
    t11_params[9].tt        = 0;
    t11_params[9].device_id = DEVICE_ID2_8BIT;
    t11_params[9].letter    = 0;
    t11_params[9].mailbox   = 0;
    t11_params[9].seg_map   = (MAX_PACKET_SIZE_STD > 256) ? 1 : 0;
    sock_params.u.t11_eps = &t11_params[0];
    srio_trans_params.sock_params = &sock_params;
    printf("Process %d : Creating TransportSrio Type 11 instance\n",
           local_process);
    srio_trans_t11_h = TransportSrio_create(&srio_trans_params);
    if (!srio_trans_t11_h) {
        printf("ERROR Process %d : "
               "Failed to create TransportSrio Type 11 handle\n", 
               local_process);
        status = -1;
        goto err_exit;
    }
    
    /* Register transport with MessageQ as network transport */
    net_trans_h = TransportSrio_upCast(srio_trans_t11_h);
    base_trans_h = INetworkTransport_upCast(net_trans_h);
    if (MessageQ_registerTransportId(TRANS_SRIO_T11_NET_ID, base_trans_h) < 0) {
        printf("ERROR Process %d : "
               "Failed to register TransportSrio Type 11 as network "
               "transport with TID %d\n", local_process, TRANS_SRIO_T11_NET_ID);
        status = -1;
        goto err_exit;
    }

    /* Type 9 instance */

    TransportSrio_Params_init(&srio_trans_params);
    snprintf(mpm_inst_name, MPM_INST_NAME_LEN, "arm-srio-generic");
    srio_trans_params.mpm_trans_inst_name = mpm_inst_name;
    srio_trans_params.rm_service_h        = rm_service_h;
    srio_trans_params.max_mtu             = SRIO_MAX_MTU;
    srio_trans_params.reroute_tid         = TRANS_QMSS_NET_ID;
    /* Don't run device init/deinit for second transport */
    srio_trans_params.srio_device_init    = NULL;
    srio_trans_params.init_cfg            = NULL;
    srio_trans_params.srio_device_deinit  = NULL;
    srio_trans_params.deinit_cfg          = NULL;
    /* TransportQmss informs mpm-transport to init QMSS */
    srio_trans_params.mpm_trans_init_qmss = 0;
    
    /* Configure producer's static socket parameters. Structures can
     * be local since TransportSrio will make a copy */
    memset(&t9_params, 0, sizeof(t9_params));
    sock_params.num_eps   = MAX_SYSTEM_PROCESSORS;
    sock_params.sock_type = sock_TYPE_9;
    
    /* Linux Host (Producer) */
    t9_params[0].tt        = 0;
    t9_params[0].device_id = DEVICE_ID1_8BIT;
    t9_params[0].cos       = 0;
    t9_params[0].stream_id = 0;
    
    /* Linux Host (Consumer) */
    t9_params[9].tt        = 0;
    t9_params[9].device_id = DEVICE_ID2_8BIT;
    t9_params[9].cos       = 0;
    t9_params[9].stream_id = 0;
    sock_params.u.t9_eps = &t9_params[0];
    srio_trans_params.sock_params = &sock_params;
    printf("Process %d : Creating TransportSrio Type 9 instance\n",
           local_process);
    srio_trans_t9_h = TransportSrio_create(&srio_trans_params);
    if (!srio_trans_t9_h) {
        printf("ERROR Process %d : "
               "Failed to create TransportSrio Type 9 handle\n", 
               local_process);
        status = -1;
        goto err_exit;
    }
    
    /* Register transport with MessageQ as network transport */
    net_trans_h = TransportSrio_upCast(srio_trans_t9_h);
    base_trans_h = INetworkTransport_upCast(net_trans_h);
    if (MessageQ_registerTransportId(TRANS_SRIO_T9_NET_ID, base_trans_h) < 0) {
        printf("ERROR Process %d : "
               "Failed to register TransportSrio Type 9 as network "
               "transport with TID %d\n", local_process, TRANS_SRIO_T9_NET_ID);
        status = -1;
        goto err_exit;
    }

err_exit:
    return(status);
}

/* FUNCTION PURPOSE: Common process initialization for IPC transports
 ***********************************************************************
 * DESCRIPTION: Initializes components required for IPC transports.  All
 *              components must be initialized per process utilizing IPC.
 */
int init_ipc(void)
{
    Int32                    status = 0;
    Char                     mpm_inst_name[MPM_INST_NAME_LEN];
    TransportQmss_Params     qm_trans_params;
    INetworkTransport_Handle net_trans_h;
    ITransport_Handle        base_trans_h;

    Ipc_transportConfig(&TransportRpmsg_Factory);
    status = Ipc_start();
    if (status < 0) {
        printf("ERROR Process %d : Ipc_start failed with error %d\n", 
               local_process, status);
    }

    /* Setup RM */
    if ((rm_service_h = init_rm()) == NULL) {
        printf ("ERROR Process %d : Could not initialize RM, exiting\n",
                local_process);
        return (-1);
    }

    TransportQmss_Params_init(&qm_trans_params);
    snprintf(mpm_inst_name, MPM_INST_NAME_LEN, "arm-qmss-generic");
    qm_trans_params.mpm_trans_inst_name = mpm_inst_name;
    qm_trans_params.rm_service_h        = rm_service_h;
    qm_trans_params.rx_msg_size_bytes   = MAX_PACKET_SIZE_FRAG;
    qm_trans_params.mpm_trans_init_qmss = 1;
    printf("Process %d : Creating TransportQmss instance\n", local_process);
    qmss_trans_h = TransportQmss_create(&qm_trans_params);
    if (!qmss_trans_h) {
        printf("ERROR Process %d : Failed to create TransportQmss handle\n", 
               local_process);
        status = -1;
        goto err_exit;
    }

    /* Register transport with MessageQ as network transport */
    net_trans_h = TransportQmss_upCast(qmss_trans_h);
    base_trans_h = INetworkTransport_upCast(net_trans_h);
    if (MessageQ_registerTransportId(TRANS_QMSS_NET_ID, base_trans_h) < 0) {
        printf("ERROR Process %d : "
               "Failed to register TransportQmss as network transport\n",
               local_process);
        status = -1;
        goto err_exit;
    }

err_exit:
    return(status);
}

int main(int argc, char *argv[])
{
    Int32 status = 0;
    pid_t pid;
    int   i;

    printf("*********************************************************\n");
    printf("**** TransportSrio Linux Multi-Board Test (Consumer) ****\n");
    printf("*********************************************************\n");

    printf("TransportSrio Version : 0x%08x\nVersion String: %s\n",
           TransportSrio_getVersion(), TransportSrio_getVersionStr());

    /* Fork one process to test TransportSrio process reroute capabilities */
    pid = fork();

    /* Disabling buffering of stdout for proper print out flow */
    setvbuf(stdout, NULL, _IONBF, 0);

    if (pid) {
        local_process = 0;
        next_process = 1;
    } else {
        local_process = 1;
        next_process = 0;
    }

    if ((status = init_ipc()) < 0) {
        goto err_exit;
    }
    if (local_process == 0) {
        if ((status = srio_trans_init()) < 0) {
            goto err_exit;
        }
    }
    if ((status = init_messageQs()) < 0) {
        goto err_exit;
    }

    if (local_process == 0) {
        /* Wait for signal from producer device to start Type 11 connectivity
         * test */
        printf("Process %d : Syncing with producer device...\n", local_process);
        signal_wait(srio_msg_q_h, READY_FLAG, MessageQ_FOREVER, NULL);
        /* Send ACK back to main process on producer device */
        signal_remote(rem_srio_q_id, TRANS_SRIO_T11_NET_ID, READY_ACK_FLAG, 0);
    }

    printf("Process %d : TransportSrio Type 11 receive connectivity test\n",
           local_process);
    if (connectivity_test_rcv(0) == TEST_FAIL) {
        printf("Process %d : Test FAILED\n", local_process);
        goto err_exit;
    }

    if (local_process == 0) {
        /* Wait for signal from producer device to start Type 9 connectivity
         * test */
        printf("Process %d : Syncing with producer device...\n", local_process);
        signal_wait(srio_msg_q_h, READY_FLAG, MessageQ_FOREVER, NULL);
        /* Send ACK back to main process on producer device */
        signal_remote(rem_srio_q_id, TRANS_SRIO_T9_NET_ID, READY_ACK_FLAG, 0);
    }

    printf("Process %d : TransportSrio Type 9 receive connectivity test\n",
           local_process);
    if (connectivity_test_rcv(0) == TEST_FAIL) {
        printf("Process %d : Test FAILED\n", local_process);
        goto err_exit;
    }

    if (local_process == 0) {
        /* Wait for signal from producer device to start Type 11 connectivity
         * test with fragmentation */
        printf("Process %d : Syncing with producer device...\n", local_process);
        signal_wait(srio_msg_q_h, READY_FLAG, MessageQ_FOREVER, NULL);
        /* Send ACK back to main process on producer device */
        signal_remote(rem_srio_q_id, TRANS_SRIO_T11_NET_ID, READY_ACK_FLAG, 0);
    }

    printf("Process %d : TransportSrio Type 11 receive connectivity test "
           "with fragmentation\n",
           local_process);
    if (connectivity_test_rcv(1) == TEST_FAIL) {
        printf("Process %d : Test FAILED\n", local_process);
        goto err_exit;
    }

    if (local_process == 0) {
        /* Wait for signal from producer device to start Type 9 connectivity
         * test with fragmentation */
        printf("Process %d : Syncing with producer device...\n", local_process);
        signal_wait(srio_msg_q_h, READY_FLAG, MessageQ_FOREVER, NULL);
        /* Send ACK back to main process on producer device */
        signal_remote(rem_srio_q_id, TRANS_SRIO_T9_NET_ID, READY_ACK_FLAG, 0);
    }

    printf("Process %d : TransportSrio Type 9 receive connectivity test "
           "with fragmentation\n",
           local_process);
    if (connectivity_test_rcv(1) == TEST_FAIL) {
        printf("Process %d : Test FAILED\n", local_process);
        goto err_exit;
    }

    printf("Process %d : All Tests PASSED\n", local_process);

err_exit:

    if (local_process == 0) {
        for (i = 1; i < CONSUMER_PROCESSES; i++) {
            pid = wait(&status);
            if (WIFEXITED(status)) {
                if (WEXITSTATUS(status)) {
                    printf ("Child %d returned fail (%d)\n",
                            pid, WEXITSTATUS(status));
                }
            } else {
                printf ("Child %d failed to exit\n", pid);
            }
        }
    }

    if (local_process == (CONSUMER_PROCESSES - 1)) {
        printf("Cleaning up\n");
    }

    if (rem_srio_q_id != MessageQ_INVALIDMESSAGEQ) {
        MessageQ_close(&rem_srio_q_id);
    }
    if (srio_msg_q_h) {
        MessageQ_delete(&srio_msg_q_h);
    }
    if (next_process_q_id != MessageQ_INVALIDMESSAGEQ) {
        MessageQ_close(&next_process_q_id);
    }
    if (loc_msg_q_h) {
        MessageQ_delete(&loc_msg_q_h);
    }
    /* Delete Type 9 instance first since it was created second */
    if (srio_trans_t9_h) {
        MessageQ_unregisterTransportId(TRANS_SRIO_T9_NET_ID);
        TransportSrio_delete(&srio_trans_t9_h);
    }
    if (srio_trans_t11_h) {
        MessageQ_unregisterTransportId(TRANS_SRIO_T11_NET_ID);
        TransportSrio_delete(&srio_trans_t11_h);
    }
    if (qmss_trans_h) {
        MessageQ_unregisterTransportId(TRANS_QMSS_NET_ID);
        TransportQmss_delete(&qmss_trans_h);
    }

    if (local_process == 0){
        printf("Test Complete!\n");
    }
    return (status);
}

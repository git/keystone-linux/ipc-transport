/*
 *   test_common.c
 *
 *   Common initialization and utility functions for the SRIO test code
 *
 *  ============================================================================
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Shut off: remark #880-D: parameter "appTransport" was never referenced
 *
 * This is better than removing the argument since removal would break
 * backwards compatibility
 */
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
 * warning: unused parameter ‘appTransport’ [-Wunused-parameter]
 */
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/* Standard includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>

/* RM includes */
#include <ti/drv/rm/rm_server_if.h>
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>

/* Common test includes */
#include "test_common.h"

/* Socket utility includes */
#include "sockutils.h"

/* RM globals */
Rm_Handle          rm_client_handle = NULL;
Rm_ServiceHandle  *rm_client_service_handle = NULL;
sock_h             rm_client_sock;
trans_map_entry_t  rm_trans_map[MAX_MAPPING_ENTRIES];
char               rm_sock_name[MAX_CLIENT_SOCK_NAME];
char               rm_client_name[RM_NAME_MAX_CHARS];

extern int local_process;

/* FUNCTION PURPOSE: RM transport packet allocation function
 ***********************************************************************
 * DESCRIPTION: Allocates a function for the RM transport implementation.
 *              Packet is returned to RM to be populated with service request
 *              or response data.
 */
Rm_Packet *transportAlloc(Rm_AppTransportHandle appTransport, uint32_t pktSize,
                          Rm_PacketHandle *pktHandle)
{
    Rm_Packet *rm_pkt = NULL;

    rm_pkt = calloc(1, sizeof(*rm_pkt));
    if (!rm_pkt) {
        printf("ERROR Process %d : "
               "Can't malloc for RM send message (err: %s)\n",
               local_process, strerror(errno));
        return (NULL);
    }
    rm_pkt->pktLenBytes = pktSize;
    *pktHandle = rm_pkt;

    return(rm_pkt);
}

/* FUNCTION PURPOSE: RM transport packet free function
 ***********************************************************************
 * DESCRIPTION: Frees a function for the RM transport implementation.
 */
void transportFree(Rm_Packet *rm_pkt)
{
    if (rm_pkt) {
        free (rm_pkt);
    }
}

/* FUNCTION PURPOSE: RM transport packet receive function
 ***********************************************************************
 * DESCRIPTION: RM packet receive function for the RM transport
 *              implementation.  Receives packets over Linux socket interface
 *              and provides them to RM for processing.
 */
void transportReceive(void)
{
    int32_t             rm_result;
    int                 retval;
    size_t              length = 0;
    sock_name_t         server_sock_addr;
    Rm_Packet          *rm_pkt = NULL;
    struct sockaddr_un  server_addr;
    
    retval = sock_wait(rm_client_sock, (int *)&length, NULL, -1);
    if (retval == -2) {
        /* Timeout */
        printf("ERROR Process %d : Error socket timeout\n", local_process);
        return;
    } else if (retval < 0) {
        printf("ERROR Process %d : Error in reading from socket, error %d\n",
               local_process, retval);
        return;
    }
    
    if (length < sizeof(*rm_pkt)) {
        printf("ERROR Process %d : invalid RM message length %d\n",
               local_process, length);
        return;
    }
    rm_pkt = calloc(1, length);
    if (!rm_pkt) {
        printf("ERROR Process %d : "
               "can't malloc for recv'd RM message (err: %s)\n",
               local_process, strerror(errno));
        return;
    }
    
    server_sock_addr.type = sock_addr_e;
    server_sock_addr.s.addr = &server_addr;
    retval = sock_recv(rm_client_sock, (char *)rm_pkt, length,
                       &server_sock_addr);
    if (retval != ((int)length)) {
        printf("ERROR Process %d : "
               "recv RM pkt failed from socket, received = %d, expected = %d\n",
               local_process, retval, length);
        return;
    }
    
    /* Provide packet to RM Client for processing */
    if ((rm_result =
         Rm_receivePacket(rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].trans_handle,
                          rm_pkt))) {
        printf("ERROR Process %d : RM failed to process received packet: %d\n",
               local_process, rm_result);
    }

    transportFree(rm_pkt);
}

/* FUNCTION PURPOSE: RM transport packet send function
 ***********************************************************************
 * DESCRIPTION: Sends RM packets over the RM transport implementation.  Packets
 *              provided by RM are sent over the Linux socket interface.
 */
int32_t transportSendRcv (Rm_AppTransportHandle appTransport,
                          Rm_PacketHandle pktHandle)
{
    sock_name_t *server_sock_name = (sock_name_t *)appTransport;
    Rm_Packet   *rm_pkt = (Rm_Packet *)pktHandle;
    
    if (sock_send(rm_client_sock, (char *)rm_pkt, (int) rm_pkt->pktLenBytes,
                  server_sock_name)) {
        printf("ERROR Process %d : send data failed\n", local_process);
    }

    /* Wait for response from Server */
    transportReceive();
    return (0);
}

/* FUNCTION PURPOSE: Utility to create a thread
 ***********************************************************************
 * DESCRIPTION: Creates a thread that runs thread_routine with input
 *              arguments *args.
 */
int thread_create(void *(thread_routine)(void *), void *args, void *handle)
{
    int                max_priority, err;
    pthread_t          thread;
    pthread_attr_t     attr;
    struct sched_param param;

    max_priority = sched_get_priority_max(SCHED_FIFO);
    err = pthread_attr_init(&attr);
    if (err) {
        fprintf(stderr, "pthread_attr_init failed: (%s)\n", strerror(err));
        return(err);
    }
    err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    if (err) {
        fprintf(stderr, "pthread_attr_setdetachstate failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setstacksize(&attr, THREAD_DEFAULT_STACK_SIZE);
    if (err) {
        fprintf(stderr, "pthread_attr_setstacksize failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (err) {
        fprintf(stderr, "pthread_attr_setinheritsched failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (err) {
        fprintf(stderr, "pthread_attr_setschedpolicy failed: (%s)\n",
                strerror(err));
        return(err);
    }
    memset(&param, 0, sizeof(param));
    param.sched_priority = max_priority;
    err = pthread_attr_setschedparam(&attr, &param);
    if (err) {
        fprintf(stderr, "pthread_attr_setschedparam failed: (%s)\n",
                strerror(err));
        return(err);
    }
    if (err) return err;
    err = pthread_create(&thread, &attr, thread_routine, args);
    if (err) {
        fprintf(stderr, "pthread_create failed: (%s)\n", strerror(err));
        return(err);
    }
    if (err)
        return(err);
    
    *(pthread_t *)handle = thread;
    return(0);
}

/* FUNCTION PURPOSE: Signals remote process or processor via MessageQ
 ***********************************************************************
 * DESCRIPTION: Sends a MessageQ message to a remote process's or processor's
 *              MessageQ queue.  Used to signal that process of a start/stop
 *              action.
 */
int signal_remote(MessageQ_QueueId remote_q_id, int tid, uint8_t flag_val,
                  uint32_t info)
{
    test_msg_t *msg = NULL;
    Int32       status;

    msg = (test_msg_t *) MessageQ_alloc(0, sizeof(*msg));
    if (msg == NULL) {
        printf("ERROR Process %d : MessageQ_alloc failed\n", local_process);
        return(TEST_FAIL);
    }
    msg->flags = flag_val;
    msg->info  = info;

    MessageQ_setTransportId(msg, tid);
    status = MessageQ_put(remote_q_id, (MessageQ_Msg)msg);
    if (status < 0) {
        printf("ERROR Process %d : MessageQ_put failed with error %d\n",
               local_process, status);
        return(TEST_FAIL);
    }

    return(TEST_PASS);
}

/* FUNCTION PURPOSE: Waits for remote process or processor to signal via
 *                   MessageQ
 ***********************************************************************
 * DESCRIPTION: Waits for a signal from a remote process or processor via
 *              MessageQ.  Used to signal local process of a start/stop
 *              action.
 */
int signal_wait(MessageQ_Handle loc_q_h, uint8_t flag_val, uint timeout,
                uint32_t *info)
{
    test_msg_t *msg = NULL;
    Int32       status;
    int         ret_val = TEST_FAIL;
    
    /* Wait for signal */
    status = MessageQ_get(loc_q_h, (MessageQ_Msg *)&msg, (UInt) timeout);
    if ((status < 0) && (status != MessageQ_E_TIMEOUT)) {
        printf("ERROR Process %d : MessageQ_get failed with error %d\n",
               local_process, status);
        goto err_exit;
    } else if (status == MessageQ_E_TIMEOUT) {
        ret_val = TEST_TIMEOUT;
        goto err_exit;
    }

    if (msg->flags == flag_val) {
        if (info) {
            *info = msg->info;
        }
        ret_val = TEST_PASS;
    }
    MessageQ_free((MessageQ_Msg)msg);

err_exit:
    return(ret_val);
}

/* FUNCTION PURPOSE: Configures RM transport implementation path between RM
 *                   Server daemone and RM Clients
 ***********************************************************************
 * DESCRIPTION: Configures Linux sockets as the transport path between the RM
 *              Server daemon and the RM Clients.
 */
int rm_connection_setup(void)
{
    Rm_TransportCfg trans_cfg;
    int32_t         rm_result;
    int             i;
    sock_name_t     sock_name;
    char            server_sock_name[] = RM_SERVER_SOCKET_NAME;
    
    /* Initialize the transport map */
    for (i = 0; i < MAX_MAPPING_ENTRIES; i++) {
        rm_trans_map[i].trans_handle = NULL;
    }

    /* Create local socket for this RM Client */
    snprintf(rm_sock_name, MAX_CLIENT_SOCK_NAME,
             "/tmp/var/run/rm/rm_client%d", local_process);
    if (strlen(rm_sock_name) >= MAX_CLIENT_SOCK_NAME) {
        printf("ERROR Process %d : error - client socket name truncated\n",
               local_process);
        return -1;
    }
    sock_name.type = sock_name_e;
    sock_name.s.name = rm_sock_name;

    rm_client_sock = sock_open(&sock_name);
    if (!rm_client_sock) {
        printf("ERROR Process %d : "
               "connection_setup - Client socket open failed\n",
               local_process);
        return (-1);
    }

    /* Configure mapping to the RM Server daemon's socket */
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock = calloc(1, sizeof(sock_name_t));
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->type = sock_name_e;
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name = calloc(1, strlen(server_sock_name)+1);
    strncpy(rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name,
            server_sock_name, strlen(server_sock_name)+1);

    /* Register the Server with the Client instance */
    trans_cfg.rmHandle = rm_client_handle;
    trans_cfg.appTransportHandle = (Rm_AppTransportHandle) rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock;
    trans_cfg.remoteInstType = Rm_instType_SERVER;
    trans_cfg.transportCallouts.rmAllocPkt = transportAlloc;
    trans_cfg.transportCallouts.rmSendPkt = transportSendRcv;
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].trans_handle = Rm_transportRegister(&trans_cfg, &rm_result);

    return(0);
}

/* FUNCTION PURPOSE: RM Client initialization
 ***********************************************************************
 * DESCRIPTION: Initializes the RM Client for this test by establishing a
 *              socket connection with the RM Server
 */
Rm_ServiceHandle *init_rm(void)
{
    Rm_InitCfg rmInitCfg;
    int32_t    result;

    rm_client_handle = NULL;
    rm_client_service_handle = NULL;
    
    /* Initialize the RM Client - RM must be initialized before anything 
     * else in the system */
    memset(&rmInitCfg, 0, sizeof(rmInitCfg));
    snprintf(rm_client_name, RM_NAME_MAX_CHARS, "RM_Client%d", local_process);
    if (strlen(rm_client_name) >= RM_NAME_MAX_CHARS) {
        printf("ERROR Process %d : client name truncated\n", local_process);
        goto err_exit;
    }
    rmInitCfg.instName = rm_client_name;
    rmInitCfg.instType = Rm_instType_CLIENT;
    rm_client_handle = Rm_init(&rmInitCfg, &result);
    if (result != RM_OK) {
        printf("ERROR Process %d : RM initialization failed with error %d\n",
               local_process, result);
        goto err_exit;
    }

    printf("Process %d : Initialized %s\n", local_process, rm_client_name);

    /* Open Client service handle */
    rm_client_service_handle = Rm_serviceOpenHandle(rm_client_handle, &result);
    if (result != RM_OK) {
        printf("ERROR Process %d : Service handle open failed with error %d\n",
               local_process, result);
        goto err_exit;
    }

    if (rm_connection_setup()) {
        printf("ERROR Process %d : "
               "Failed setting up connection to RM Server\n", local_process);
    }

    return(rm_client_service_handle);
err_exit:
    if (rm_client_handle) {
        Rm_delete(rm_client_handle, 1);
    }
    return(NULL);
}

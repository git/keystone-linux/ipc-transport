/**
 *   @file  TransportQmss.c
 *
 *   @brief
 *      INetworkTransport interface implementation for IPC ARMv7 Linux
 *      MessageQ over MPM-Transport Navigator (QMSS)
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/*
 *  ======== TransportQmss.c ========
 */

/* Standard includes */
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/interfaces/INetworkTransport.h>

#include "TransportQmss.h"

/* LLD includes */
#include <ti/drv/rm/rm_services.h>

/* MPM-Transport includes */
#include <mpm_transport.h>

/* Transport version includes */
#include <../Transportver.h>

/* Global Variable that describes the Linux IPC Transport Version Information */
const char  transportQmssVersionStr[] = TRANSPORT_VERSION_STR ":" __DATE__  ":" __TIME__;

/* Initial allocation size of sorted list used for destination flow cache */
#define FLOW_CACHE_INIT_SIZE         16
/* Return value for flow IDs not found in the flow cache */
#define UNCACHED_FLOW_ID             (-1)
/* Default stack size for transportQmssRcv thread */
#define RX_THREAD_DEFAULT_STACK_SIZE 0x8000

/* Destination flow cache sorted list node.  Each node maps a remote MessageQ
 * queue ID to a CPPI rx flow ID.  Sending to the a rx flow ID via QMSS will
 * allow the destination to route the sent MessageQ message to the associated
 * MessageQ queue ID */
typedef struct {
    /* MessageQ queue ID */
    uint32_t queue_id;
    /* CPPI rx flow ID */
    uint16_t flow;
} fcache_node_t;

/* Data structure that tracks the usage of the flow cache sorted list */
typedef struct {
    /* Total number of flow cache nodes pointed to by node_list */
    uint32_t       total_nodes;
    /* Number of flow cache nodes in use out of total pointed to by node_list */
    uint32_t       used_nodes;
    /* Pointer to an array of flow cache nodes sorted by queue_id.  The array
     * is of size total_nodes */
    fcache_node_t *node_list;
} fcache_list_t;

/*
 *  ======== Params Structure Evolution ========
 */
 
/* Initial version of TransportQmss_Params defined in TransportQmss.h.  Please
 * see TransportQmss.h for parameter element definitions */
typedef struct {
    Int   __version;
    Char *mpm_trans_inst_name;
    Void *rm_service_h;
    Int   rx_msg_size_bytes;
    Int   mpm_trans_init_qmss;
} TransportQmss_Params_Version1;

/*
 *  ======== TransportQmss_Object ========
 *  TransportQmss instance object definition
 */
typedef struct {
    /* TransportQmss is a INetworkTransport so must inherit the
     * INetworkTransport object */
    INetworkTransport_Object  base;
    /* Pointer to QMSS MPM Transport instance */
    void                     *mpm_transport_handle;
    /* Pointer to RM instance service handle needed for RM NameServer usage */
    Rm_ServiceHandle         *rm_service_h;
    /* Maximum size, in bytes , of expected receive data packets retrieved from
     * MPM-Transport */
    int                       rx_msg_size_bytes;
    /* CPPI receive flow ID opened for this transport instance.  Will be mapped
     * to all MessageQ queue IDs opened within the same process context as the
     * TransportQmss instance */
    int32_t                   rx_flow_id;
    /* Thread handle for the transportQmssRcv data reception thread */
    pthread_t                 rx_thread_h;
    /* Flow cache */
    fcache_list_t             fcache_list;
    /* Tracks the last allocated MessageQ message in the transportQmssRcv
     * thread.  The thread can be cancelled before the receive message has been
     * passed to the application or properly freed within the context of the
     * thread.  The main process will free the message within the cleanup logic
     * if the latter occurs */
    MessageQ_Msg              allocd_rcv_msg;
} TransportQmss_Object;

/*
 *  ======== Instance Function Declarations ========
 */
Int TransportQmss_bind(Void *handle, UInt32 queueId);
Int TransportQmss_unbind(Void *handle, UInt32 queueId);
Bool TransportQmss_put(Void *handle, Ptr msg);

/*
 *  ======== TransportQmss_Fxns ========
 *  The instance function table.  Functions are not part of external API.
 *  instead they are called under the hood of IPC MessageQ:
 * TransportQmss_bind   --> Called within MessageQ_create()
 * TransportQmss_unbind --> Called within MessageQ_delete()
 * TransportQmss_put    --> Called within MessageQ_put()
 */
INetworkTransport_Fxns TransportQmss_Fxns = {
    TransportQmss_bind,
    TransportQmss_unbind,
    TransportQmss_put
};


/* FUNCTION PURPOSE: Utility to feed a command to RM
 ***********************************************************************
 * DESCRIPTION: Formulates a RM service request and returns the response
 *              data value and resultant state
 */
static int32_t rm_cmd(Rm_ServiceHandle *service_h, const char *name,
                      int32_t *val, Rm_ServiceType type, int retry)
{
    Rm_ServiceReqInfo  rmServiceReq;
    Rm_ServiceRespInfo rmServiceResp;
    int                succeed;

    memset((void *)&rmServiceReq, 0, sizeof(rmServiceReq));
    memset((void *)&rmServiceResp, 0, sizeof(rmServiceResp));
    
    rmServiceReq.type           = type;
    rmServiceReq.resourceName   = name;
    rmServiceReq.resourceNsName = name;
    rmServiceReq.resourceLength = 1;
    if (val) {
        rmServiceReq.resourceBase = *val;
    }

    /* RM will block until resource is returned since callback is NULL */
    rmServiceReq.callback.serviceCallback = NULL;
    do {
        service_h->Rm_serviceHandler(service_h->rmHandle, &rmServiceReq,
                                     &rmServiceResp);
        succeed = (rmServiceResp.serviceState == RM_SERVICE_APPROVED) ||
                  (rmServiceResp.serviceState == RM_SERVICE_APPROVED_STATIC);
        if (retry && (!succeed)) {
            sleep(1);
        }
    } while (retry && (!succeed));

    if (succeed) {
        if ((type == Rm_service_RESOURCE_GET_BY_NAME) && (val)) {
            *val = rmServiceResp.resourceBase;
        }
    } else {
        if (rmServiceResp.serviceState < RM_ERROR_BASE) {
            fprintf(stderr, "RM transaction failed %d %s %d (%d)\n", type, name,
                    val ? *val : 0, rmServiceResp.serviceState);
        } else {
            fprintf(stderr, "RM transaction denied %d %s %d (%d)\n", type, name,
                    val ? *val : 0, rmServiceResp.serviceState);
        }
    }

    return(rmServiceResp.serviceState);
}

/* FUNCTION PURPOSE: Utility to lookup a name in RM NameServer
 ***********************************************************************
 * DESCRIPTION: Sends a name lookup request to RM NameServer.  Value
 *              associated with the name is returned through the val pointer
 *              if the name exists.  The service request state (pass, deny, or
 *              fail) is returned.
 */
static int32_t rm_name_lookup(Rm_ServiceHandle *service_h, const char *name,
                              int32_t *val)
{
    *val = RM_RESOURCE_BASE_UNSPECIFIED;
    return(rm_cmd(service_h, name, val, Rm_service_RESOURCE_GET_BY_NAME, 1));
}

/* FUNCTION PURPOSE: Utility to set a name in RM NameServer
 ***********************************************************************
 * DESCRIPTION: Sends a name set request to RM NameServer.  Value to be
 *              associated with the name is passed through the val pointer.
 *              The service request state (pass, deny, or fail) is returned.
 */
static int32_t rm_name_set(Rm_ServiceHandle *service_h, const char *name,
                           int32_t *val)
{
    return(rm_cmd(service_h, name, val, Rm_service_RESOURCE_MAP_TO_NAME, 0));
}

/* FUNCTION PURPOSE: Utility to delete a name from the RM NameServer
 ***********************************************************************
 * DESCRIPTION: Sends a name delete request to RM NameServer.  The service
 *              request state (pass, deny, or fail) is returned.
 */
static int32_t rm_name_del(Rm_ServiceHandle *service_h, const char *name)
{
    return(rm_cmd(service_h, name, NULL, Rm_service_RESOURCE_UNMAP_NAME, 0));
}

/* FUNCTION PURPOSE: Utility to lookup CPPI flow ID in the flow cache
 ***********************************************************************
 * DESCRIPTION: Looks up a CPPI flow ID based on an associated MessageQ
 *              queue ID.  Assumes the flow cache list is a sorted list
 *              with the sort key being the MessageQ queue ID.
 *              UNCACHED_FLOW_ID will be returned for a failed lookup
 */
static int32_t fcache_list_lookup(fcache_list_t *list, uint32_t queue_id)
{
    uint16_t min = 0;
    uint16_t max = list->used_nodes;
    uint16_t mid;

    while (max >= min) {
        mid = min + ((max - min) / 2);
        if (list->node_list[mid].queue_id == queue_id) {
            return ((int32_t) list->node_list[mid].flow);
        } else if (list->node_list[mid].queue_id < queue_id) {
            min = mid + 1;
        } else {
            max = mid - 1;
        }
    }
    return (UNCACHED_FLOW_ID);
}

/* FUNCTION PURPOSE: Utility to add a CPPI flow ID to the flow cache
 ***********************************************************************
 * DESCRIPTION: Adds a MessageQ queue ID to CPPI rx flow ID mapping to
 *              the flow cache sorted list.  New cache entry will be
 *              slotted into the flow cache based on the MessageQ queue ID.
 *              Total nodes will be expanded by a power of 2 if 
 *              used_nodes = total_nodes prior to adding the new entry
 */
static void fcache_list_add(fcache_list_t *list, uint32_t queue_id,
                            uint16_t flow)
{
    fcache_node_t *tmp_list;
    uint32_t       i;
    
    if (list->total_nodes == list->used_nodes) {
        /* Need to expand list by power of 2 */
        tmp_list = list->node_list;
        list->total_nodes *= 2;
        list->node_list = calloc(list->total_nodes, sizeof(*(list->node_list)));
        for (i = 0; i < list->used_nodes; i++) {
            list->node_list[i] = tmp_list[i];
        }
        free(tmp_list);
    }

    for (i = list->used_nodes;
         (i > 0) && (queue_id < list->node_list[i - 1].queue_id);
         i--) {
        list->node_list[i].queue_id = list->node_list[i - 1].queue_id;
        list->node_list[i].flow= list->node_list[i - 1].flow;
    }
    list->node_list[i].queue_id = queue_id;
    list->node_list[i].flow = flow;
    list->used_nodes++;
}

/* FUNCTION PURPOSE: Utility to remove a CPPI flow ID from the flow cache
 ***********************************************************************
 * DESCRIPTION: Deletes a MessageQ queue ID to CPPI rx flow ID mapping from
 *              the flow cache sorted list.  Total nodes will be contracted by
 *              a power of 2 if used_nodes = (total_nodes/2) post entry removal
 */
static void fcache_list_del(fcache_list_t *list, uint32_t queue_id)
{
    fcache_node_t *tmp_list;
    uint32_t       i;

    for (i = 0; i < (list->used_nodes - 1); i++) {
        if (list->node_list[i].queue_id >= queue_id) {
            list->node_list[i].queue_id = list->node_list[i+1].queue_id;
            list->node_list[i].flow = list->node_list[i+1].flow;
        }
    }
    list->node_list[i].queue_id = 0;
    list->node_list[i].flow = 0;
    list->used_nodes--;

    if (list->used_nodes == (list->total_nodes / 2)) {
        /* Need to shrink list by power of 2 */
        tmp_list = list->node_list;
        list->total_nodes /= 2;
        list->node_list = calloc(list->total_nodes,
                                 sizeof(*(list->node_list)));
        for (i = 0; i < list->used_nodes; i++) {
            list->node_list[i].queue_id = tmp_list[i].queue_id;
            list->node_list[i].flow = tmp_list[i].flow;
        }
        free(tmp_list);
    }
}

/* FUNCTION PURPOSE: Utility to create a thread
 ***********************************************************************
 * DESCRIPTION: Creates a thread that runs thread_routine with input
 *              arguments *args.
 */
static int thread_create(void *(thread_routine)(void *), void *args,
                         void *handle)
{
    int                max_priority, err;
    pthread_t          thread;
    pthread_attr_t     attr;
    struct sched_param param;

    max_priority = sched_get_priority_max(SCHED_FIFO);
    err = pthread_attr_init(&attr);
    if (err) {
        fprintf(stderr, "pthread_attr_init failed: (%s)\n", strerror(err));
        return(err);
    }
    err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    if (err) {
        fprintf(stderr, "pthread_attr_setdetachstate failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setstacksize(&attr, RX_THREAD_DEFAULT_STACK_SIZE);
    if (err) {
        fprintf(stderr, "pthread_attr_setstacksize failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (err) {
        fprintf(stderr, "pthread_attr_setinheritsched failed: (%s)\n",
                strerror(err));
        return(err);
    }
    err = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (err) {
        fprintf(stderr, "pthread_attr_setschedpolicy failed: (%s)\n",
                strerror(err));
        return(err);
    }
    memset(&param, 0, sizeof(param));
    param.sched_priority = max_priority;
    err = pthread_attr_setschedparam(&attr, &param);
    if (err) {
        fprintf(stderr, "pthread_attr_setschedparam failed: (%s)\n",
                strerror(err));
        return(err);
    }
    if (err) return err;
    err = pthread_create(&thread, &attr, thread_routine, args);
    if (err) {
        fprintf(stderr, "pthread_create failed: (%s)\n", strerror(err));
        return(err);
    }
    if (err)
        return(err);
    
    *(pthread_t *)handle = thread;
    return(0);
}

/* FUNCTION PURPOSE: Deletes an executing thread
 ***********************************************************************
 * DESCRIPTION: Deletes the thread pointed to by handle by cancelling it.
 *              The thread will be cancelled at the next cancellation point.
 *              This function will block on pthread_join until the thread
 *              completes the cancellation request
 */
static void *thread_delete(void *handle)
{
    void *ret_val = NULL;
    
    pthread_cancel((pthread_t) handle);
    /* Wait for thread to cancel */
    pthread_join((pthread_t) handle, &ret_val);
    if (ret_val != PTHREAD_CANCELED) {
        fprintf(stderr, "pthread_join did not return PTHREAD_CANCELED when"
                        "after cancelling thread\n");
    }
    return(ret_val);
}

/* FUNCTION PURPOSE: Cleans up objects created for TransportQmss object
 ***********************************************************************
 * DESCRIPTION: Cleans up any objects or allocated data structures associated
 *              with the TransportQmss object.
 */
static void transportQmssCleanup(TransportQmss_Object *obj)
{
    if (obj->fcache_list.node_list)
        free(obj->fcache_list.node_list);

    if (obj->mpm_transport_handle)
        mpm_transport_close(obj->mpm_transport_handle);

    if (obj->allocd_rcv_msg)
        MessageQ_free(obj->allocd_rcv_msg);
}

/* FUNCTION PURPOSE: MessageQ message receive thread
 ***********************************************************************
 * DESCRIPTION: Receives incoming MessageQ messages via MPM-Transport
 *              Calls to mpm_transport_packet_recv will block until data
 *              is received.
 *
 *              Input args pointer is the TransportQmss_Object pointer passed 
 *              through the pthread_create function when creating the receive 
 *              thread.
 *
 *              Thread can be sent a cancellation request at any time but will
 *              not officially cancel until a cancellation point is reached.
 *              The alloc_rcv_msg object variable tracks the currently
 *              allocated receive MessageQ message.  The main process cancelling
 *              thread will free allocd_rcv_msg if a cancellation occurs before
 *              the allocd_rcv_msg is passed to the application or freed
 */
static void *transportQmssRcv(void *args)
{
    TransportQmss_Object *obj = (TransportQmss_Object *) args;

    while (1) {
        MessageQ_Msg         rx_msg;
        int                  rx_msg_bytes = obj->rx_msg_size_bytes;
        mpm_transport_recv_t rx_cfg;
        int                  drop_msg = 0;
        int                  status;

        /* Allocate local MessageQ message for eventual memcpy of received data.
         * Needs to be done at the moment since MessageQ heap has no way to 
         * register an application-specific heap */
        rx_msg = MessageQ_alloc(0, obj->rx_msg_size_bytes);
        /* Store pointer to allocated msg in obj in case thread is cancelled
         * prior to full reception */
        obj->allocd_rcv_msg = rx_msg;

        memset(&rx_cfg, 0, sizeof(rx_cfg));
        /* Tell MPM to block until packet received */
        rx_cfg.wait_opt = packet_recv_BLOCKING_INTERRUPT;
        /* MPM-Transport will memcpy recieved data into rx_msg */
        status = mpm_transport_packet_recv(obj->mpm_transport_handle,
                                           (char **) &rx_msg, &rx_msg_bytes,
                                           NULL, &rx_cfg);
        if (status < 0) {
            fprintf(stderr, "transportQmssRcv : "
                            "mpm_transport_packet_recv err: %d\n", status);
            MessageQ_free(rx_msg);
        } else {
            /* Reset heapId and msgSize */
            rx_msg->heapId = 0;
            rx_msg->msgSize = obj->rx_msg_size_bytes;

            if (MultiProc_self() != rx_msg->dstProc) {
                fprintf(stderr, "transportQmssRcv: Destination processor %d\n"
                                "                  Local processor       %d\n"
                                "DROPPING MESSAGE\n",
                                rx_msg->dstProc, MultiProc_self());
                drop_msg = 1;
                goto drop;
            }

drop:
            if (drop_msg)
                MessageQ_free(rx_msg);
            else
                MessageQ_put(MessageQ_getDstQueue(rx_msg), rx_msg);
        }
        /* Clear since msg was either freed or given to MessageQ_put */
        obj->allocd_rcv_msg = NULL;
    }

    return(NULL);
}

/* FUNCTION PURPOSE: Initializes the TransportQmss creation parameters
 ***********************************************************************
 * DESCRIPTION: Initializes the TransportQmss creation parameters based on
 *              parameter structure version.
 */
Void TransportQmss_Params_init__S(TransportQmss_Params *params, Int version)
{
    TransportQmss_Params_Version1 *params1;

    if (params == NULL) {
        fprintf(stderr, "TransportQmss_Params_init : "
                        "Params structure is NULL\n");
        assert(FALSE);
        return;
    }

    switch (version) {
        case TransportQmss_Params_VERSION_1:
            params1 = (TransportQmss_Params_Version1 *)params;
            params1->__version           = TransportQmss_Params_VERSION_1;
            params1->mpm_trans_inst_name = NULL;
            params1->rm_service_h        = NULL;
            params1->rx_msg_size_bytes   = 0;
            params1->mpm_trans_init_qmss = 0;
            break;
        default:
            fprintf(stderr, "TransportQmss_Params_init : "
                            "Invalid Params version\n");
            assert(FALSE);
            break;
    }
}

/* FUNCTION PURPOSE: Creates a TransportQmss object
 ***********************************************************************
 * DESCRIPTION: Creates an instance of TransportQmss.  A MPM-Transport
 *              instance will be opened configured for QMSS.
 *
 *              The transportQmssRcv thread will be created as part of this
 *              routine.  A separate thread is created for each TransportQmss
 *              instance.
 */
TransportQmss_Handle TransportQmss_create(TransportQmss_Params *pp)
{
    TransportQmss_Params_Version1 *pp1;
    TransportQmss_Params           params;
    TransportQmss_Object          *obj = NULL;
    mpm_transport_open_t           cfg;

    if (pp == NULL) {
        fprintf(stderr, "TransportQmss_create : Params structure cannot be "
                        "NULL\n");
        goto err_exit;
    }
    
    /* initialize local params structure */
    TransportQmss_Params_init(&params);

    /* initialize local params using caller's given params */
    switch (pp->__version) {
        case TransportQmss_Params_VERSION_1:
            pp1 = (TransportQmss_Params_Version1 *)pp;
            params.mpm_trans_inst_name = pp1->mpm_trans_inst_name;
            params.rm_service_h        = pp1->rm_service_h;
            params.rx_msg_size_bytes   = pp1->rx_msg_size_bytes;
            params.mpm_trans_init_qmss = pp1->mpm_trans_init_qmss;
            break;
        default:
            fprintf(stderr, "TransportQmss_create : Invalid Params version\n");
            goto err_exit;
    }

    /* Error checking */
    if (strlen(params.mpm_trans_inst_name) == 0) {
        fprintf(stderr, "TransportQmss_create : mpm_trans_inst_name "
                        "cannot be NULL\n");
        goto err_exit;
    }
    if (params.rx_msg_size_bytes == 0) {
        fprintf(stderr, "TransportQmss_create : rx_msg_size_bytes must be "
                        "greater than zero\n");
        goto err_exit;
    }
    if (params.rm_service_h == NULL) {
        fprintf(stderr, "TransportQmss_create : rm_service_h cannot be NULL\n");
        goto err_exit;
    }

    /* allocate the instance object */
    obj = calloc(1, sizeof(TransportQmss_Object));

    /* initialize the inherited interface */
    obj->base.base.interfaceType = INetworkTransport_TypeId;
    obj->base.fxns               = &TransportQmss_Fxns;
    obj->rm_service_h            = params.rm_service_h;
    obj->rx_msg_size_bytes       = params.rx_msg_size_bytes;
    obj->allocd_rcv_msg          = NULL;

    /* Open MPM transport instance for QMSS */
    memset(&cfg, 0, sizeof(cfg));
    cfg.open_mode                     = (O_SYNC|O_RDWR);
    cfg.rm_info.rm_client_handle      = obj->rm_service_h;
    if (params.mpm_trans_init_qmss) {
        cfg.transport_info.qmss.qmss_init = 1;
    }

    obj->mpm_transport_handle = mpm_transport_open(params.mpm_trans_inst_name,
                                                   &cfg);
    if (obj->mpm_transport_handle == NULL) {
        fprintf(stderr, "TransportQmss_create : mpm_transport_open failed\n");
        goto err_exit;
    }

    /* Save Navigator hardware resources allocated by MPM that are needed by
     * TransportQmss in order to manage send/bind/receive operations */
    obj->rx_flow_id = cfg.transport_info.qmss.flowId;
    /* Initialize destination flow cache */
    obj->fcache_list.total_nodes = FLOW_CACHE_INIT_SIZE;
    obj->fcache_list.used_nodes = 0;
    obj->fcache_list.node_list = calloc(FLOW_CACHE_INIT_SIZE,
                                        sizeof(*(obj->fcache_list.node_list)));

    /* Create receive thread that will monitor the FD tied to the transport's
     * QMSS receive queue interrupt */
    if (thread_create(transportQmssRcv, (void *)obj,
                      &(obj->rx_thread_h))) {
        /* Error creating receive thread */
        fprintf(stderr, "TransportQmss_create : "
                        "Failed to create receive thread\n");
        goto err_exit;
    }

    return((TransportQmss_Handle)obj);
    
err_exit:
    /* Cleanup */
    if (obj) {
        transportQmssCleanup(obj);
        free(obj);
        obj = NULL;
    }    
    return((TransportQmss_Handle)obj);
}

/* FUNCTION PURPOSE: Deletes a TransportQmss object
 ***********************************************************************
 * DESCRIPTION: Deletes the specified TransportQmss object.  The receive thread
 *              associated to the object will be cancelled and the object will
 *              be set to NULL upon return.
 */
Void TransportQmss_delete(TransportQmss_Handle *hp)
{
    TransportQmss_Object *obj;

    obj = *(TransportQmss_Object **)hp;

    /* Cancel receive thread */
    thread_delete((void *)obj->rx_thread_h);
    /* Cleanup transport specific parameters */
    transportQmssCleanup(obj);
    free(obj);
    *hp = NULL;
}

/* FUNCTION PURPOSE: TransportQmss_Handle upcast to INetworkTransport_Handle
 ***********************************************************************
 * DESCRIPTION: Upcasts a TransportQmss_Handle to its inherited
 *              INetworkTransport_Handle
 */
INetworkTransport_Handle TransportQmss_upCast(TransportQmss_Handle inst)
{
    return((INetworkTransport_Handle)inst);
}

/* FUNCTION PURPOSE: INetworkTransport_Handle downcast to TransportQmss_Handle
 ***********************************************************************
 * DESCRIPTION: downcasts an INetworkTransport_Handle to the 
 *              TransportQmss_Handle that inheritied it
 */
TransportQmss_Handle TransportQmss_downCast(INetworkTransport_Handle base)
{
    return((TransportQmss_Handle)base);
}

/* FUNCTION PURPOSE: Binds a created MessageQ queue ID
 ***********************************************************************
 * DESCRIPTION: Registers a new MessageQ queue ID, created within the same
 *              context as the TransportQmss object, with the transport's
 *              flow ID discovery mechanism.
 *
 *              Each MessageQ queue ID will be associated to the rx flow ID of
 *              the TransportQmss object created within the same system context.
 *              The association is stored in the RM NameServer.
 *              TransportQmss will query the RM NameServer on send operations
 *              for the flow ID associated with the destination flow ID.
 */
Int TransportQmss_bind(void *handle, UInt32 queueId)
{
    TransportQmss_Object *obj;
    Int                   status = 0;
    char                  flow_name[RM_NAME_MAX_CHARS];

    obj = (TransportQmss_Object *)handle;

    /* Map rx_flow_id to a unique string name containing local MultiProc ID and
     * MessageQ queue index.  Mapping stored in NameServer */
    snprintf(flow_name, sizeof(flow_name), "%d", queueId);
    rm_name_set(obj->rm_service_h, flow_name, &(obj->rx_flow_id));

    return(status);
}

/* FUNCTION PURPOSE: Unbinds a deleted MessageQ queue ID
 ***********************************************************************
 * DESCRIPTION: Unbinds a MessageQ queue ID, deleted within the same context
 *              as the TransportQmss object, by removing the associated
 *              MessageQ queue ID to CPPI rx flow ID mapping from the RM
 *              NameServer.
 */
Int TransportQmss_unbind(void *handle, UInt32 queueId)
{
    TransportQmss_Object *obj;
    Int                   status = 0;
    char                  flow_name[RM_NAME_MAX_CHARS];

    obj = (TransportQmss_Object *)handle;

    /* Removing mapping of rx_flow_id to string stored in RM NameServer */
    snprintf(flow_name, sizeof(flow_name), "%d", queueId);
    rm_name_del(obj->rm_service_h, flow_name);

    return (status);
}

/* FUNCTION PURPOSE: Sends a MessageQ message over TransportQmss
 ***********************************************************************
 * DESCRIPTION: Sends a MessageQ message by querying the RM NameServer for
 *              CPPI rx flow ID associated with the destination MessageQ queue
 *              ID and then providing that flow ID and the message to 
 *              MPM-Transport for the send operation.
 */
Bool TransportQmss_put(void *handle, Ptr msg)
{
    TransportQmss_Object        *obj = (TransportQmss_Object *)handle;
    Bool                         status = TRUE;
    mpm_transport_packet_addr_t  send_addr;
    int32_t                      flow_id = UNCACHED_FLOW_ID;
    MessageQ_Msg                 msg_q_msg = (MessageQ_Msg) msg;
    int                          msg_size = MessageQ_getMsgSize(msg_q_msg);
    uint32_t                     dst_q_id;
    char                         flow_name[RM_NAME_MAX_CHARS];
    char                        *send_msg;
    mpm_transport_send_t         send_cfg;
    int                          mpm_status;

    dst_q_id = ((uint32_t)(msg_q_msg->dstProc << 16)) |
               ((uint32_t)(msg_q_msg->dstId));

    if (obj->fcache_list.used_nodes) {
        flow_id = fcache_list_lookup(&(obj->fcache_list), dst_q_id);
    }
    if (flow_id == UNCACHED_FLOW_ID) {
        snprintf(flow_name, sizeof(flow_name), "%d", dst_q_id);
        rm_name_lookup(obj->rm_service_h, flow_name, &flow_id);
        if (flow_id == RM_RESOURCE_BASE_UNSPECIFIED) {
            /* Destination processor has not bound a rx flow to the destination
             * MessageQ queue ID via RM */
            status = FALSE;
            fprintf(stderr, "TransportQmss_put : "
                            "Destination MsgQ Id not bound rx flow\n");
            goto err_exit;
        }
        fcache_list_add(&(obj->fcache_list), dst_q_id, flow_id);
    }

    memset(&send_addr, 0, sizeof(send_addr));
    send_addr.addr_type = packet_addr_type_QMSS;
    send_addr.addr.qmss.flowId = flow_id;

    memset(&send_cfg, 0, sizeof(send_cfg));
    /* MPM-Transport configured to memcpy buffer since MessageQ doesn't provide
     * means for registering an application-specific heap */
    send_cfg.buf_opt = packet_send_MEMCPY_BUFFER;
    send_msg = (char *)msg_q_msg;

    if ((mpm_status = mpm_transport_packet_send(obj->mpm_transport_handle,
                                               (char **)&send_msg,
                                               &msg_size, &send_addr,
                                               &send_cfg))) {
        status = FALSE;
        fprintf(stderr, "TransportQmss_put : "
                        "MPM-Transport packet send returned error %d\n",
                        mpm_status);
    }

err_exit:
    MessageQ_free(msg_q_msg);

    return (status);
}

/* FUNCTION PURPOSE: Clears the TransportQmss instance's destination cache
 ***********************************************************************
 * DESCRIPTION: Flush the destination address cache stored within the
 *              transport instance.  An entry is inserted into the cache
 *              for each new destination the transport must send to.  The
 *              cache will need to be flushed if a destination is transport
 *              instance is closed.  Currently, there's no backpath for
 *              informing of instance closures.  As a result, the
 *              destination cache must be flushed manually.
 *
 *              The cache can be flushed of a single entry by providing a
 *              destination MessageQ queue ID that was previously sent to but
 *              has since been closed.
 *
 *              The entire destination cache will be flushed if
 *              MessageQ_INVALIDMESSAGEQ is provided for the MessageQ queue ID
 */
Bool TransportQmss_flushDstCache(TransportQmss_Handle handle, UInt32 queueId)
{
    TransportQmss_Object *obj = (TransportQmss_Object *)handle;
    Bool                  status = TRUE;

    if (queueId == MessageQ_INVALIDMESSAGEQ) {
        /* Delete dst flow cache */
        if (obj->fcache_list.node_list)
            free(obj->fcache_list.node_list);

        /* Re-init destination flow cache */
        obj->fcache_list.total_nodes = FLOW_CACHE_INIT_SIZE;
        obj->fcache_list.used_nodes  = 0;
        obj->fcache_list.node_list   = calloc(FLOW_CACHE_INIT_SIZE,
                                         sizeof(*(obj->fcache_list.node_list)));
    } else {
        if (fcache_list_lookup(&(obj->fcache_list), queueId) ==
            UNCACHED_FLOW_ID) {
            status = FALSE;
        } else {
            fcache_list_del(&(obj->fcache_list), queueId);
        }
    }

    return(status);
}

/* FUNCTION PURPOSE: Returns Linux IPC Transport version information
 ***********************************************************************
 */
UInt32 TransportQmss_getVersion(Void)
{
    return TRANSPORT_VERSION_ID;
}

/* FUNCTION PURPOSE: Returns Linux IPC Transport version string
 ***********************************************************************
 */
const Char *TransportQmss_getVersionStr(Void)
{
    return transportQmssVersionStr;
}

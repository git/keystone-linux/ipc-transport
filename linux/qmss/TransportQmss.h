/**
 *   @file  TransportQmss.h
 *
 *   @brief
 *      ARMv7 Linux MessageQ TransportQmss public API
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
 */

/*
 *  ======== TransportQmss.h ========
 */

#ifndef TRANSPORTQMSS_H_
#define TRANSPORTQMSS_H_

#include <ti/ipc/interfaces/INetworkTransport.h>

/**  @mainpage ARMv7 Linux TransportQmss
 *
 *   @section intro  Introduction
 *
 *   TransportQmss is a Navigator-based transport for ARMv7 Linux IPC
 *   MessageQ.  TransportQmss can be registered with MessageQ as an
 *   INetworkTransport.  Once registered, TransportQmss allows MessageQ-based
 *   communication between Linux processes.
 */

/*
 *  ======== TransportQmss_Params_VERSION_1 ========
 *  TransportQmss ver 1.0.0.0
 *
 *  Initial implementation of the params structure.
 */
#define TransportQmss_Params_VERSION_1 1

/*
 *  ======== TransportQmss_Params_VERSION ========
 *  Defines the current params structure version. Internal use only.
 */
#define TransportQmss_Params_VERSION   TransportQmss_Params_VERSION_1

/*
 *  ======== TransportQmss_Handle ========
 **
 *  @brief Opaque handle to TransportQmss instance object.
 */
typedef struct TransportQmss_Object *TransportQmss_Handle;

/*
 *  ======== TransportQmss_Params ========
 **
 *  @brief TransportQmss initialization parameters
 */
typedef struct {
    /* Tracks the implementation evolution.  Internal use only. */
    Int   __version;
    /** MPM-Transport instance name.  This string must match a "slaves" name
     *  string defined within the mpm_config.json file used in the Linux
     *  filesystem */
    Char *mpm_trans_inst_name;
    /** RM instance service handle needed by MPM-Transport to request hardware
     *  resources */
    Void *rm_service_h;
    /** Maximum size in bytes of messages that will be received by this
     *  transport.  Used to allocated MessageQ messages for reception */
    Int   rx_msg_size_bytes;
    /** Controls whether MPM-Transport will initialize the QMSS hardware upon
     *  opening.
     *  0 - MPM-Transport will not initialize QMSS.  Set if another entity
     *      within the system initialized the QMSS hardware.
     *  1 - MPM-Transport will initialize QMSS.  Set if this is the first
     *      system entity being created that used QMSS and QMSS has not been
     *      initialized */
    Int   mpm_trans_init_qmss;
} TransportQmss_Params;

/*
 *  ======== TransportQmss_Params_init__S ========
 *  Internal system function, not to be called by the user.
 */
Void TransportQmss_Params_init__S(TransportQmss_Params *params, Int version);

/*
 *  ======== TransportQmss_Params_init ========
 *  Using a static inline method binds the parameter structure version
 *  at the calls site. This allows new library implementations to recognize
 *  old call sites and thus to dereference the parameter structure correctly.
 **
 *  @b Description
 *  @n  
 *      Initializes the TransportQmss initialization parameter structure
 *      to its default values.
 *
 *  @param[in]  params
 *      Pointer to TransportQmss initialization parameter structure
 */
static inline Void TransportQmss_Params_init(TransportQmss_Params *params)
{
    if (params != NULL) {
        TransportQmss_Params_init__S(params, TransportQmss_Params_VERSION);
    }
}

/*
 *  ======== TransportQmss_create ========
 **
 *  @b Description
 *  @n  
 *      Creates an instance of TransportQmss.  The returned transport handle
 *      must be registered with MessageQ using the 
 *      MessageQ_registerTransportId API before the transport can be used
 *      to route MessageQ messages.
 *
 *  @param[in]  params
 *      Pointer to the TransportQmss initialization structure
 *
 *  @retval
 *      Success - Non-zero TransportQmss_Handle
 *  @retval
 *      Failure - NULL TransportQmss_Handle
 */
TransportQmss_Handle TransportQmss_create(TransportQmss_Params *params);

/*
 *  ======== TransportQmss_delete ========
 **
 *  @b Description
 *  @n  
 *      Finalizes a TransportQmss instance.  The TransportQmss_Handle will
 *      be made NULL upon return.
 *
 *  @param[in]  hp
 *      Pointer to the TransportQmss_Handle to be deleted
 *  @param[out] hp
 *       Dereferenced TransportQmss_Handle value will be made NULL upon 
 *       successful finalization
 */
Void TransportQmss_delete(TransportQmss_Handle *hp);

/*
 *  ======== TransportQmss_upCast ========
 **
 *  @b Description
 *  @n  
 *      Converts a TransportQmss instance handle to its inherited
 *      INetworkTransport_Handle
 *
 *  @param[in]  TransportQmss_Handle
 *      TransportQmss_handle
 *
 *  @retval
 *      INetworkTransport_Handle stored within the TransportQmss_Handle
 */
INetworkTransport_Handle TransportQmss_upCast(TransportQmss_Handle inst);

/*
 *  ======== TransportQmss_downCast ========
 **
 *  @b Description
 *  @n  
 *      Converts an INetworkTransport_Handle instance handle to its
 *      corresponding TransportQmss_Handle.
 *
 *      It is the caller's responsibility to ensure the underlying object
 *      is of the correct type.
 *
 *  @param[in]  INetworkTransport_Handle corresponding to a
 *      TransportQmss_handle
 *
 *  @retval
 *      Opaque TransportQmss_Handle
 */
TransportQmss_Handle TransportQmss_downCast(INetworkTransport_Handle base);

/*
 *  ======== TransportQmss_delete ========
 **
 *  @b Description
 *  @n
 *      Flush the destination address cache stored within the
 *      transport instance.  An entry is inserted into the cache
 *      for each new destination the transport must send to.  The
 *      cache must be flushed if a destination transport
 *      instance is closed.  Currently, there's no backpath for
 *      informing of instance closures.  As a result, the destination
 *      cache must be flushed manually.
 *
 *  @param[in]  handle
 *      TransportQmss_Handle
 *  @param[in]  queueId
 *      Destination MessageQ queue ID to be flushed from the cache.  The
 *      entire cache will be flushed if MessageQ_INVALIDMESSAGEQ is provided
 *      for the queue ID
 *
 *  @retval
 *      Success - TRUE: Flush (entry or entire cache) succeeded
 *  @retval
 *      Failure - FALSE: Flush failed
 */
Bool TransportQmss_flushDstCache(TransportQmss_Handle handle, UInt32 queueId);

/*
 *  ======== TransportQmss_getVersion ========
 **
 *  @b Description
 *  @n  
 *      The function is used to get the version information of TransportQmss.
 *
 *  @retval
 *      Version Information.
 */
UInt32 TransportQmss_getVersion(Void);

/*
 *  ======== TransportQmss_getVersionStr ========
 **
 *  @b Description
 *  @n  
 *      The function is used to get the version string for TransportQmss.
 *
 *  @retval
 *      Version String.
 */
const Char *TransportQmss_getVersionStr(Void);

#endif /* TRANSPORTQMSS_H_ */

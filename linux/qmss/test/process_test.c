/*
 *   process_test.c
 *
 *   Multi-process TransportQmss test that sends messages between
 *   Linux processes using IPC MessageQ.  The messages are transmitted
 *   between the processes by way of the TransportQmss transport.
 *
 *   A total of MAX_NUM_PROCESSES are created.  Each process creates a MessageQ
 *   queue and then opens the MessageQ queue created by the next logical
 *   process.  The last process will open the first process's MessageQ queue.
 *   An instance of TransportQmss is created on each process and registered with
 *   MessageQ.  A MessageQ message is allocated and sent from process to 
 *   process, round-robin, using the MessageQ APIs which, in turn, use the
 *   registered TransportQmss as specified in the MessageQ message header.
 *   Each time a process sends the message it populates the data buffer in the
 *   message with unique data which is verified by the receiving process upon
 *   reception.  The number of round-trip sends that take place is specified by
 *   TEST_ITERATIONS.
 *
 *  ============================================================================
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Shut off: remark #880-D: parameter "appTransport" was never referenced
 *
 * This is better than removing the argument since removal would break
 * backwards compatibility
 */
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
 * warning: unused parameter �appTransport, argc, and argv� [-Wunused-parameter]
 */
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/* Standard includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>
#include <ti/ipc/transports/TransportQmss.h>

/* RM includes */
#include <ti/drv/rm/rm_server_if.h>
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>

/* Socket utility includes */
#include "sockutils.h"

/* Number of processes to test */
#define MAX_NUM_PROCESSES          4

#define TEST_PASS                  0
#define TEST_FAIL                  1

#define MPM_INST_NAME_LEN          20
#define MSGQ_Q_NAME_LEN            32

/* Number of round trips packet should travel */
#define TEST_ITERATIONS            100

#define MAX_PACKET_SIZE            2048

#define TRANS_QMSS_NET_ID          1

/* Application's registered RM transport indices */
#define SERVER_TO_CLIENT_MAP_ENTRY 0
/* Maximum number of registered RM transports */
#define MAX_MAPPING_ENTRIES        1

#define FILL_DATA_BYTE(proc, idx) (uint8_t)(((proc & 0xF) << 4) | (idx & 0xF))

typedef struct test_msg_s {
    MessageQ_MsgHeader header; /* 32 bytes */
    uint8_t            data[MAX_PACKET_SIZE - sizeof(MessageQ_MsgHeader)];
} test_msg_t;

typedef struct trans_map_entry_s {
    Rm_TransportHandle  trans_handle;
    sock_name_t        *remote_sock;
} trans_map_entry_t;

int                local_process;
int                next_process;
int                prev_process;

Rm_Handle          rm_client_handle = NULL;
Rm_ServiceHandle  *rm_client_service_handle = NULL;
sock_h             rm_client_sock;

trans_map_entry_t  rm_trans_map[MAX_MAPPING_ENTRIES];

/* Client socket name */
#define MAX_CLIENT_SOCK_NAME 32
char               rm_sock_name[MAX_CLIENT_SOCK_NAME];
char               rm_client_name[RM_NAME_MAX_CHARS];

/* FUNCTION PURPOSE: RM transport packet allocation function
 ***********************************************************************
 * DESCRIPTION: Allocates a function for the RM transport implementation.
 *              Packet is returned to RM to be populated with service request
 *              or response data.
 */
Rm_Packet *transportAlloc(Rm_AppTransportHandle appTransport, uint32_t pktSize,
                          Rm_PacketHandle *pktHandle)
{
    Rm_Packet *rm_pkt = NULL;

    rm_pkt = calloc(1, sizeof(*rm_pkt));
    if (!rm_pkt) {
        printf("ERROR Process %d : "
               "Can't malloc for RM send message (err: %s)\n",
               local_process, strerror(errno));
        return (NULL);
    }
    rm_pkt->pktLenBytes = pktSize;
    *pktHandle = rm_pkt;

    return(rm_pkt);
}

/* FUNCTION PURPOSE: RM transport packet free function
 ***********************************************************************
 * DESCRIPTION: Frees a function for the RM transport implementation.
 */
void transportFree(Rm_Packet *rm_pkt)
{
    if (rm_pkt) {
        free (rm_pkt);
    }
}

/* FUNCTION PURPOSE: RM transport packet receive function
 ***********************************************************************
 * DESCRIPTION: RM packet receive function for the RM transport
 *              implementation.  Receives packets over Linux socket interface
 *              and provides them to RM for processing.
 */
void transportReceive(void)
{
    int32_t             rm_result;
    int                 retval;
    size_t              length = 0;
    sock_name_t         server_sock_addr;
    Rm_Packet          *rm_pkt = NULL;
    struct sockaddr_un  server_addr;
    
    retval = sock_wait(rm_client_sock, (int *)&length, NULL, -1);
    if (retval == -2) {
        /* Timeout */
        printf("ERROR Process %d : Error socket timeout\n", local_process);
        return;
    } else if (retval < 0) {
        printf("ERROR Process %d : Error in reading from socket, error %d\n",
               local_process, retval);
        return;
    }
    
    if (length < sizeof(*rm_pkt)) {
        printf("ERROR Process %d : invalid RM message length %d\n",
               local_process, length);
        return;
    }
    rm_pkt = calloc(1, length);
    if (!rm_pkt) {
        printf("ERROR Process %d : "
               "can't malloc for recv'd RM message (err: %s)\n",
               local_process, strerror(errno));
        return;
    }
    
    server_sock_addr.type = sock_addr_e;
    server_sock_addr.s.addr = &server_addr;
    retval = sock_recv(rm_client_sock, (char *)rm_pkt, (int)length,
                       &server_sock_addr);
    if (retval != ((int)length)) {
        printf("ERROR Process %d : "
               "recv RM pkt failed from socket, received = %d, expected = %d\n",
               local_process, retval, length);
        return;
    }
    
    /* Provide packet to RM Client for processing */
    if ((rm_result =
         Rm_receivePacket(rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].trans_handle,
                          rm_pkt))) {
        printf("ERROR Process %d : RM failed to process received packet: %d\n",
               local_process, rm_result);
    }

    transportFree(rm_pkt);
}

/* FUNCTION PURPOSE: RM transport packet send function
 ***********************************************************************
 * DESCRIPTION: Sends RM packets over the RM transport implementation.  Packets
 *              provided by RM are sent over the Linux socket interface.
 */
int32_t transportSendRcv (Rm_AppTransportHandle appTransport,
                          Rm_PacketHandle pktHandle)
{
    sock_name_t *server_sock_name = (sock_name_t *)appTransport;
    Rm_Packet   *rm_pkt = (Rm_Packet *)pktHandle;
    
    if (sock_send(rm_client_sock, (char *)rm_pkt, (int) rm_pkt->pktLenBytes,
                  server_sock_name)) {
        printf("ERROR Process %d : send data failed\n", local_process);
    }

    /* Wait for response from Server */
    transportReceive();
    return (0);
}

/* FUNCTION PURPOSE: Multiprocess round-trip test code
 ***********************************************************************
 * DESCRIPTION: Allocates and sends a MessageQ message between all test
 *              processes created.  The number of round-trips executed is
 *              equivalent to TEST_ITERATIONS
 */
int round_trip_test(TransportQmss_Handle qmss_trans_h, MessageQ_Handle local_q,
                    MessageQ_QueueId remote_q)
{
    test_msg_t *msg = NULL;
    int         last_process = MAX_NUM_PROCESSES - 1;
    size_t      i, j;
    uint8_t     expected;
    int         status;
    int         ret = TEST_FAIL;

    /* Process 0 starts round-robin transfer */
    if (local_process == 0) {
        printf("Process %d : Allocating round trip test MessageQ msg\n",
               local_process);
        msg = (test_msg_t *) MessageQ_alloc(0, sizeof(*msg));
        if (msg == NULL) {
            printf("ERROR Process %d : MessageQ_alloc failed\n", local_process);
            goto err_exit;
        }
    }

    for (i = 0; i < TEST_ITERATIONS; i++) {
        /* First process shouldn't receive on first iteration since it must
         * start the round-robin test */
        if ((local_process != 0) || ((local_process == 0) && (i > 0))) {
            status = MessageQ_get(local_q, (MessageQ_Msg *)&msg,
                                  MessageQ_FOREVER);
            if (status < 0) {
                printf("ERROR Process %d : MessageQ_get failed\n",
                       local_process);
                goto err_exit;
            }

            for (j = 0; j < sizeof(msg->data); j++) {
                expected = FILL_DATA_BYTE(prev_process, j);
                if (msg->data[j] != expected) {
                    printf("ERROR Process %d : Bad data byte %4d\n"
                           "                   Got:      0x%2x\n"
                           "                   Expected: 0x%2x\n",
                           local_process, j, msg->data[j], expected);
                    goto err_exit;
                }
            }
            printf("Process %d : Received msg with good data from Process %d\n",
                   local_process, prev_process);
        }

        /* Last process shouldn't send on last iterations since it must free
         * msg */
        if ((local_process != last_process) ||
            ((local_process == last_process) && (i < (TEST_ITERATIONS - 1)))) {
            if (local_process == 0) {
                printf("Round Trip - %4d\n", i+1);
            }

            if (i == (TEST_ITERATIONS/4)) {
                printf("Process %d : Flushing transport's dst cache of dst "
                       "MessageQ queue ID 0x%x\n", local_process, remote_q);
                if (!TransportQmss_flushDstCache(qmss_trans_h, remote_q)) {
                    printf("ERROR Process %d : Dst cache flush failed\n",
                           local_process);
                    goto err_exit;
                }
            }

            if (i == (TEST_ITERATIONS/2)) {
                printf("Process %d : Flushing transport's entire dst cache\n",
                       local_process);
                if (!TransportQmss_flushDstCache(qmss_trans_h,
                                                 MessageQ_INVALIDMESSAGEQ)) {
                    printf("ERROR Process %d : Dst cache flush failed\n",
                           local_process);
                    goto err_exit;
                }
            }

            for (j = 0; j < sizeof(msg->data); j++) {
                msg->data[j] = FILL_DATA_BYTE(local_process, j);
            }
            printf("Process %d : Sending msg to Process %d\n", local_process,
                   next_process);
            MessageQ_setTransportId(msg, TRANS_QMSS_NET_ID);
            status = MessageQ_put(remote_q, (MessageQ_Msg)msg);
            if (status < 0) {
                printf("ERROR Process %d : MessageQ_put failed\n",
                       local_process);
                goto err_exit;
            }
            msg = NULL;
        }
    }

    ret = TEST_PASS;
err_exit:
    if (msg) {
        printf("Process %d : Freeing round trip test MessageQ msg\n",
               local_process);
        MessageQ_free((MessageQ_Msg)msg);
    }
    return (ret);
}

/* FUNCTION PURPOSE: Configures RM transport implementation path between RM
 *                   Server daemone and RM Clients
 ***********************************************************************
 * DESCRIPTION: Configures Linux sockets as the transport path between the RM
 *              Server daemon and the RM Clients.
 */
int connection_setup(void)
{
    Rm_TransportCfg trans_cfg;
    int32_t         rm_result;
    int             i;
    sock_name_t     sock_name;
    char            server_sock_name[] = RM_SERVER_SOCKET_NAME;
    
    /* Initialize the transport map */
    for (i = 0; i < MAX_MAPPING_ENTRIES; i++) {
        rm_trans_map[i].trans_handle = NULL;
    }

    /* Create local socket for this RM Client */
    snprintf(rm_sock_name, MAX_CLIENT_SOCK_NAME,
             "/var/run/rm/rm_client%d", local_process);
    if (strlen(rm_sock_name) >= MAX_CLIENT_SOCK_NAME) {
        printf("ERROR Process %d : error - client socket name truncated\n",
               local_process);
        return -1;
    }
    sock_name.type = sock_name_e;
    sock_name.s.name = rm_sock_name;

    rm_client_sock = sock_open(&sock_name);
    if (!rm_client_sock) {
        printf("ERROR Process %d : "
               "connection_setup - Client socket open failed\n",
               local_process);
        return (-1);
    }

    /* Configure mapping to the RM Server daemon's socket */
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock = calloc(1, sizeof(sock_name_t));
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->type = sock_name_e;
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name = calloc(1, strlen(server_sock_name)+1);
    strncpy(rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name,
            server_sock_name, strlen(server_sock_name)+1);

    /* Register the Server with the Client instance */
    trans_cfg.rmHandle = rm_client_handle;
    trans_cfg.appTransportHandle = (Rm_AppTransportHandle) rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock;
    trans_cfg.remoteInstType = Rm_instType_SERVER;
    trans_cfg.transportCallouts.rmAllocPkt = transportAlloc;
    trans_cfg.transportCallouts.rmSendPkt = transportSendRcv;
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].trans_handle = Rm_transportRegister(&trans_cfg, &rm_result);

    return(0);
}

/* FUNCTION PURPOSE: RM Client initialization
 ***********************************************************************
 * DESCRIPTION: Initializes the RM Client for this test by establishing a
 *              socket connection with the RM Server
 */
int init_rm (void)
{
    Rm_InitCfg rmInitCfg;
    int32_t    result;
    
    /* Initialize the RM Client - RM must be initialized before anything 
     * else in the system */
    memset(&rmInitCfg, 0, sizeof(rmInitCfg));
    snprintf(rm_client_name, RM_NAME_MAX_CHARS, "RM_Client%d", local_process);
    if (strlen(rm_client_name) >= RM_NAME_MAX_CHARS) {
        printf("ERROR Process %d : client name truncated\n", local_process);
        return (-1);
    }
    rmInitCfg.instName = rm_client_name;
    rmInitCfg.instType = Rm_instType_CLIENT;
    rm_client_handle = Rm_init(&rmInitCfg, &result);
    if (result != RM_OK) {
        printf("ERROR Process %d : RM initialization failed with error %d\n",
               local_process, result);
        return (-1);
    }

    printf("Process %d : Initialized %s\n", local_process, rm_client_name);

    /* Open Client service handle */
    rm_client_service_handle = Rm_serviceOpenHandle(rm_client_handle, &result);
    if (result != RM_OK) {
        printf("ERROR Process %d : Service handle open failed with error %d\n",
               local_process, result);
        return (-1);
    }    
    return(connection_setup());
}

int main(int argc, char *argv[])
{
    pid_t                     pid;
    Int32                     status = 0;
    TransportQmss_Params      qm_trans_params;
    Char                      mpm_inst_name[MPM_INST_NAME_LEN];
    TransportQmss_Handle      qmss_trans_h = NULL;
    INetworkTransport_Handle  net_trans_h;
    ITransport_Handle         base_trans_h;
    MessageQ_Params           msg_params;
    char                      msg_q_name[MSGQ_Q_NAME_LEN];
    MessageQ_Handle           msg_q_h = NULL;
    char                      remote_q_name[MSGQ_Q_NAME_LEN];
    MessageQ_QueueId          remote_q_id = MessageQ_INVALIDMESSAGEQ;
    int                       i;

    printf("*********************************************************\n");
    printf("******** TransportQmss Linux Multi-Process Test *********\n");
    printf("*********************************************************\n");

    printf("TransportQmss Version : 0x%08x\nVersion String: %s\n",
           TransportQmss_getVersion(), TransportQmss_getVersionStr());

    for (local_process = 1;
         local_process < MAX_NUM_PROCESSES;
         local_process++) {
        pid = fork();
        if (!pid) {
            /* Child breaks out of loop */
            break;
        }
    }

    /* Disabling buffering of stdout for proper print out flow */
    setvbuf(stdout, NULL, _IONBF, 0);

    if (pid) {
        /* Parent's local_process reset to 0 */
        local_process = 0;
    }

    /* Calculate adjecent processes */
    next_process = local_process + 1;
    if (next_process == MAX_NUM_PROCESSES) {
        next_process = 0;
    }
    if (local_process == 0) {
        prev_process = MAX_NUM_PROCESSES - 1;
    } else {
        prev_process = local_process - 1;
    }

    Ipc_transportConfig(&TransportRpmsg_Factory);
    status = Ipc_start();
    if (status < 0) {
        printf("ERROR Process %d : Ipc_start failed with error %d\n", 
               local_process, status);
        return(-1);
    }

    /* Setup RM */
    if (init_rm()) {
        printf ("Process %d : init_rm returned error, exiting\n",
                local_process);
        return(-1);
    }

    TransportQmss_Params_init(&qm_trans_params);
    snprintf(mpm_inst_name, MPM_INST_NAME_LEN, "arm-qmss-generic");
    qm_trans_params.mpm_trans_inst_name = mpm_inst_name;
    qm_trans_params.rm_service_h        = rm_client_service_handle;
    qm_trans_params.rx_msg_size_bytes   = MAX_PACKET_SIZE;
    qm_trans_params.mpm_trans_init_qmss = 1;
    printf("Process %d : Creating TransportQmss instance\n", local_process);
    qmss_trans_h = TransportQmss_create(&qm_trans_params);
    if (!qmss_trans_h) {
        printf("ERROR Process %d : Failed to create TransportQmss handle\n", 
               local_process);
        status = -1;
        goto err_exit;
    }

    /* Register transport with MessageQ as network transport */
    net_trans_h = TransportQmss_upCast(qmss_trans_h);
    base_trans_h = INetworkTransport_upCast(net_trans_h);
    if (MessageQ_registerTransportId(TRANS_QMSS_NET_ID, base_trans_h) < 0) {
        printf("ERROR Process %d : "
               "Failed to register TransportQmss as network transport\n",
               local_process);
        status = -1;
        goto err_exit;
    }

    /* Create a MessageQ */
    snprintf(msg_q_name, MSGQ_Q_NAME_LEN, "Process_%d_MsgQ", local_process);
    MessageQ_Params_init(&msg_params);
    msg_q_h = MessageQ_create(msg_q_name, &msg_params);
    if (msg_q_h == NULL) {
        printf("ERROR Process %d : Failed to create MessageQ\n", local_process);
        status = -1;
        goto err_exit;
    }
    printf("Process %d : Local MessageQ: %s, QId: 0x%x\n", local_process,
           msg_q_name, MessageQ_getQueueId(msg_q_h));

    /* Open next process's MessageQ */
    snprintf(remote_q_name, MSGQ_Q_NAME_LEN, "Process_%d_MsgQ", next_process);
    printf ("Process %d : Attempting to open remote queue: %s\n",
            local_process, remote_q_name);
    do {
        status = MessageQ_open(remote_q_name, &remote_q_id);
        sleep(1);
    } while ((status == MessageQ_E_NOTFOUND) || (status == MessageQ_E_TIMEOUT));
    if (status < 0) {
        printf("ERROR Process %d : Error %d when opening next process MsgQ\n",
               local_process, status);
        status = -1;
        goto err_exit;
    } else {
        printf("Process %d : Opened Remote queue: %s, QId: 0x%x\n",
               local_process, remote_q_name, remote_q_id);
    }

    /* Send a msg round-robin through all process MessageQ queues */
    if (round_trip_test(qmss_trans_h, msg_q_h, remote_q_id) == TEST_FAIL) {
        printf("Process %d : Test FAILED\n", local_process);
    } else {
        if (local_process == (MAX_NUM_PROCESSES - 1))
            printf("Test PASSED\n");
    }

err_exit:
    if (local_process == (MAX_NUM_PROCESSES - 1)) {
        printf("Cleaning up\n");
    }

    if (remote_q_id != MessageQ_INVALIDMESSAGEQ) {
        MessageQ_close(&remote_q_id);
    }
    if (msg_q_h) {
        MessageQ_delete(&msg_q_h);
    }
    if (qmss_trans_h) {
        MessageQ_unregisterTransportId(TRANS_QMSS_NET_ID);
        TransportQmss_delete(&qmss_trans_h);
    }

    if (local_process == 0) {
        for (i = 0; i < MAX_NUM_PROCESSES; i++) {
            pid = wait(&status);
            if (WIFEXITED(status)) {
                if (WEXITSTATUS(status)) {
                    printf("Child %d returned fail (%d)\n",
                           pid, WEXITSTATUS(status));
                }
            } else {
                printf("Child %d failed to exit\n", pid);
            }
        }
    }

    status = Ipc_stop();
    if (status < 0) {
        printf("ERROR Process %d : Ipc_stop failed with error %d\n", 
               local_process, status);
    }

    if (local_process == 0){
        printf("Test Complete!\n");
    }
    return (status);
}

/*
 *   arm_ep.c
 *
 *   ARMv7 Linux endpoint in the Heterogeneous Processor Test which tests
 *   TransportQmss between ARMv7 and DSP cores.
 *
 *  ============================================================================
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Shut off: remark #880-D: parameter "appTransport" was never referenced
 *
 * This is better than removing the argument since removal would break
 * backwards compatibility
 */
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
 * warning: unused parameter �appTransport, argc, and argv� [-Wunused-parameter]
 */
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/* Standard includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>

/* IPC includes */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>
#include <ti/ipc/transports/TransportQmss.h>

/* RM includes */
#include <ti/drv/rm/rm_server_if.h>
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>

/* Socket utility includes */
#include "sockutils.h"

/* Number of processes to test - *NOTE* Currently DSPs are only expecting one
 * process to send them messages:
 * Parent  - RM message hub
 * Child 1 - Bidirectional test process */
#define MAX_NUM_PROCESSES          2
/* Number of DSP processors in test */
#ifdef DEVICE_K2E
#define MAX_REMOTE_DSPS            1
#else
#define MAX_REMOTE_DSPS            2
#endif
#define TEST_ITERATIONS            100

#define TEST_PASS                  0
#define TEST_FAIL                  1

#define MPM_INST_NAME_LEN          20
#define MSGQ_Q_NAME_LEN            32
#define MAX_SOCK_NAME_LEN          32

#define MAX_PACKET_SIZE            4096

#define TRANS_QMSS_NET_ID          1

#define KILL_RM_HUB_MSG_ID         1

#define SOCKET_NAME_RM_HUB         "/var/run/rm/rm_msg_hub"
#define SOCKET_NAME_RM_CLIENT      "/var/run/rm/rm_client"

#define MESSAGEQ_MAX_NAME_LEN     32u
#define MESSAGEQ_NAME_RM_HUB      "RM_Message_Hub"
#define MESSAGEQ_RM_NAME_PREFIX   "RM_Client_DSP_"

#define MESSAGEQ_TEST_NAME_PREFIX "TEST_MsgQ_Proc_"

/* Application's registered RM transport indices */
#define SERVER_TO_CLIENT_MAP_ENTRY 0
/* Maximum number of registered RM transports */
#define MAX_MAPPING_ENTRIES        1

#define FILL_DATA_BYTE(proc, idx) (uint8_t)(((proc & 0xF) << 4) | (idx & 0xF))

typedef struct {
    MessageQ_MsgHeader header; /* 32 bytes */
    uint8_t            data[MAX_PACKET_SIZE - sizeof(MessageQ_MsgHeader)];
} test_msg_t;

/* IPC MessageQ RM packet encapsulation structure */
typedef struct {
    /* IPC MessageQ header (must be first element in structure) */
    MessageQ_MsgHeader msgq_header;
    /* Pointer to RM packet */
    Rm_Packet          rm_pkt;
} msgq_rm_pkt_t;

typedef struct trans_map_entry_s {
    Rm_TransportHandle  trans_handle;
    sock_name_t        *remote_sock;
} trans_map_entry_t;

int                   local_process;
int                   next_process;
int                   prev_process;

Rm_Handle             rm_client_handle = NULL;
Rm_ServiceHandle     *rm_client_service_handle = NULL;
sock_h                rm_client_sock;

trans_map_entry_t     rm_trans_map[MAX_MAPPING_ENTRIES];

char                  rm_sock_name[MAX_SOCK_NAME_LEN];
char                  rm_hub_sock_name[MAX_SOCK_NAME_LEN];
char                  rm_server_sock_name[MAX_SOCK_NAME_LEN];
char                  rm_client_name[RM_NAME_MAX_CHARS];

TransportQmss_Handle  qmss_trans_h = NULL;
MessageQ_Handle       loc_msg_q_h = NULL;
#ifdef DEVICE_K2E
MessageQ_QueueId      rem_qid[MAX_REMOTE_DSPS] = {MessageQ_INVALIDMESSAGEQ};
#else
MessageQ_QueueId      rem_qid[MAX_REMOTE_DSPS] = {MessageQ_INVALIDMESSAGEQ,
                                                  MessageQ_INVALIDMESSAGEQ};
#endif

/* FUNCTION PURPOSE: RM transport packet allocation function
 ***********************************************************************
 * DESCRIPTION: Allocates a function for the RM transport implementation.
 *              Packet is returned to RM to be populated with service request
 *              or response data.
 */
Rm_Packet *transportAlloc(Rm_AppTransportHandle appTransport, uint32_t pktSize,
                          Rm_PacketHandle *pktHandle)
{
    Rm_Packet *rm_pkt = NULL;

    rm_pkt = calloc(1, sizeof(*rm_pkt));
    if (!rm_pkt) {
        printf("ERROR Process %d : "
               "Can't malloc for RM send message (err: %s)\n",
               local_process, strerror(errno));
        return (NULL);
    }
    rm_pkt->pktLenBytes = pktSize;
    *pktHandle = rm_pkt;

    return(rm_pkt);
}

/* FUNCTION PURPOSE: RM transport packet free function
 ***********************************************************************
 * DESCRIPTION: Frees a function for the RM transport implementation.
 */
void transportFree(Rm_Packet *rm_pkt)
{
    if (rm_pkt) {
        free (rm_pkt);
    }
}

/* FUNCTION PURPOSE: RM transport packet receive function
 ***********************************************************************
 * DESCRIPTION: RM packet receive function for the RM transport
 *              implementation.  Receives packets over Linux socket interface
 *              and provides them to RM for processing.
 */
void transportReceive(void)
{
    int32_t             rm_result;
    int                 retval;
    size_t              length = 0;
    sock_name_t         server_sock_addr;
    Rm_Packet          *rm_pkt = NULL;
    struct sockaddr_un  server_addr;
    
    retval = sock_wait(rm_client_sock, (int *)&length, NULL, -1);
    if (retval == -2) {
        /* Timeout */
        printf("ERROR Process %d : Error socket timeout\n", local_process);
        return;
    } else if (retval < 0) {
        printf("ERROR Process %d : Error in reading from socket, error %d\n",
               local_process, retval);
        return;
    }
    
    if (length < sizeof(*rm_pkt)) {
        printf("ERROR Process %d : invalid RM message length %d\n",
               local_process, length);
        return;
    }
    rm_pkt = calloc(1, length);
    if (!rm_pkt) {
        printf("ERROR Process %d : "
               "can't malloc for recv'd RM message (err: %s)\n",
               local_process, strerror(errno));
        return;
    }
    
    server_sock_addr.type = sock_addr_e;
    server_sock_addr.s.addr = &server_addr;
    retval = sock_recv(rm_client_sock, (char *)rm_pkt, (int)length,
                       &server_sock_addr);
    if (retval != ((int)length)) {
        printf("ERROR Process %d : "
               "recv RM pkt failed from socket, received = %d, expected = %d\n",
               local_process, retval, length);
        return;
    }
    
    /* Provide packet to RM Client for processing */
    if ((rm_result =
         Rm_receivePacket(rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].trans_handle,
                          rm_pkt))) {
        printf("ERROR Process %d : RM failed to process received packet: %d\n",
               local_process, rm_result);
    }

    transportFree(rm_pkt);
}

/* FUNCTION PURPOSE: RM transport packet send function
 ***********************************************************************
 * DESCRIPTION: Sends RM packets over the RM transport implementation.  Packets
 *              provided by RM are sent over the Linux socket interface.
 */
int32_t transportSendRcv (Rm_AppTransportHandle appTransport,
                          Rm_PacketHandle pktHandle)
{
    sock_name_t *server_sock_name = (sock_name_t *)appTransport;
    Rm_Packet   *rm_pkt = (Rm_Packet *)pktHandle;
    
    if (sock_send(rm_client_sock, (char *)rm_pkt, (int) rm_pkt->pktLenBytes,
                  server_sock_name)) {
        printf("ERROR Process %d : send data failed\n", local_process);
    }

    /* Wait for response from Server */
    transportReceive();
    return (0);
}

int rm_msg_hub(uint16_t num_dsps)
{
    MessageQ_Handle     msg_q_h = NULL;
    MessageQ_Params     msg_params;
    int                 ret = TEST_FAIL;
    int                 i;
    char                rem_msg_q_name[MESSAGEQ_MAX_NAME_LEN];
    Int                 status;
    MessageQ_QueueId   *msg_q_id_arr = NULL;
    MessageQ_Msg        msg = NULL;
    sock_name_t         loc_sock_name;
    sock_h              loc_sock = NULL;
    sock_name_t         serv_sock;
    MessageQ_QueueId    reply_qid;
    msgq_rm_pkt_t      *msgq_rm_pkt;
    Rm_Packet          *rm_pkt;
    int                 sock_err;
    sock_name_t         server_sock_addr;
    struct sockaddr_un  server_addr;

    printf("Process %d : Starting RM Message Hub\n", local_process);

    /* Create MessageQ to receive RM requests from DSPs */
    MessageQ_Params_init(&msg_params);
    msg_q_h = MessageQ_create(MESSAGEQ_NAME_RM_HUB, &msg_params);
    if (msg_q_h == NULL) {
        printf("ERROR Process %d : MessageQ_create failed\n", local_process);
        goto hub_err;
    } else {
        printf("Process %d : Created RM hub queue: %s, Qid: 0x%x\n",
               local_process, MESSAGEQ_NAME_RM_HUB,
               MessageQ_getQueueId(msg_q_h));
    }

    msg_q_id_arr = calloc(num_dsps, sizeof(MessageQ_QueueId));
    
    /* Open the DSP RM MessageQs. */
    for (i = 0; i < num_dsps; i++) {
        /* Beginning of DSP MultiProc IDs is cluster ID + 1 since Host takes
         * the first ID on the device */
        snprintf(rem_msg_q_name, MESSAGEQ_MAX_NAME_LEN, "%s%d",
                 MESSAGEQ_RM_NAME_PREFIX,
                 i + 1 + MultiProc_getBaseIdOfCluster());
        printf("Process %d : Opening %s\n", local_process, rem_msg_q_name);
        do {
            status = MessageQ_open(rem_msg_q_name, &msg_q_id_arr[i]);
            sleep(1);
        } while ((status == MessageQ_E_NOTFOUND) ||
                 (status == MessageQ_E_TIMEOUT));
        if (status < 0) {
            printf("ERROR Process %d : Error %d when opening DSP MessageQ\n",
                   local_process, status);
            goto hub_err;
        } else {
            printf("Process %d : Opened Remote queue: %s, QId: 0x%x\n",
                   local_process, rem_msg_q_name, msg_q_id_arr[i]);
        }

        /* Provide the Linux MessageQ ID to each DSP through a handshake msg */
        msg = MessageQ_alloc(0, sizeof(MessageQ_MsgHeader));
        if (msg == NULL) {
            printf("ERROR_Process %d : Could not allocate handshake message\n",
                   local_process);
            goto hub_err;
        }

        MessageQ_setReplyQueue(msg_q_h, msg);
        printf("Process %d : Sending handshake to DSP %d\n", local_process,
               i + 1);
        status = MessageQ_put(msg_q_id_arr[i], msg);
        if (status < 0) {
            printf("ERROR Process %d : MessageQ_put failed for handshake\n",
                   local_process);
            MessageQ_free(msg);
            goto hub_err;
        }

        status = MessageQ_get(msg_q_h, &msg, MessageQ_FOREVER);
        if (status < 0) {
            printf("ERROR Process %d : MessageQ_get failed for handshake\n",
                   local_process);
            goto hub_err;
        } else {
            printf("Process %d : Received handshake response from DSP %d\n",
                   local_process, i + 1);
        }
        status = MessageQ_free(msg);
        if (status < 0) {
            printf("ERROR Process %d : Could not free handshake message\n",
                   local_process);
            goto hub_err;
        }
    }

    /* Open local sock for communication to RM Server */
    snprintf(rm_hub_sock_name, MAX_SOCK_NAME_LEN, "%s",
             SOCKET_NAME_RM_HUB);
    if (strlen(rm_hub_sock_name) >= MAX_SOCK_NAME_LEN) {
        printf("ERROR Process %d : RM hub socket name too big\n",
               local_process);
        goto hub_err;
    } else {
        printf("Process %d : Opening RM hub socket %s\n", local_process,
               rm_hub_sock_name);
    }
    loc_sock_name.type = sock_name_e;
    loc_sock_name.s.name = rm_hub_sock_name;
    loc_sock = sock_open(&loc_sock_name);
    if (loc_sock == NULL) {
        printf("ERROR Process %d : Could not open RM msg hub socket\n",
               local_process);
        goto hub_err;
    }

    snprintf(rm_server_sock_name, MAX_SOCK_NAME_LEN, "%s",
             RM_SERVER_SOCKET_NAME);
    if (strlen(rm_server_sock_name) >= MAX_SOCK_NAME_LEN) {
        printf("ERROR Process %d : RM Server socket name too big\n",
               local_process);
        goto hub_err;
    } else {
        printf("Process %d : Opening RM Server socket %s\n", local_process,
               rm_server_sock_name);
    }
    serv_sock.type = sock_name_e;
    serv_sock.s.name = rm_server_sock_name;

    /* Inform DSPs RM hub is ready to receive RM requests */
    for (i = 0; i < num_dsps; i++) {
        msg = MessageQ_alloc(0, sizeof(MessageQ_MsgHeader));
        if (msg == NULL) {
            printf("ERROR_Process %d : Could not allocate ready message\n",
                   local_process);
            goto hub_err;
        }

        printf("Process %d : Sending ready msg to DSP %d\n", local_process,
               i + 1);
        status = MessageQ_put(msg_q_id_arr[i], msg);
        if (status < 0) {
            printf("ERROR Process %d : MessageQ_put failed for ready msg\n",
                   local_process);
            MessageQ_free(msg);
            goto hub_err;
        }
    }

    printf("Process %d : Wait for RM messages from DSP RM clients\n",
           local_process);
    while(1) {
        size_t rm_pkt_len;

        status = MessageQ_get(msg_q_h, &msg, MessageQ_FOREVER);
        if (status < 0) {
            printf("ERROR Process %d : MessageQ_get failed\n", local_process);
            goto hub_err;
        }

        if (MessageQ_getMsgId(msg) == KILL_RM_HUB_MSG_ID) {
            /* Kill the hub since DSP applications are shutting down */
            MessageQ_free(msg);
            break;
        }

        reply_qid = MessageQ_getReplyQueue(msg);
        msgq_rm_pkt = (msgq_rm_pkt_t *)msg;
        rm_pkt = &(msgq_rm_pkt->rm_pkt);

        /* Send received data to RM Server */
        if (sock_send(loc_sock, (char *)rm_pkt, sizeof(*rm_pkt), &serv_sock)) {
            printf("ERROR Process %d : Failed to send msg to RM Server\n",
                   local_process);
            MessageQ_free(msg);
            goto hub_err;
        }
        MessageQ_free(msg);

        /* Wait for response from RM Server */
        rm_pkt_len = 0;
        sock_err = sock_wait(loc_sock, (int *)&rm_pkt_len, NULL, -1);
        if (sock_err == -2) {
            /* Timeout */
            printf("ERROR Process %d : "
                   "Timeout waiting for RM Server to respond\n", local_process);
            goto hub_err;
        } else if (sock_err < 0) {
            printf("ERROR Process %d : "
                   "Socket error %d while waiting for RM Server response\n",
                   local_process, sock_err);
            goto hub_err;
        }

        if (rm_pkt_len > sizeof(*msgq_rm_pkt)) {
            printf("ERROR Process %d : "
                   "Packet from RM Server exceeds size of msgq_rm_pkt_t\n",
                   local_process);
            goto hub_err;
        } else {
            msg = MessageQ_alloc(0, sizeof(*msgq_rm_pkt));
            msgq_rm_pkt = (msgq_rm_pkt_t *)msg;
            rm_pkt = &(msgq_rm_pkt->rm_pkt);
        }

        server_sock_addr.type = sock_addr_e;
        server_sock_addr.s.addr = &server_addr;
        sock_err = sock_recv(loc_sock, (char *)rm_pkt, (int)rm_pkt_len,
                             &server_sock_addr);
        if (sock_err != ((int)rm_pkt_len)) {
            printf("ERROR Process %d : "
                   "Socket error or invalid size when receiving from RM "
                   "Server socket\n", local_process);
            MessageQ_free((MessageQ_Msg)msgq_rm_pkt);
            goto hub_err;
        }

        /* Send back to DSP */
        status = MessageQ_put(reply_qid, (MessageQ_Msg)msgq_rm_pkt);
        if (status < 0) {
             printf("ERROR Process %d : "
                    "Error %d when sending msg back to DSP\n",
                    local_process, status);
             MessageQ_free((MessageQ_Msg)msgq_rm_pkt);
             goto hub_err;
        }
    }

    ret = TEST_PASS;

hub_err:
    /* Clean-up */
    printf("Process %d : Cleaning up RM Message Hub\n", local_process);
    sock_close(loc_sock);

    for (i = 0; i < num_dsps; i++) {
        if (&msg_q_id_arr[i]) {
            status = MessageQ_close(&msg_q_id_arr[i]);
            if (status < 0) {
                printf("ERROR Process %d : "
                       "Error %d when closing MessageQ 0x%x\n",
                       local_process, status, (uint32_t)&msg_q_id_arr[i]);
            }
        }
    }

    free(msg_q_id_arr);

    status = MessageQ_delete(&msg_q_h);
    if (status < 0) {
        printf("ERROR Process %d : Error %d when deleting MessageQ\n",
               local_process, status);
    }

    return(ret);
}

/* FUNCTION PURPOSE: ARM-DSP bidirectional test code
 ***********************************************************************
 * DESCRIPTION: Allocates and sends a MessageQ message to a specified
 *              number of DSPs.  Responses are sent by all DSPs.
 */
int bidirectional_test(void)
{
    test_msg_t *msg = NULL;
    size_t      i, j, k;
    uint8_t     expected;
    int         status;
    int         ret = TEST_FAIL;

    printf("Process %d : Allocating bidirectional test MessageQ msg\n",
           local_process);
    msg = (test_msg_t *) MessageQ_alloc(0, sizeof(*msg));
    if (msg == NULL) {
        printf("ERROR Process %d : MessageQ_alloc failed\n", local_process);
        goto err_exit;
    }

    for (i = 0; i < TEST_ITERATIONS; i++) {
        printf("Process %d : ### Round Trip - %4d ###\n", local_process, i+1);
        for (j = 0; j < MAX_REMOTE_DSPS; j++) {
            /* Each process sends a message to and receives a message from
             * each DSP */

            for (k = 0; k < sizeof(msg->data); k++) {
                msg->data[k] = FILL_DATA_BYTE(MultiProc_self(), k);
            }
            printf("Process %d : Sending msg to DSP %d using Qid 0x%x\n",
                   local_process, j + 1, rem_qid[j]);
            MessageQ_setTransportId(msg, TRANS_QMSS_NET_ID);
            status = MessageQ_put(rem_qid[j], (MessageQ_Msg)msg);
            if (status < 0) {
                printf("ERROR Process %d : MessageQ_put failed\n",
                       local_process);
                goto err_exit;
            }
            msg = NULL;

            status = MessageQ_get(loc_msg_q_h, (MessageQ_Msg *)&msg,
                                  MessageQ_FOREVER);
            if (status < 0) {
                printf("ERROR Process %d : MessageQ_get failed\n",
                       local_process);
                goto err_exit;
            }

            for (k = 0; k < sizeof(msg->data); k++) {
                expected = FILL_DATA_BYTE((j + 1 +
                                          MultiProc_getBaseIdOfCluster()), k);
                if (msg->data[k] != expected) {
                    printf("ERROR Process %d : Bad data byte %4d\n"
                           "                   Got:      0x%2x\n"
                           "                   Expected: 0x%2x\n",
                           local_process, k, msg->data[k], expected);
                    goto err_exit;
                }
            }
            printf("Process %d : Received msg with good data from DSP %d\n",
                   local_process, j + 1);
        }
    }

    ret = TEST_PASS;
err_exit:
    if (msg) {
        printf("Process %d : Freeing bidirectional test MessageQ msg\n",
               local_process);
        MessageQ_free((MessageQ_Msg)msg);
    }
    return (ret);
}

/* FUNCTION PURPOSE: Configures RM transport implementation path between RM
 *                   Server daemone and RM Clients
 ***********************************************************************
 * DESCRIPTION: Configures Linux sockets as the transport path between the RM
 *              Server daemon and the RM Clients.
 */
int connection_setup(void)
{
    Rm_TransportCfg trans_cfg;
    int32_t         rm_result;
    int             i;
    sock_name_t     sock_name;
    char            server_sock_name[] = RM_SERVER_SOCKET_NAME;
    
    /* Initialize the transport map */
    for (i = 0; i < MAX_MAPPING_ENTRIES; i++) {
        rm_trans_map[i].trans_handle = NULL;
    }

    /* Create local socket for this RM Client */
    snprintf(rm_sock_name, MAX_SOCK_NAME_LEN,
             "%s%d", SOCKET_NAME_RM_CLIENT, MultiProc_self());
    if (strlen(rm_sock_name) >= MAX_SOCK_NAME_LEN) {
        printf("ERROR Process %d : RM client socket name too big\n",
               local_process);
        return -1;
    } else {
        printf("Process %d : Opening RM client socket %s\n", local_process,
               rm_sock_name);
    }
    sock_name.type = sock_name_e;
    sock_name.s.name = rm_sock_name;

    rm_client_sock = sock_open(&sock_name);
    if (!rm_client_sock) {
        printf("ERROR Process %d : "
               "connection_setup - Client socket open failed\n",
               local_process);
        return (-1);
    }

    /* Configure mapping to the RM Server daemon's socket */
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock = calloc(1, sizeof(sock_name_t));
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->type = sock_name_e;
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name = calloc(1, strlen(server_sock_name)+1);
    strncpy(rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name,
            server_sock_name, strlen(server_sock_name)+1);

    /* Register the Server with the Client instance */
    trans_cfg.rmHandle = rm_client_handle;
    trans_cfg.appTransportHandle = (Rm_AppTransportHandle) rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock;
    trans_cfg.remoteInstType = Rm_instType_SERVER;
    trans_cfg.transportCallouts.rmAllocPkt = transportAlloc;
    trans_cfg.transportCallouts.rmSendPkt = transportSendRcv;
    rm_trans_map[SERVER_TO_CLIENT_MAP_ENTRY].trans_handle = Rm_transportRegister(&trans_cfg, &rm_result);

    return(0);
}

int init_transportqmss(void)
{
    Int32                    status = 0;
    TransportQmss_Params     qm_trans_params;
    Char                     mpm_inst_name[MPM_INST_NAME_LEN];
    INetworkTransport_Handle net_trans_h;
    ITransport_Handle        base_trans_h;
    MessageQ_Params          msg_params;
    char                     msg_q_name[MSGQ_Q_NAME_LEN];
    int                      i;
    char                     remote_q_name[MSGQ_Q_NAME_LEN];

    TransportQmss_Params_init(&qm_trans_params);
    snprintf(mpm_inst_name, MPM_INST_NAME_LEN, "arm-qmss-generic");
    qm_trans_params.mpm_trans_inst_name = mpm_inst_name;
    qm_trans_params.rm_service_h        = rm_client_service_handle;
    qm_trans_params.rx_msg_size_bytes   = MAX_PACKET_SIZE;
    qm_trans_params.mpm_trans_init_qmss = 1;
    printf("Process %d : Creating TransportQmss instance\n", local_process);
    qmss_trans_h = TransportQmss_create(&qm_trans_params);
    if (!qmss_trans_h) {
        printf("ERROR Process %d : Failed to create TransportQmss handle\n", 
               local_process);
        status = -1;
        goto err_exit;
    }

    /* Register transport with MessageQ as network transport */
    net_trans_h = TransportQmss_upCast(qmss_trans_h);
    base_trans_h = INetworkTransport_upCast(net_trans_h);
    if (MessageQ_registerTransportId(TRANS_QMSS_NET_ID, base_trans_h) < 0) {
        printf("ERROR Process %d : "
               "Failed to register TransportQmss as network transport\n",
               local_process);
        status = -1;
        goto err_exit;
    }

    /* Create a MessageQ */
    snprintf(msg_q_name, MSGQ_Q_NAME_LEN, "%s%d", MESSAGEQ_TEST_NAME_PREFIX,
             MultiProc_self());
    MessageQ_Params_init(&msg_params);
    loc_msg_q_h = MessageQ_create(msg_q_name, &msg_params);
    if (loc_msg_q_h == NULL) {
        printf("ERROR Process %d : Failed to create MessageQ\n", local_process);
        status = -1;
        goto err_exit;
    }
    printf("Process %d : Local MessageQ: %s, QId: 0x%x\n", local_process,
           msg_q_name, MessageQ_getQueueId(loc_msg_q_h));

    for (i = 0; i < MAX_REMOTE_DSPS; i++) {
        /* Open DSP MessageQs */
        snprintf(remote_q_name, MSGQ_Q_NAME_LEN, "%s%d",
                 MESSAGEQ_TEST_NAME_PREFIX,
                 i + 1 + MultiProc_getBaseIdOfCluster());
        printf("Process %d : Attempting to open DSP %d queue: %s\n",
               local_process, i + 1, remote_q_name);
        do {
            status = MessageQ_open(remote_q_name, &(rem_qid[i]));
            sleep(1);
        } while ((status == MessageQ_E_NOTFOUND) ||
                 (status == MessageQ_E_TIMEOUT));
        if (status < 0) {
            printf("ERROR Process %d : "
                   "Error %d when opening next DSP %d MsgQ\n",
                   local_process, status, i);
            status = -1;
            goto err_exit;
        } else {
            printf("Process %d : Opened DSP %d queue: %s, QId: 0x%x\n",
                   local_process, i + 1, remote_q_name, rem_qid[i]);
        }
    }

err_exit:
    return(status);
}

/* FUNCTION PURPOSE: RM Client initialization
 ***********************************************************************
 * DESCRIPTION: Initializes the RM Client for this test by establishing a
 *              socket connection with the RM Server
 */
int init_rm(void)
{
    Rm_InitCfg rmInitCfg;
    int32_t    result;
    
    /* Initialize the RM Client - RM must be initialized before anything
     * else in the system */
    memset(&rmInitCfg, 0, sizeof(rmInitCfg));
    snprintf(rm_client_name, RM_NAME_MAX_CHARS, "RM_Client%d",
             MultiProc_self());
    if (strlen(rm_client_name) >= RM_NAME_MAX_CHARS) {
        printf("ERROR Process %d : client name truncated\n", local_process);
        return (-1);
    }
    rmInitCfg.instName = rm_client_name;
    rmInitCfg.instType = Rm_instType_CLIENT;
    rm_client_handle = Rm_init(&rmInitCfg, &result);
    if (result != RM_OK) {
        printf("ERROR Process %d : RM initialization failed with error %d\n",
               local_process, result);
        return (-1);
    }

    printf("Process %d : Initialized %s\n", local_process, rm_client_name);

    /* Open Client service handle */
    rm_client_service_handle = Rm_serviceOpenHandle(rm_client_handle, &result);
    if (result != RM_OK) {
        printf("ERROR Process %d : Service handle open failed with error %d\n",
               local_process, result);
        return (-1);
    }    
    return(connection_setup());
}

int main(int argc, char *argv[])
{
    pid_t pid;
    Int32 status = 0;
    int   i;

    printf("*********************************************************\n");
    printf("* ARMv7 Linux TransportQmss Heterogeneous Test (ARM EP) *\n");
    printf("*********************************************************\n");

    printf("TransportQmss Version : 0x%08x\nVersion String: %s\n",
           TransportQmss_getVersion(), TransportQmss_getVersionStr());

    for (local_process = 1;
         local_process < MAX_NUM_PROCESSES;
         local_process++) {
        pid = fork();
        if (!pid) {
            /* Child breaks out of loop */
            break;
        }
    }

    /* Disabling buffering of stdout for proper print out flow */
    setvbuf(stdout, NULL, _IONBF, 0);

    if (pid) {
        /* Parent's local_process reset to 0 */
        local_process = 0;
    }

    Ipc_transportConfig(&TransportRpmsg_Factory);
    status = Ipc_start();
    if (status < 0) {
        printf("ERROR Process %d : Ipc_start failed with error %d\n", 
               local_process, status);
        return(-1);
    }

    if (local_process == 0) {
        /* RM request/response hub between server and DSP clients */
        rm_msg_hub(MAX_REMOTE_DSPS);
    } else {
        /* Setup RM */
        if (init_rm()) {
            printf("ERROR Process %d : init_rm returned error, exiting\n",
                   local_process);
            status = -1;
            goto err_exit;
        }

        /* Test process */
        if ((status = init_transportqmss()) < 0) {
            goto err_exit;
        }
       
        /* Test bidirectional MessageQ paths between ARM and DSPs */
        if (bidirectional_test() == TEST_FAIL) {
            printf("Process %d : Test FAILED\n", local_process);
        } else {
            if (local_process == (MAX_NUM_PROCESSES - 1))
                printf("Test PASSED\n");
        }
    }

err_exit:
    if (local_process == (MAX_NUM_PROCESSES - 1)) {
        printf("Cleaning test process\n");
    }

    for (i = 0; i < MAX_REMOTE_DSPS; i++) {
        if (rem_qid[i] != MessageQ_INVALIDMESSAGEQ) {
            MessageQ_close(&(rem_qid[i]));
        }
    }
    if (loc_msg_q_h) {
        MessageQ_delete(&loc_msg_q_h);
    }
    if (qmss_trans_h) {
        MessageQ_unregisterTransportId(TRANS_QMSS_NET_ID);
        TransportQmss_delete(&qmss_trans_h);
    }

    if (local_process == 0) {
        for (i = 0; i < MAX_NUM_PROCESSES; i++) {
            pid = wait(&status);
            if (WIFEXITED(status)) {
                if (WEXITSTATUS(status)) {
                    printf("Child %d returned fail (%d)\n",
                           pid, WEXITSTATUS(status));
                }
            } else {
                printf("Child %d failed to exit\n", pid);
            }
        }
    }

    status = Ipc_stop();
    if (status < 0) {
        printf("ERROR Process %d : Ipc_stop failed with error %d\n", 
               local_process, status);
    }

    if (local_process == 0){
        printf("Test Complete!\n");
    }
    return (status);
}
